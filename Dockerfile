## REACT BUILD ##
FROM node:lts-alpine3.13 as react-builder 
WORKDIR /root/internally

RUN npm install react-scripts -g

COPY ./Frontend/internally .
RUN npm ci && npm run build

## JAVA BUILD ##
FROM maven:3.6.3-openjdk-11-slim as maven-builder

WORKDIR /root/internally
COPY ./Backend/internally .
COPY --from=react-builder /root/internally/build ./src/main/webapp

RUN mvn clean package

## WILDFLY ##
FROM alpine:3 as wildfly

LABEL maintainer="Jan Bellenberg, Simon Maddahi"
LABEL version="1.0.0"
WORKDIR "/root/internally"

ENV WILDFLY_VERSION=22.0.0
ENV ZONE=Europe/Berlin

# install packages
RUN apk update && apk add --no-cache curl openjdk11-jre

# download and setup wildfly
RUN curl https://download.jboss.org/wildfly/$WILDFLY_VERSION.Final/wildfly-$WILDFLY_VERSION.Final.zip --output ./wildfly.zip && \
  unzip wildfly.zip && \
  mv ./wildfly-$WILDFLY_VERSION.Final/ ./wildfly/ && \ 
  ./wildfly/bin/add-user.sh -u 'admin' -p 'gXg33Ep4urGp6bF2' && \
  rm wildfly.zip

# set time zone
RUN apk add --no-cache tzdata && \
  cp /usr/share/zoneinfo/$ZONE /etc/localtime && \
  echo $ZONE > /etc/timezone && \
  apk del tzdata


# copy files
COPY ./Backend/wildfly-config/mysql ./wildfly/modules/system/layers/base/com/mysql/main
COPY ./Backend/wildfly-config/standalone.conf ./wildfly/bin/standalone.conf
COPY ./Backend/wildfly-config/standalone.xml ./wildfly/standalone/configuration/standalone.xml
COPY ./Backend/wildfly-config/server.pfx ./wildfly/standalone/configuration/server.pfx
COPY --from=maven-builder /root/internally/target/internally.war ./wildfly/standalone/deployments/internally.war

CMD ["./wildfly/bin/standalone.sh"]
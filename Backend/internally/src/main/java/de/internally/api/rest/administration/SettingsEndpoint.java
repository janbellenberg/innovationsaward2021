package de.internally.api.rest.administration;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonPatchBuilder;
import javax.json.JsonReader;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.internally.ejb.database.EmployeeEJB;
import de.internally.entities.database.Employee;
import de.internally.helper.MailHelper;
import de.internally.helper.security.SecurePassword;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/administration/settings
 * settings management
 * @author janbellenberg
 * @see EmployeeEJB
 */
@Path("/administration/settings")
public class SettingsEndpoint {

	@Inject
	private EmployeeEJB employeeEJB;

	/**
	 * load settings file
	 * @return HTTP Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response getSettings() {
		String path = System.getProperty("jboss.server.data.dir") + "/internally/settings.json";
		File file = new File(path);
		return Response.ok(file).build();
	}

	/**
	 * update settings file
	 * @param uid id of the employee
	 * @param request parsed request data (application/json)
	 * @return HTTP Response
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response updateSettings(@HeaderParam("X-UID") int uid, JsonObject request) {
		try {
			String path = System.getProperty("jboss.server.data.dir") + "/internally/settings.json";
			File file = new File(path);
			JsonReader reader = Json.createReader(new FileReader(file));
			JsonObject settings = reader.readObject();

			// process new admin password
			JsonPatchBuilder adminPatch = Json.createPatchBuilder();
			if(request.containsKey("admin_password")) {
				
				String salt = SecurePassword.generateSalt(new ArrayList<>());
				String pepper = SecurePassword.generatePepper();
				String password = SecurePassword.generateHash(request.getString("admin_password"), salt, pepper);
				
				JsonObjectBuilder admin = Json.createObjectBuilder();
				admin.add("hash", password);
				admin.add("salt", salt);
				adminPatch.add("/admin", admin.build());
				adminPatch.remove("/admin_password");
			} else {
				adminPatch.add("/admin", settings.getJsonObject("admin"));
			}
			request = adminPatch.build().apply(request);
			
			// validate settings format
			if(!checkSettingsFormat(request, settings) ||
				!checkSettingsFormat(request.getJsonObject("smtp"), settings.getJsonObject("smtp"))) {
				throw new NullPointerException();
			}
			
		    BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		    writer.write(request.toString());
		    writer.close();
		    
		    Employee receiver = this.employeeEJB.getEmployee(uid);
		    
		    MailHelper.sendMail(receiver.getEmail(), "Einstellungen", "Ihre Einstellungen wurden erfolgreich übernommen");

		} catch (IOException e) {
			e.printStackTrace();
			return Response.serverError().build();
		}
		return Response.noContent().build();
	}
	
	/**
	 * compares new settings data to already existing
	 * @param newData new settings
	 * @param oldData existing settings
	 * @return if the settings match
	 */
	private boolean checkSettingsFormat(JsonObject newData, JsonObject oldData) {
		for(String k : oldData.keySet()) {
			if(!newData.containsKey(k)) {
				return false;
			}
		}
		return true;
	}
 
}

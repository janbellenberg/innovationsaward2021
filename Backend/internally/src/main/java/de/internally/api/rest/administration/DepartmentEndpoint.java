package de.internally.api.rest.administration;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.internally.ejb.database.DepartmentEJB;
import de.internally.entities.database.Department;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/administration/departments
 * Department management
 * @author simonmaddahi
 * @see DepartmentEJB
 */
@Singleton
@Path("/administration/departments")
public class DepartmentEndpoint {

	@Inject
	private DepartmentEJB departmenEJB;

	/**
	 * get all departments
	 * @return HTTP Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response getDepartments() {
		JsonArrayBuilder builder = Json.createArrayBuilder();
		List<Department> list = departmenEJB.getAll();
		for (Department d : list) {
			JsonObjectBuilder request = Json.createObjectBuilder();
			request.add("id", d.getId());
			request.add("name", d.getName());
			builder.add(request.build());
		}

		return Response.ok(builder.build()).build();
	}

	
	/**
	 * add a department
	 * @param request parsed request data (application/json)
	 * @return HTTP Response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response addDepartment(JsonObject request) {

		Department department = new Department();
		department.setName(request.getString("name"));

		departmenEJB.saveDepartment(department);
		return Response.created(null).build();
	}

	/**
	 * update a department
	 * @param id id of the department
	 * @param request parsed request data (application/json)
	 * @return  HTTP Response
	 */
	@Path("/{id}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response editDepartment(@PathParam("id") int id, JsonObject request) {

		Department department = this.departmenEJB.getDepartment(Integer.valueOf(id));
		department.setName(request.getString("name"));

		departmenEJB.saveDepartment(department);
		return Response.noContent().build();
	}

	/**
	 * delete department
	 * @param id id of the department
	 * @return HTTP Response
	 */
	@Path("/{id}")
	@DELETE
	@Secured(level = SecurityLevel.ADMIN)
	public Response dropDepartment(@PathParam("id") int id) {
		Department department = departmenEJB.getDepartment(id);
		departmenEJB.deleteDepartment(department);
		return Response.noContent().build();
	}
}

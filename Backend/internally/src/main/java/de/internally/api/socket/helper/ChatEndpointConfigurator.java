package de.internally.api.socket.helper;

import java.util.List;
import java.util.Map;

import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;
import javax.websocket.server.ServerEndpointConfig.Configurator;

import de.internally.api.rest.authentication.AuthenticationFilter;

/**
 * configuration class for chat sockets
 * @author janbellenberg
 */
public class ChatEndpointConfigurator extends Configurator {

	/**
	 * extracts the jwt and the chat id from the http request
	 * @param config the endpoint configuration
	 * @param request the http request for the handshake
	 * @param response the http response for the handshake
	 */
	@Override
	public void modifyHandshake(ServerEndpointConfig config, HandshakeRequest request, HandshakeResponse response) {
		// get jwt
		Map<String, List<String>> headers = request.getHeaders();

		// iterate through all headers
		if(headers.containsKey("cookie")) {
			for (String s : headers.get("cookie")) {
				if (s.contains(AuthenticationFilter.COOKIE_NAME)) {
					
					// split into single cookies
					String[] cookies = s.split(";");
					for (String t : cookies) {
						if (t.contains(AuthenticationFilter.COOKIE_NAME)) {
							
							// split cookies into key and value
							String[] cookie = t.trim().split("=");
							if (cookie.length == 2) {
								config.getUserProperties().put("jwt", cookie[1]);
							}
						}
					}
				}
			}
		}

		// get chat id
		Map<String, List<String>> params = request.getParameterMap();

		if (params.containsKey("id")) {
			List<String> ids = params.get("id");
			if (ids.size() == 1) {
				config.getUserProperties().put("chat", ids.get(0));
			}
		} else {
			if(config.getUserProperties().containsKey("chat")) {
				config.getUserProperties().remove("chat");
			}
		}
	}
}

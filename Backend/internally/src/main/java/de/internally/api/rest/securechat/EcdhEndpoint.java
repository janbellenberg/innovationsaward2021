package de.internally.api.rest.securechat;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import de.internally.ejb.database.EcdhEJB;
import de.internally.ejb.database.EmployeeEJB;
import de.internally.entities.database.EcdhPreKey;
import de.internally.entities.database.Employee;
import de.internally.helper.security.RsaHelper;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/secure_chat/ecdh
 * secure chat ECDH Pre-Keys
 * @author janbellenberg
 * @see EcdhEJB
 * @see EmployeeEJB
 */
@Singleton
@Path("/secure_chat/ecdh")
public class EcdhEndpoint {
	
	@Inject
	private EcdhEJB ecdhEJB;
	
	@Inject
	private EmployeeEJB employeeEJB;

	/**
	 * get my next ecdh prekey
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Path("/next")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getNextEcdh(@HeaderParam("X-UID") int uid) {
		EcdhPreKey key = this.ecdhEJB.getNext(uid);
		
		if(key == null) {
			return Response.status(416).build();
		}
		
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("id", key.getId());
		builder.add("public", key.getPublicComponent());
		builder.add("private", key.getPrivateComponent());
		builder.add("signature", key.getSignature());
		builder.add("server_signature", RsaHelper.sign(key.getPrivateComponent()));
		
		return Response.ok(builder.build()).build();
	}

	/**
	 * get next public ecdh prekey of other employee
	 * @param id id of the other employee
	 * @return HTTP Response
	 */
	@GET
	@Path("/{id}/next")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getNextEcdhPublic(@PathParam("id") int id) {
		EcdhPreKey key = this.ecdhEJB.getNext(id);
		
		if(key == null) {
			return Response.status(416).build();
		}
		
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("id", key.getId());
		builder.add("public", key.getPublicComponent());
		builder.add("server_signature", RsaHelper.sign(key.getPublicComponent()));
		
		return Response.ok(builder.build()).build();
	}

	/**
	 * get my ecdh prekey by id
	 * @param id id of the prekey
	 * @return HTTP Response
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getEcdhPreKey(@PathParam("id") int id) {
		EcdhPreKey key = this.ecdhEJB.getPreKey(id);
		
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("id", key.getId());
		builder.add("public", key.getPublicComponent());
		builder.add("private", key.getPrivateComponent());
		builder.add("signature", key.getSignature());
		builder.add("server_signature", RsaHelper.sign(key.getPrivateComponent()));
		
		return Response.ok(builder.build()).build();
	}

	/**
	 * add a new ecdh prekey
	 * @param uid id of the employee
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response addEcdhPreKey(@HeaderParam("X-UID") int uid, JsonObject data) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		
		String privateKey = data.getString("private");
		String publicKey = data.getString("public");
		String signature = data.getString("signature");
		
		// verify identitiy of the sender
		if(!RsaHelper.verify(privateKey, signature, employee.getRsaPublic())) {
			return Response.status(Status.NOT_ACCEPTABLE).build();
		}
		
		EcdhPreKey key = new EcdhPreKey();
		key.setEmployee(employee);
		key.setPrivateComponent(privateKey);
		key.setPublicComponent(publicKey);
		key.setSignature(signature);
		
		this.ecdhEJB.saveEcdhKey(key);
		return Response.status(Status.CREATED).build();
	}
	
	/**
	 * delete my ecdh prekey
	 * @param uid id of the employee
	 * @param id id of the prekey
	 * @return HTTP Response
	 */
	@DELETE
	@Path("/{id}")
	@Secured(level = SecurityLevel.USER)
	public Response deleteEcdhPreKey(@HeaderParam("X-UID") int uid, @PathParam("id") int id) {
		EcdhPreKey key = this.ecdhEJB.getPreKey(id);
		
		if(key.getEmployee().getId() != uid) {
			throw new NotAuthorizedException(Response.status(Status.UNAUTHORIZED).build());
		}
		
		this.ecdhEJB.deletePreKey(key); 
		return Response.ok().build();
	}
}
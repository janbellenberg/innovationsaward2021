package de.internally.api.rest;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.internally.ejb.database.EmployeeEJB;
import de.internally.entities.database.Employee;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/ui-init
 * ui initialization
 * @author janbellenberg
 *
 */
@Singleton
@Path("/ui-init")
public class UiEndpoint {

	@Inject
	EmployeeEJB employeeEJB;

	/**
	 * load data about the user
	 * @param UID id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.BOTH)
	public Response getUiInitializationInformation(@HeaderParam("X-UID") String UID) {
		if (UID.equals("0")) {
			// for administrative user from config file
			JsonObjectBuilder data = Json.createObjectBuilder();
			data.add("id", 0);
			data.add("username", "admin");
			data.add("firstname", "System");
			data.add("lastname", "Admin");
			data.add("email", "keine E-Mail");
			data.add("administration", true);
			data.add("chat", false);
			data.add("design", false);
			data.add("missing_ecdh", 0);
			data.add("new_rsa", false);
			data.add("new_master_key", false);
			data.add("status", 1);

			return Response.ok(data.build().toString()).build();
		}

		Employee e = employeeEJB.getEmployee(Integer.valueOf(UID));

		// calculate ecdh prekey that should be generated
		int missingEcdh = 0;
		if (e.getEcdhPrekeys().size() < 5) {
			missingEcdh = 10 - e.getEcdhPrekeys().size();
		}

		JsonObjectBuilder data = Json.createObjectBuilder();
		data.add("id", e.getId());
		data.add("username", e.getUsername());
		data.add("firstname", e.getFirstname());
		data.add("lastname", e.getLastname());
		data.add("email", e.getEmail());
		data.add("administration", e.getGroup().getAdministration());
		data.add("chat", true);
		data.add("design", e.getDesign());
		data.add("missing_ecdh", missingEcdh);
		data.add("new_rsa", e.getRsaPrivate() == null || e.getRsaPublic() == null);
		data.add("new_master_key", e.getMasterKey() == null);
		data.add("status", e.getStatus());

		return Response.ok(data.build().toString()).build();
	}
}

package de.internally.api.rest.securechat;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import de.internally.ejb.database.EcdhEJB;
import de.internally.ejb.database.EmployeeEJB;
import de.internally.ejb.database.EncryptedMessagesEJB;
import de.internally.entities.database.Employee;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/secure_chat
 * secure chat general
 * @author janbellenberg
 * @see EmployeeEJB
 * @see EncryptedMessagesEJB
 * @see EcdhEJB
 */
@Singleton
@Path("/secure_chat")
public class SecureChatEndpoint {

	@Inject
	private EmployeeEJB employeeEJB;

	@Inject
	private EncryptedMessagesEJB messageEJB;

	@Inject
	private EcdhEJB ecdhEJB;

	/**
	 * reset the secure communication (for bug fixes)
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@POST
	@Path("/reset")
	@Secured(level = SecurityLevel.USER)
	public Response resetSecureChat(@HeaderParam("X-UID") int uid) {
		
		Employee employee = this.employeeEJB.getEmployee(uid);
		employee.setMasterKey(null);
		employee.setRsaPrivate(null);
		employee.setRsaPublic(null);
		
		this.employeeEJB.saveEmployee(employee);
		
		this.messageEJB.deleteAllMyMessages(uid);
		this.ecdhEJB.deleteAllMyKeys(uid);
		this.ecdhEJB.deleteAllMyPreKeys(uid);
		
		return Response.noContent().build();
	}
}

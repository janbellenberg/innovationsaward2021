package de.internally.api.rest.user;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PATCH;

import java.util.List;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import de.internally.ejb.database.EmployeeEJB;
import de.internally.entities.database.Employee;
import de.internally.helper.security.RsaHelper;
import de.internally.helper.security.SecurePassword;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/employees
 * managing employees
 * @author janbellenberg
 * @see EmployeeEJB
 */
@Singleton
@Path("/employees")
public class EmployeeEndpoint {

	@Inject
	private EmployeeEJB employeeEJB;

	/**
	 * get employees by query
	 * @param query search query
	 * @return HTTP Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getEmployees(@QueryParam("query") String query) {
		
		query = query.trim();
		
		if(query.equals("")) {
			return Response.ok("[]").build();
		}
		
		query = "%" + query + "%";
		List<Employee> employees = this.employeeEJB.getAllByQuery(query);
		JsonArrayBuilder result = Json.createArrayBuilder();
		
		for(Employee e : employees) {
			JsonObjectBuilder employee = Json.createObjectBuilder();
			employee.add("id", e.getId());
			employee.add("username", e.getUsername());
			employee.add("firstname", e.getFirstname());
			employee.add("lastname", e.getLastname());
			
			result.add(employee);
		}
		
		return Response.ok(result.build()).build();
	}

	/**
	 * update the design
	 * @param uid id of the employee
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@PATCH
	@Path("/design")
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response changeDesign(@HeaderParam("X-UID") int uid, JsonObject data) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		employee.setDesign(data.getBoolean("design"));
		this.employeeEJB.saveEmployee(employee);
		return Response.noContent().build();
	}
	
	/**
	 * status the design
	 * @param uid id of the employee
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@PATCH
	@Path("/status")
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response changeStatus(@HeaderParam("X-UID") int uid, JsonObject data) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		employee.setStatus(data.getInt("status"));
		this.employeeEJB.saveEmployee(employee);

		return Response.noContent().build();
	}

	/**
	 * enable / disable 2FA
	 * @param uid id of the employee
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@PATCH
	@Path("/twoFA")
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response changeTwoFA(@HeaderParam("X-UID") int uid, JsonObject data) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		employee.setTwoFA(data.getBoolean("twoFA"));
		this.employeeEJB.saveEmployee(employee);

		return Response.noContent().build();
	}

	/**
	 * update the password
	 * @param uid id of the employee
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@PATCH
	@Path("/password")
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response changePassword(@HeaderParam("X-UID") int uid, JsonObject data) {
		Employee employee = employeeEJB.getEmployee(uid);
		
		// check if user is valid
		if(!SecurePassword.matchPassword(data.getString("old"), employee.getPassword(), employee.getSalt())) {
			return Response.status(Status.NOT_ACCEPTABLE).build();
		}

		String pepper = SecurePassword.generatePepper();
		String password = SecurePassword.generateHash(data.getString("password"), employee.getSalt(), pepper);

		employee.setPassword(password);

		return Response.noContent().build();
	}

	/**
	 * get salt of employee
	 * @param id id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Path("{id}/salt")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getSalt(@PathParam("id") int id) {
		Employee employee = this.employeeEJB.getEmployee(id);
		JsonObjectBuilder data = Json.createObjectBuilder();
		data.add("salt", employee.getSalt());
		data.add("server_signature", RsaHelper.sign(employee.getSalt()));

		return Response.ok(data.build()).build();
	}
}

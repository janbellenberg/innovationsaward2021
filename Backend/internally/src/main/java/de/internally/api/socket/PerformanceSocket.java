package de.internally.api.socket;

import javax.inject.Inject;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import de.internally.ejb.SocketManagerEJB;

/**
 * websocket endpoint for transmitting cpu-, ram-usage and socket count
 * @author janbellenberg
 *
 */
@ServerEndpoint(value = "/socket/performance")
public class PerformanceSocket {
	@Inject
	private SocketManagerEJB sessionRegistry;

	/**
	 * save new session when its opened
	 * @param session the new session
	 * @param conf the configuration of the endpoint
	 */
	@OnOpen
	public void open(Session session, EndpointConfig conf) {
		sessionRegistry.addPerformanceSession(session);
	}

	/**
	 * remove session from list when disconnected
	 * @param session the connection session
	 * @param reason the reason for the close event
	 */
	@OnClose
	public void close(Session session, CloseReason reason) {
		sessionRegistry.removePerformanceSession(session);
	}
}

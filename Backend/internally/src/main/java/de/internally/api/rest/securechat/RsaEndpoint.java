package de.internally.api.rest.securechat;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.internally.ejb.database.EmployeeEJB;
import de.internally.entities.database.Employee;
import de.internally.helper.security.RsaHelper;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/secure_chat/rsa
 * secure chat RSA
 * @author janbellenberg
 * @see EmployeeEJB
 */
@Singleton
@Path("/secure_chat/rsa")
public class RsaEndpoint {
	
	@Inject
	private EmployeeEJB employeeEJB;
	
	/**
	 * get my rsa private + public key
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getRsaPrivateKey(@HeaderParam("X-UID") int uid) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("public", employee.getRsaPublic());
		builder.add("private", employee.getRsaPrivate());
		builder.add("server_signature", RsaHelper.sign(employee.getRsaPrivate()));
		return Response.ok(builder.build()).build();
	}

	/**
	 * update my rsa private + public keys
	 * @param uid id of the employee
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response updateRsaKeys(@HeaderParam("X-UID") int uid, JsonObject data) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		employee.setRsaPrivate(data.getString("private"));
		employee.setRsaPublic(data.getString("public"));
		this.employeeEJB.saveEmployee(employee); 
		return Response.noContent().build();
	}

	/**
	 * get rsa public of other employee
	 * @param uid id of the employee the rsa public key is from
	 * @return HTTP Response
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	@Secured(level = SecurityLevel.USER)
	public Response getRsaPublicKey(@PathParam("id") int uid) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("public", employee.getRsaPublic());
		builder.add("server_signature", RsaHelper.sign(employee.getRsaPublic()));
		return Response.ok(builder.build()).build();
	}
}

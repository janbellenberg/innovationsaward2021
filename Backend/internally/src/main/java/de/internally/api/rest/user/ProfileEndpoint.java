package de.internally.api.rest.user;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PATCH;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;

import javax.ejb.Singleton;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.internally.ejb.SocketManagerEJB;
import de.internally.ejb.database.EmployeeEJB;
import de.internally.entities.ChatSession;
import de.internally.entities.database.Employee;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/profile
 * managing the profiles of the employees
 * @author janbellenberg
 * @see EmployeeEJB
 * @see SocketManagerEJB
 */
@Singleton
@Path("/profile")
public class ProfileEndpoint {

	@Inject
	private EmployeeEJB employeeEJB;
	
	@Inject
	private SocketManagerEJB socketManager;

	/**
	 * get own profile
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getMyProfile(@HeaderParam("X-UID") int uid) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		JsonObjectBuilder result = Json.createObjectBuilder();

		result.add("username", employee.getUsername());
		result.add("firstname", employee.getFirstname());
		result.add("lastname", employee.getLastname());
		result.add("email", employee.getEmail());
		result.add("status", employee.getStatus());
		result.add("message", employee.getStatusNote());
		result.add("department", employee.getDepartment().getName());
		result.add("location", employee.getLocation().getDescription());
		result.add("group", employee.getGroup().getName());

		// if the groups has 2FA enabled the user can't disable it
		if (!employee.getGroup().getTwoFA()) {
			result.add("twoFA", employee.getTwoFA());
		}

		return Response.ok(result.build()).build();
	}

	/**
	 * get profile of employee
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getProfile(@PathParam("id") int uid) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		JsonObjectBuilder result = Json.createObjectBuilder();
		
		int status = 0;
		
		// check if the employee is currently connected
		for(ChatSession s : this.socketManager.getAllChatSessions()) {
			if(s.getJwt().getPayload().getInt("sub") == employee.getId()) {
				status = employee.getStatus();
				break;
			}
		}

		result.add("username", employee.getUsername());
		result.add("firstname", employee.getFirstname());
		result.add("lastname", employee.getLastname());
		result.add("email", employee.getEmail());
		result.add("status", status);
		result.add("message", employee.getStatusNote());
		result.add("department", employee.getDepartment().getName());
		result.add("location", employee.getLocation().getDescription());
		result.add("group", employee.getGroup().getName());
		result.add("rsa_exist", employee.getRsaPrivate() != null && employee.getRsaPublic() != null);
		
		return Response.ok(result.build()).build();
	}

	/**
	 * get profile image of employee
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Path("/{id}/image")
	@Produces("image/jpeg")
	@Secured(level = SecurityLevel.USER)
	public Response getImage(@PathParam("id") int uid) {
		this.employeeEJB.getEmployee(uid);
		
		File file = new File(
				System.getProperty("jboss.server.data.dir") + "/internally/profile-images/" + uid + ".jpg");

		if (!file.exists()) {
			file = new File(System.getProperty("jboss.server.data.dir") + "/internally/profile-default.jpg");
		}

		return Response.ok(file).build();
	}

	/**
	 * update the profile image
	 * @param uid id of the employee
	 * @param stream http request stream for body-data
	 * @return HTTP Response
	 */
	@PATCH
	@Path("/image")
	@Secured(level = SecurityLevel.USER)
	public Response updateImage(@HeaderParam("X-UID") int uid, InputStream stream) {
		this.employeeEJB.getEmployee(uid);

		try {
			// read image
		    BufferedImage image = ImageIO.read(stream);

		    // reduce image size
			int height, width;
		    if(image.getHeight() < image.getWidth()) {
		    	width = (int) ((double)image.getWidth() * (150.0 / (double)image.getHeight()));
		    	height = 150;
		    } else {
		    	height = (int) ((double)image.getHeight() * (150.0 / (double)image.getWidth()));
		    	width = 150;
		    }
		    
		    // get scaled image
		    BufferedImage scaled = new BufferedImage(width, height, image.getType());
		    
		    Graphics2D g2d = scaled.createGraphics();
	        g2d.drawImage(image, 0, 0, width, height, null);
	        g2d.dispose();
		    
	        // create output stream
		    File compressedImageFile = new File(System.getProperty("jboss.server.data.dir") + "/internally/profile-images/" + uid + ".jpg");
		    OutputStream os = new FileOutputStream(compressedImageFile);
		    
		    Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName("jpg");
		    ImageWriter writer = (ImageWriter) writers.next();

		    ImageOutputStream ios = ImageIO.createImageOutputStream(os);
		    writer.setOutput(ios);

		    // compress image quality
		    ImageWriteParam param = writer.getDefaultWriteParam();

		    param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		    param.setCompressionQuality(0.3f);
		    writer.write(null, new IIOImage(scaled, null, null), param);

		    os.close();
		    ios.close();
		    writer.dispose();
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		return Response.noContent().build();
	}

	/**
	 * set profile image to default
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@DELETE
	@Path("/image")
	@Secured(level = SecurityLevel.USER)
	public Response deleteImage(@HeaderParam("X-UID") int uid) {
		File file = new File(
				System.getProperty("jboss.server.data.dir") + "/internally/profile-images/" + uid + ".jpg");

		if (file.exists()) {
			file.delete();
		}

		return Response.noContent().build();
	}

	/**
	 * update status text
	 * @param uid id of the employee
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@PATCH
	@Path("/message")
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response updateStatus(@HeaderParam("X-UID") int uid, JsonObject data) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		employee.setStatusNote(data.getString("message"));
		this.employeeEJB.saveEmployee(employee);
		return Response.noContent().build();
	}
}

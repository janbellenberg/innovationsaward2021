package de.internally.api.rest.administration;

import java.util.List;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.internally.ejb.database.DepartmentEJB;
import de.internally.ejb.database.EmployeeEJB;
import de.internally.ejb.database.GroupEJB;
import de.internally.ejb.database.LocationEJB;
import de.internally.entities.database.Department;
import de.internally.entities.database.Employee;
import de.internally.entities.database.Group;
import de.internally.entities.database.Location;
import de.internally.helper.ConfigurationHelper;
import de.internally.helper.security.SecurePassword;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;
/**
 * REST-API endpoint for /api/v1/administration/employees
 * Employee management
 * @author simonmaddahi
 * @see EmployeeEJB
 * @see DepartmentEJB
 * @see LocationEJB
 * @see GroupEJB
 */
@Singleton
@Path("/administration/employees")
public class EmployeeAdministrationEndpoint {

	@Inject
	private EmployeeEJB employeeEJB;

	@Inject
	private DepartmentEJB departmentEJB;

	@Inject
	private LocationEJB locationEJB;

	@Inject
	private GroupEJB groupEJB;
	
	/**
	 * search all employees
	 * @param username query parameter for username
	 * @param firstname query parameter for firstname
	 * @param lastname query parameter for lastname
	 * @param email query parameter for email
	 * @param department query parameter for department (0 = all)
	 * @param location query parameter for location (0 = all)
	 * @param group query parameter for group (0 = all)
	 * @return HTTP Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response getEmployee(
			@QueryParam("username") String username, 
			@QueryParam("firstname") String firstname, 
			@QueryParam("lastname") String lastname, 
			@QueryParam("email") String email, 
			@QueryParam("department") int department, 
			@QueryParam("location") int location, 
			@QueryParam("group") int group) {
		
		JsonArrayBuilder builder = Json.createArrayBuilder();
		List<Employee> employees = this.employeeEJB.filterAll(username, firstname, lastname, email, department, location, group);
		
		for(Employee e : employees) {
			JsonObjectBuilder request = Json.createObjectBuilder();
			request.add("id", e.getId());
			request.add("username", e.getUsername());
			request.add("firstname", e.getFirstname());
			request.add("lastname", e.getLastname());
			request.add("email", e.getEmail());
			
			request.add("department", e.getDepartment().getId());
			request.add("location", e.getLocation().getId());
			request.add("group", e.getGroup().getId());
			
			builder.add(request.build());
		}
		
		return Response.ok(builder.build()).build();
	}

	/**
	 * add an employee
	 * @param request parsed request data (application/json)
	 * @return HTTP Response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response addEmployee(JsonObject request) {

		Department department = this.departmentEJB.getDepartment(request.getInt("department"));
		Location location = this.locationEJB.getLocation(request.getInt("location"));
		Group group = this.groupEJB.getGroup(request.getInt("group"));
		
		List<String> salts = this.employeeEJB.getSalts();

		String salt = SecurePassword.generateSalt(salts);
		String pepper = SecurePassword.generatePepper();
		String password = SecurePassword.generateHash(ConfigurationHelper.getDefaultPassword(), salt, pepper);

		Employee employee = new Employee();
		employee.setLastname(request.getString("lastname"));
		employee.setFirstname(request.getString("firstname"));
		employee.setUsername(request.getString("username"));
		employee.setEmail(request.getString("email"));

		employee.setDepartment(department);
		employee.setLocation(location);
		employee.setGroup(group);

		employee.setPassword(password);
		employee.setSalt(salt);
		employee.setTwoFA(false);

		employee.setStatus(1);
		employee.setStatusNote("Hey there, I'm using internally!");
		employee.setDesign(false);
		
		if(!this.verifyEmail(employee.getEmail())) {
			throw new NotAcceptableException();
		}

		employeeEJB.saveEmployee(employee);
		return Response.created(null).build();
	}

	/**
	 * update an employee
	 * @param id id of the employee
	 * @param request parsed request data (application/json)
	 * @return HTTP Response
	 */
	@Path("/{id}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response editUser(@PathParam("id") int id, JsonObject request) {

		Employee employee = this.employeeEJB.getEmployee(Integer.valueOf(id));

		Department department = this.departmentEJB.getDepartment(request.getInt("department"));
		Location location = this.locationEJB.getLocation(request.getInt("location"));
		Group group = this.groupEJB.getGroup(request.getInt("group"));

		employee.setLastname(request.getString("lastname"));
		employee.setFirstname(request.getString("firstname"));
		employee.setUsername(request.getString("username"));
		employee.setEmail(request.getString("email"));

		employee.setDepartment(department);
		employee.setLocation(location);
		employee.setGroup(group);
		
		if(!this.verifyEmail(employee.getEmail())) {
			throw new NotAcceptableException();
		}

		employeeEJB.saveEmployee(employee);
		return Response.noContent().build();
	}

	/**
	 * delete an employee
	 * @param id id of the employee
	 * @return HTTP Response
	 */
	@Path("/{id}")
	@DELETE
	@Secured(level = SecurityLevel.ADMIN)
	public Response dropUser(@PathParam("id") int id) {

		Employee employee = employeeEJB.getEmployee(id);
		employeeEJB.deleteEmployee(employee);
		return Response.noContent().build();
	}
	
	/**
	 * set the password of an employee to default
	 * @param id id of the employee
	 * @return HTTP Response
	 */
	@PATCH
	@Path("/{id}/password/default")
	@Secured(level = SecurityLevel.ADMIN)
	public Response resetPassword(@PathParam("id") int id) {
		Employee employee = employeeEJB.getEmployee(id);
		
		List<String> salts = this.employeeEJB.getSalts();

		String salt = SecurePassword.generateSalt(salts);
		String pepper = SecurePassword.generatePepper();
		String password = SecurePassword.generateHash(ConfigurationHelper.getDefaultPassword(), salt, pepper);
		
		employee.setPassword(password);
		employee.setSalt(salt);
		employeeEJB.saveEmployee(employee);
		
		return Response.noContent().build();
	}
	
	/**
	 * verify email address via regex
	 * @param email input string
	 * @return if format is valid
	 */
	private boolean verifyEmail(String email) {
		return email.matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-zA-Z0-9.-]$");
	}
}

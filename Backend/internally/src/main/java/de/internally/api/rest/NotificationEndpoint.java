package de.internally.api.rest;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.POST;

import java.util.List;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import de.internally.ejb.database.EmployeeEJB;
import de.internally.ejb.database.NotificationEJB;
import de.internally.entities.database.Employee;
import de.internally.entities.database.Notification;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/notifications
 * management of notifications
 * @author janbellenberg
 * @see EmployeeEJB
 * @see NotificationEJB
 */
@Path("/notifications")
public class NotificationEndpoint {
	
	@Inject
	private EmployeeEJB employeeEJB;
	
	@Inject 
	private NotificationEJB notificationEJB;
	
	/**
	 * get list of notifications that are visible for the employee
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.BOTH)
	public Response getNotifications (@HeaderParam("X-UID") int uid) {
		if(uid == 0) {
			return Response.ok("[]").build();
		}
		
		Employee employee = this.employeeEJB.getEmployee(uid);
		List<Notification> notifications = this.notificationEJB.getNotficiations(employee.getDepartment().getId(), employee.getLocation().getId());
		
		JsonArrayBuilder result = Json.createArrayBuilder();
		for(Notification n : notifications) {
			JsonObjectBuilder notification = Json.createObjectBuilder();
			JsonObjectBuilder author = Json.createObjectBuilder();
			author.add("id", n.getEmployee().getId());
			author.add("firstname", n.getEmployee().getFirstname());
			author.add("lastname", n.getEmployee().getLastname());
			
			notification.add("id", n.getId());
			notification.add("content", n.getContent());
			notification.add("author", author.build());
			
			result.add(notification.build());
		}
		
		return Response.ok(result.build()).build();
	}
	
	/**
	 * add notification
	 * @param uid id of the employee
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response addNotification (@HeaderParam("X-UID") int uid, JsonObject data) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		
		Notification n = new Notification();
		n.setContent(data.getString("content"));
		n.setSameDepartment(data.getBoolean("same_department"));
		n.setSameLocation(data.getBoolean("same_location"));
		n.setEmployee(employee);
		
		this.notificationEJB.saveNotification(n);
		return Response.status(Status.CREATED).build();
	}
	
	/**
	 * delete a notification
	 * @param id id of the notification
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@DELETE
	@Path("/{id}")
	@Secured(level = SecurityLevel.USER)
	public Response deleteNotification(@PathParam("id") int id, @HeaderParam("X-UID") int uid) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		Notification notification = this.notificationEJB.getNotficiation(id);
		
		// you can't delete notifications from other employees
		if(notification.getEmployee().getId() != employee.getId()) {
			throw new NotAuthorizedException(null);
		}
		
		this.notificationEJB.deleteNotification(notification);
		return Response.noContent().build();
	}
}

package de.internally.api.rest.authentication;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import de.internally.ejb.database.EmployeeEJB;
import de.internally.entities.database.Employee;
import de.internally.helper.ConfigurationHelper;
import de.internally.helper.MailHelper;
import de.internally.helper.security.JWT;
import de.internally.helper.security.SecurePassword;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;
import de.internally.helper.security.TwoFactorAuth;
import ua_parser.Client;
import ua_parser.Parser;

/**
 * REST-API endpoint for /api/v1/authentication
 * login / logout
 * @author jan.bellenberg
 * @see ua_parser
 * @see EmployeeEJB
 * @see TwoFactorAuth
 *
 */
@Singleton
@Path("/authentication")
public class AuthenticationEndpoint {

	@Inject
	private TwoFactorAuth twoFA;

	@Inject
	private EmployeeEJB employeeEJB;

	/**
	 * perform login process
	 * @param request HTTP-Request
	 * @param agent HTTP Header with user agent
	 * @param loginData parsed request data (application/json)
	 * @return HTTP Response
	 */
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doLogin(@Context HttpServletRequest request, @HeaderParam("User-Agent") String agent, JsonObject loginData) {

		String username = loginData.getString("username");
		String password = loginData.getString("password");

		// for admin user from config file
		if (username.equals("admin")) {
			JsonObject adminData = ConfigurationHelper.getAdmin();
			boolean valid = SecurePassword.matchPassword(password, adminData.getString("hash"),
					adminData.getString("salt"));

			if (!valid)
				return Response.status(Status.UNAUTHORIZED).build();
			else {
				String jwt = JWT.generateForAdmin().toString();
				String maxAge = String.valueOf(30 * 60); // valid for 30 minutes
				String cookie = AuthenticationFilter.COOKIE_NAME + "=" + jwt + "; ";
				cookie += "HttpOnly; ";						// XSS-Protection: javascript has no access to this cookie
				cookie += "SameSite=strict; ";				// don't send cookie to other websites
				cookie += "Path=/; ";						// send for hole page
				cookie += "Max-Age=" + maxAge + "; ";		// set expires
				
				if(request.isSecure())
					cookie += "Secure; ";					// mark as secure, if the request is secure

				return Response.ok().header("Set-Cookie", cookie).build();
			}
		}

		// get user
		Employee user = this.employeeEJB.getEmployee(username);

		// check if 2FA is required and possible
		if ((user.getTwoFA() || user.getGroup().getTwoFA()) && ConfigurationHelper.isTwoFactorAuthEnabled()) {

			if (!loginData.containsKey("twoFA")) {
				String data = this.twoFA.start(user.getUsername());
				this.sendTwoFAMail(user.getEmail(), data, agent);
				
				// if verifications code is missing
				return Response.status(Status.NOT_ACCEPTABLE).build();

			} else if (!this.twoFA.validate(user.getUsername(), loginData.getString("twoFA"))) {
				return Response.status(Status.UNAUTHORIZED).build();

			}
		}

		// check password
		if (!SecurePassword.matchPassword(password, user.getPassword(), user.getSalt())) {
			return Response.status(Status.UNAUTHORIZED).build();
		}

		// send notification mail
		if (ConfigurationHelper.isLoginNotificationEnabled()) {
			this.sendLoginMail(user.getEmail(), agent);
		}

		// build jwt
		String jwt = JWT.generate(user).toString();
		String maxAge = String.valueOf(1 * 365 * 24 * 60 * 60); // valid for one year
		String cookie = AuthenticationFilter.COOKIE_NAME + "=" + jwt + "; ";
		cookie += "HttpOnly; ";						// XSS-Protection: javascript has no access to this cookie
		cookie += "SameSite=strict; ";				// don't send cookie to other websites
		cookie += "Path=/; ";						// send for hole page
		cookie += "Max-Age=" + maxAge + "; ";		// set expires
		
		if(request.isSecure())
			cookie += "Secure; ";					// mark as secure, if the request is secure

		return Response.ok().header("Set-Cookie", cookie).build();
	}
	
	/**
	 * check credentials without login (for secure chat)
	 * @param uid id of the employee
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@POST
	@Path("/check")
	@Secured(level = SecurityLevel.USER)
	public Response checkCredentials(@HeaderParam("X-UID") int uid, JsonObject data) {
		Employee employee = employeeEJB.getEmployee(uid);
		if(!SecurePassword.matchPassword(data.getString("password"), employee.getPassword(), employee.getSalt())) {
			return Response.status(Status.UNAUTHORIZED).build();
		}
		
		return Response.noContent().build();
	}
	
	/**
	 * perform logout
	 * @return HTTP Response
	 */
	@DELETE
	@Path("/logout")
	@Secured(level = SecurityLevel.BOTH)
	public Response doLogout() {
		return Response.ok()
				.header("Set-Cookie", AuthenticationFilter.COOKIE_NAME + "=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT")
				.build();
	}

	/**
	 * send mail for 2FA
	 * @param reciever receiver of the mail
	 * @param data verification code
	 * @param agent user agent of the login request
	 */
	private void sendTwoFAMail(String reciever, String data, String agent) {
		try {
			String mail = this.getMailPreset("2fa.html");

			agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36";
			String browser = this.parseBrowserType(agent);
			String os = this.parseOSType(agent);

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
			String timestamp = LocalDateTime.now().format(dtf);

			mail = mail.replaceAll("\\{id\\}", data);
			mail = mail.replaceAll("\\{user-agent\\}", browser + " - " + os);
			mail = mail.replaceAll("\\{timestamp\\}", timestamp);

			MailHelper.sendMail(reciever, "internally Verifikationcode", mail);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * send login notification
	 * @param reciever receiver of the email
	 * @param agent user agent of the login request
	 */
	private void sendLoginMail(String reciever, String agent) {
		try {
			String mail = this.getMailPreset("login.html");

			agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36";
			String browser = this.parseBrowserType(agent);
			String os = this.parseOSType(agent);

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
			String timestamp = LocalDateTime.now().format(dtf);

			mail = mail.replaceAll("\\{user-agent\\}", browser + " - " + os);
			mail = mail.replaceAll("\\{timestamp\\}", timestamp);

			MailHelper.sendMail(reciever, "Neue Anmeldung bei internally", mail);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * load mail preset from file
	 * @param filename name of the preset file
	 * @return content of the preset
	 * @throws FileNotFoundException when the specified file doesn't exist
	 */
	private String getMailPreset(String filename) throws FileNotFoundException {
		String cnt = "";
		File file = new File(System.getProperty("jboss.server.data.dir") + "/internally/mail-presets/" + filename);
		Scanner scanner = new Scanner(new FileInputStream(file), StandardCharsets.UTF_8);
		while (scanner.hasNextLine()) {
			cnt += scanner.nextLine();
		}
		scanner.close();
		return cnt;
	}

	/**
	 * parse browser
	 * @param ua raw user agent string from HTTP-Request
	 * @return parsed string
	 */
	private String parseBrowserType(String ua) {
		Parser uaParser = new Parser();
		Client c = uaParser.parse(ua);
		return c.userAgent.family + " " + c.userAgent.major;
	}

	/**
	 * parse os
	 * @param ua raw user agent string from HTTP-Request
	 * @return parsed string
	 */
	private String parseOSType(String ua) {
		Parser uaParser = new Parser();
		Client c = uaParser.parse(ua);
		return c.os.family + " " + c.os.major;
	}
}

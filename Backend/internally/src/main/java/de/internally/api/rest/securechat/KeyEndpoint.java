package de.internally.api.rest.securechat;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.internally.ejb.database.EcdhEJB;
import de.internally.ejb.database.EmployeeEJB;
import de.internally.entities.database.Employee;
import de.internally.entities.database.Key;
import de.internally.helper.security.RsaHelper;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/secure_chat/keys
 * secure chat Keys
 * @author janbellenberg
 * @see EcdhEJB
 * @see EmployeeEJB
 */
@Singleton
@Path("/secure_chat/key")
public class KeyEndpoint {

	@Inject
	private EcdhEJB ecdhEJB;
	
	@Inject
	private EmployeeEJB employeeEJB;
  
	/**
	 * get the key for sending
	 * @param peer id of the communication partner
	 * @param owner id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Path("/{id}/send")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getSendKey(@PathParam("id") int peer, @HeaderParam("X-UID") int owner) {
		Key key = this.ecdhEJB.getKey(owner, peer, true);
		
		if(key == null) {
			return Response.status(416).build();
		}
		
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("key", key.getContent());
		builder.add("signature", key.getSignature());
		builder.add("server_signature", RsaHelper.sign(key.getContent()));
		builder.add("usages", key.getUsages());
		
		return Response.ok(builder.build()).build();
	}

	/**
	 * update the key for sending
	 * @param peer id of the communication partner
	 * @param owner id of the employee
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@PUT
	@Path("/{id}/send")
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response updateSendKey(@PathParam("id") int peer, @HeaderParam("X-UID") int owner, JsonObject data) {
		Key key = this.ecdhEJB.getKey(owner, peer, true);
		Employee employee = this.employeeEJB.getEmployee(owner);
		
		// when adding a new key
		if(key == null) {
			key = new Key();
			key.setOwner(employee);
			key.setPeer(this.employeeEJB.getEmployee(peer));
			key.setSend(true);
		}
		
		key.setContent(data.getString("key"));
		key.setSignature(data.getString("signature"));
		key.setUsages(data.getInt("usages"));
		
		if(!RsaHelper.verify(key.getContent(), key.getSignature(), employee.getRsaPublic())) {
			throw new NotAuthorizedException(null);
		}
		
		this.ecdhEJB.saveKey(key);
		return Response.ok().build();
	}
	
	/**
	 * get the key for receiving
	 * @param peer id of the communication partner
	 * @param owner id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Path("/{id}/receive")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getReceiveKey(@PathParam("id") int peer, @HeaderParam("X-UID") int owner) {
		Key key = this.ecdhEJB.getKey(owner, peer, false);
		
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("key", key.getContent());
		builder.add("signature", key.getSignature());
		builder.add("server_signature", RsaHelper.sign(key.getContent()));
		builder.add("usages", key.getUsages());
		
		return Response.ok(builder.build()).build();
	}
	
	/**
	 * update the key for receiving
	 * @param peer id of the communication partner
	 * @param owner id of the employee
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@PUT
	@Path("/{id}/receive")
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response updateReceiveKey(@PathParam("id") int peer, @HeaderParam("X-UID") int owner, JsonObject data) {
		Key key = this.ecdhEJB.getKey(owner, peer, false);
		Employee employee = this.employeeEJB.getEmployee(owner);
		
		// when adding a new key
		if(key == null) {
			key = new Key();
			key.setOwner(employee);
			key.setPeer(this.employeeEJB.getEmployee(peer));
			key.setSend(false);
		}
		
		key.setContent(data.getString("key"));
		key.setSignature(data.getString("signature"));
		key.setUsages(data.getInt("usages"));
		
		if(!RsaHelper.verify(key.getContent(), key.getSignature(), employee.getRsaPublic())) {
			throw new NotAuthorizedException(null);
		}
		
		this.ecdhEJB.saveKey(key);
		return Response.ok().build();
	}
}
package de.internally.api.rest.securechat;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.internally.ejb.database.EmployeeEJB;
import de.internally.entities.database.Employee;
import de.internally.helper.security.RsaHelper;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/secure_chat/master
 * secure chat Master Keys
 * @author janbellenberg
 * @see EmployeeEJB
 */
@Singleton
@Path("/secure_chat/master")
public class MasterKeyEndpoint {

	@Inject
	private EmployeeEJB employeeEJB;
	
	/**
	 * get my master-key
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getMasterKey(@HeaderParam("X-UID") int uid) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		
		if(employee.getMasterKey() == null) {
			return Response.noContent().build();
		}
		
		JsonObjectBuilder result = Json.createObjectBuilder();
		result.add("master_key", employee.getMasterKey());
		result.add("server_signature", RsaHelper.sign(employee.getMasterKey()));
		return Response.ok(result.build()).build();
	}
	
	/**
	 * update my master-key
	 * @param uid id of the employee
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@PATCH
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response updateMasterKey(@HeaderParam("X-UID") int uid, JsonObject data) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		employee.setMasterKey(data.getString("master_key"));
		this.employeeEJB.saveEmployee(employee);
		
		return Response.noContent().build();
	}

}

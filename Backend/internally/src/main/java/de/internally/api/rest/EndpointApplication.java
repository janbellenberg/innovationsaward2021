package de.internally.api.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * initialize a jax-rs application
 * @author janbellenberg
 *
 */
@ApplicationPath("/api/v1")
public class EndpointApplication extends Application {
}

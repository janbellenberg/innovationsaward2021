package de.internally.api.rest.user;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import javax.ws.rs.POST;
import javax.ws.rs.PATCH;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;

import de.internally.ejb.database.ChatEJB;
import de.internally.ejb.database.EmployeeEJB;
import de.internally.ejb.database.HighlightsEJB;
import de.internally.ejb.database.MemberEJB;
import de.internally.ejb.database.MessageEventsEJB;
import de.internally.ejb.database.PlainMessageEJB;
import de.internally.entities.database.Chat;
import de.internally.entities.database.Employee;
import de.internally.entities.database.Highlight;
import de.internally.entities.database.Member;
import de.internally.entities.database.PlainMessage;
import de.internally.entities.database.ReadEvent;
import de.internally.entities.database.ReceivedEvent;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/chat
 * management of chats
 * @author janbellenberg
 * @see EmployeeEJB
 * @see ChatEJB
 * @see MemberEJB
 * @see PlainMessageEJB
 * @see HighlightsEJB
 * @see MessageEventsEJB
 */
@Singleton
@Path("/chat")
public class ChatEndpoint {
	
	@Inject
	private EmployeeEJB employeeEJB;
	
	@Inject
	private ChatEJB chatEJB;
	
	@Inject
	private MemberEJB memberEJB;
	
	@Inject
	private PlainMessageEJB messageEJB;
	
	@Inject
	private HighlightsEJB highlightEJB;
	
	@Inject
	private MessageEventsEJB eventsEJB;
	
	/**
	 * get all chats of employee
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getChats(@HeaderParam("X-UID") int uid) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		List<Chat> chats = this.chatEJB.getChats(employee.getId());
		
		JsonArrayBuilder result = Json.createArrayBuilder();
		
		for(Chat c : chats) {
			Member m = this.memberEJB.getMember(c.getId(), employee.getId());
			JsonObjectBuilder chat = Json.createObjectBuilder();
			
			chat.add("id", c.getId());
			chat.add("name", c.getName());
			
			// last message
			if(c.getPlainMessages().size() > 0) {
				PlainMessage last = c.getPlainMessages().get(c.getPlainMessages().size() - 1);
				if(last.getTimestamp().compareTo(m.getMemberSince()) > 0) {
					chat.add("last", last.getContent());
				
					// check if is unread
					ReadEvent read = this.eventsEJB.getReadEvent(last.getId(), m.getEmployee().getId());
					chat.add("unread", read == null);
				} else {
					chat.add("last", "Willkommen im Chat!");
					chat.add("unread", false);
				}
			} else {
				chat.add("last", "");
				chat.add("unread", false);
			}
			
			result.add(chat.build());
		}
		
		return Response.ok(result.build()).build();
	}
	
	/**
	 * get messages of a chat
	 * @param uid id of the employee
	 * @param last id of the oldest message that was already loaded
	 * @param chatId id of the chat
	 * @return HTTP Response
	 */
	@GET
	@Path("/{id}/messages")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getMessages(@HeaderParam("X-UID") int uid, @QueryParam("last") Integer last, @PathParam("id") int chatId) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		Chat chat = this.chatEJB.getChat(chatId);
		this.checkIfEmployeeIsMember(chat, employee);
		
		int lastIndex = last == null ? 0 : last;
		
		JsonArrayBuilder result = Json.createArrayBuilder();
		for(PlainMessage m : this.messageEJB.get(lastIndex, chat.getId())) {
			
			// dont show messages from before employee is member
			if(m.getTimestamp().compareTo(this.memberEJB.getMember(chat.getId(), employee.getId()).getMemberSince()) < 0) {
				continue;
			}

			// check if received
			ReceivedEvent received = this.eventsEJB.getReceivedEvent(m.getId(), employee.getId());
			if(received == null) {
				received = new ReceivedEvent();
				received.setPlainMessage(m);
				received.setMember(this.memberEJB.getMember(chat.getId(), employee.getId()));
				received.setTimestamp(new Date());
				this.eventsEJB.saveReceivedEvent(received);
			}
			
			// check if read
			ReadEvent read = this.eventsEJB.getReadEvent(m.getId(), employee.getId());
			if(read == null) {
				read = new ReadEvent();
				read.setPlainMessage(m);
				read.setMember(this.memberEJB.getMember(chat.getId(), employee.getId()));
				read.setTimestamp(new Date());
				this.eventsEJB.saveReadEvent(read);
			}
			
			// build json
			JsonObjectBuilder message = Json.createObjectBuilder();
			JsonObjectBuilder sender = Json.createObjectBuilder();
			
			if(m.getMember() != null) {
				sender.add("id", m.getMember().getEmployee().getId());
				sender.add("firstname", m.getMember().getEmployee().getFirstname());
				sender.add("lastname", m.getMember().getEmployee().getLastname());
				message.add("sender", sender.build());
				
				Highlight h = this.highlightEJB.getHighlight(m.getId(), m.getMember().getId());
				message.add("highlight", h != null);
			} else {
				// if sender is currently not a member
				sender.add("id", 0);
				sender.add("firstname", "");
				sender.add("lastname", "");
				message.add("sender", sender.build());
				
				message.add("highlight", false);
			}
			
			message.add("id", m.getId());
			message.add("content", m.getContent());
			
			result.add(message.build());
		}
		
		return Response.ok(result.build()).build();
	}
	
	/**
	 * add a new chat
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response addChat(@HeaderParam("X-UID") int uid) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		Chat chat = new Chat();
		chat.setName("Neuer Chat");
		chat = this.chatEJB.saveChat(chat);
		
		Member member = new Member();
		member.setChat(chat);
		member.setEmployee(employee);
		member.setMemberSince(new Date());
		this.memberEJB.saveMember(member);
		
		JsonObjectBuilder result = Json.createObjectBuilder();
		result.add("id", chat.getId());
		
		return Response
				.created(URI.create("/api/v1/chat/" + chat.getId()))
				.entity(result.build())
				.build();
	}
	
	/**
	 * rename a chat
	 * @param uid id of the employee
	 * @param chatId id of the chat
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@PATCH
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response renameChat(@HeaderParam("X-UID") int uid, @PathParam("id") int chatId, JsonObject data) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		Chat chat = this.chatEJB.getChat(chatId);
		this.checkIfEmployeeIsMember(chat, employee);
		
		chat.setName(data.getString("title"));
		this.chatEJB.saveChat(chat);
		return Response.status(Status.NO_CONTENT).build();
	}
	
	/**
	 * delete a chat
	 * @param uid id of the employee
	 * @param chatId id of the chat
	 * @return HTTP Response
	 */
	@DELETE
	@Path("/{id}")
	@Secured(level = SecurityLevel.USER)
	public Response deleteChat(@HeaderParam("X-UID") int uid, @PathParam("id") int chatId) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		Chat chat = this.chatEJB.getChat(chatId);
		this.checkIfEmployeeIsMember(chat, employee);
		
		this.chatEJB.deleteChat(chat);
		return Response.status(Status.NO_CONTENT).build();
	}
	
	/**
	 * get all members of the chat
	 * @param uid id of the employee
	 * @param chatId id of the chat
	 * @return HTTP Response
	 */
	@GET
	@Path("/{id}/members")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getMembers(@HeaderParam("X-UID") int uid, @PathParam("id") int chatId) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		Chat chat = this.chatEJB.getChat(chatId);
		this.checkIfEmployeeIsMember(chat, employee);
		
		List<Member> members = this.memberEJB.getAll(chat.getId());
		JsonArrayBuilder result = Json.createArrayBuilder();
		
		for(Member m : members) {
			JsonObjectBuilder member = Json.createObjectBuilder();
			member.add("id", m.getEmployee().getId());
			member.add("firstname", m.getEmployee().getFirstname());
			member.add("lastname", m.getEmployee().getLastname());
			
			result.add(member.build());
		}
		
		return Response.ok(result.build()).build();
	}
	
	/**
	 * add member to chat
	 * @param uid id of the employee
	 * @param chatId id of the chat
	 * @param newMemberId id of the employee that should be added
	 * @return HTTP Response
	 */
	@POST
	@Path("/{id}/employee/{username}")
	@Secured(level = SecurityLevel.USER)
	public Response addMember(@HeaderParam("X-UID") int uid, @PathParam("id") int chatId, @PathParam("username") String newMemberId) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		Chat chat = this.chatEJB.getChat(chatId);
		this.checkIfEmployeeIsMember(chat, employee);

		Member m = new Member();
		m.setChat(chat);
		m.setEmployee(this.employeeEJB.getEmployee(newMemberId));
		m.setMemberSince(new Date());
		this.memberEJB.saveMember(m);
		
		return Response.status(Status.CREATED).build();
	}
	
	/**
	 * remove member of chat
	 * @param uid id of the employee
	 * @param chatId id of the employee
	 * @param newMember id of the member that should be removed
	 * @return HTTP Response
	 */
	@DELETE
	@Path("/{id}/employee/{uid}")
	@Secured(level = SecurityLevel.USER)
	public Response deleteMember(@HeaderParam("X-UID") int uid, @PathParam("id") int chatId, @PathParam("uid") int newMember) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		Chat chat = this.chatEJB.getChat(chatId);
		this.checkIfEmployeeIsMember(chat, employee);
		
		Employee member = this.employeeEJB.getEmployee(newMember);
		Member m = this.memberEJB.getMember(chat.getId(), member.getId());
		this.memberEJB.deleteMember(m);
		
		return Response.status(Status.NO_CONTENT).build();
	}
	
	/**
	 * get the pinned message in the chat
	 * @param uid id of the employee
	 * @param chatId id of the chat
	 * @return HTTP Response
	 */
	@GET
	@Path("/{id}/pin")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getPinnedMessage(@HeaderParam("X-UID") int uid, @PathParam("id") int chatId) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		Chat chat = this.chatEJB.getChat(chatId);
		this.checkIfEmployeeIsMember(chat, employee);
		
		if(chat.getPinned() == null) {
			return Response.noContent().build();
		}
		
		JsonObjectBuilder pinned = Json.createObjectBuilder();
		JsonObjectBuilder pinnedSender = Json.createObjectBuilder();
		
		pinned.add("id", chat.getPinned().getId());
		pinned.add("content", chat.getPinned().getContent());
		
		if(chat.getPinned().getMember() == null) {
			// if the sender not currently not a member
			pinnedSender.add("id", 0);
			pinnedSender.add("firstname", "");
			pinnedSender.add("lastname", "");
		} else {
			pinnedSender.add("id", chat.getPinned().getMember().getEmployee().getId());
			pinnedSender.add("firstname", chat.getPinned().getMember().getEmployee().getFirstname());
			pinnedSender.add("lastname", chat.getPinned().getMember().getEmployee().getLastname());
		}
			
		pinned.add("sender", pinnedSender.build());
		
		return Response.ok(pinned.build()).build();
	}
	
	/**
	 * pin the message in the chat
	 * @param uid id of the employee
	 * @param messageId id of the message
	 * @return HTTP Response
	 */
	@POST
	@Path("/message/{id}/pin")
	@Secured(level = SecurityLevel.USER)
	public Response pinMessage(@HeaderParam("X-UID") int uid, @PathParam("id") int messageId) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		PlainMessage message = this.messageEJB.getMessage(messageId);
		Chat chat = this.chatEJB.getChat(message.getChat().getId());
		this.checkIfEmployeeIsMember(chat, employee);
		
		chat.setPinned(message);
		this.chatEJB.saveChat(chat);
		
		return Response.status(Status.NO_CONTENT).build();
	}
	
	/**
	 * unpin message from the chat
	 * @param uid id of the employee
	 * @param chatId id of the chat
	 * @return HTTP Response
	 */
	@DELETE
	@Path("/{id}/pin")
	@Secured(level = SecurityLevel.USER)
	public Response unpinMessage(@HeaderParam("X-UID") int uid, @PathParam("id") int chatId) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		Chat chat = this.chatEJB.getChat(chatId);
		this.checkIfEmployeeIsMember(chat, employee);
		
		chat.setPinned(null);
		this.chatEJB.saveChat(chat);
		
		return Response.status(Status.NO_CONTENT).build();
	}
	
	/**
	 * get all highlighted message of the employee
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Path("/highlights")
	@Secured(level = SecurityLevel.USER)
	public Response getSavedMessages(@HeaderParam("X-UID") int uid) {
		List<Highlight> highlights = this.highlightEJB.getHighlights(uid);
		JsonArrayBuilder result = Json.createArrayBuilder();
		
		for(Highlight h : highlights) {
			JsonObjectBuilder message = Json.createObjectBuilder();
			JsonObjectBuilder sender = Json.createObjectBuilder();
			if(h.getPlainMessage().getMember() == null) {
				// if the sender is currently no member of the chat
				sender.add("id", 0);
				sender.add("firstname", "");
				sender.add("lastname", "");
			} else {
				sender.add("id", h.getPlainMessage().getMember().getEmployee().getId());
				sender.add("firstname", h.getPlainMessage().getMember().getEmployee().getFirstname());
				sender.add("lastname", h.getPlainMessage().getMember().getEmployee().getLastname());
			}
			
			message.add("id", h.getPlainMessage().getId());
			message.add("content", h.getPlainMessage().getContent());
			message.add("sender", sender.build());
			result.add(message.build());
		}
		
		return Response.ok(result.build()).build();
	}
	
	/**
	 * highlight message
	 * @param uid id of the employee
	 * @param messageId id of the message
	 * @return HTTP Response
	 */
	@POST
	@Path("/message/{id}/highlight")
	@Secured(level = SecurityLevel.USER)
	public Response saveMessage(@HeaderParam("X-UID") int uid, @PathParam("id") int messageId) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		PlainMessage message = this.messageEJB.getMessage(messageId);
		Chat chat = this.chatEJB.getChat(message.getChat().getId());
		this.checkIfEmployeeIsMember(chat, employee);
		
		Highlight h = new Highlight();
		Member m = this.memberEJB.getMember(chat.getId(), employee.getId());
		h.setMember(m);
		h.setPlainMessage(message);
		this.highlightEJB.saveHighlight(h);
		
		return Response.status(Status.NO_CONTENT).build();
	}
	
	/**
	 * remove message from highlights
	 * @param uid id of the employee
	 * @param messageId id of the message
	 * @return HTTP Response
	 */
	@DELETE
	@Path("/message/{id}/highlight")
	@Secured(level = SecurityLevel.USER)
	public Response unsaveMessage(@HeaderParam("X-UID") int uid, @PathParam("id") int messageId) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		PlainMessage message = this.messageEJB.getMessage(messageId);
		Chat chat = this.chatEJB.getChat(message.getChat().getId());
		this.checkIfEmployeeIsMember(chat, employee);
		
		Member m = this.memberEJB.getMember(chat.getId(), employee.getId());
		Highlight h = this.highlightEJB.getHighlight(message.getId(), m.getId());
		this.highlightEJB.deleteHighlight(h);
		
		return Response.status(Status.NO_CONTENT).build();
	}
	
	/**
	 * get details of the message
	 * @param uid id of the employee
	 * @param messageId id of the message
	 * @return HTTP Response
	 */
	@GET
	@Path("/message/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getMessageDetails(@HeaderParam("X-UID") int uid, @PathParam("id") int messageId) {
		Employee employee = this.employeeEJB.getEmployee(uid);
		PlainMessage message = this.messageEJB.getMessage(messageId);
		Chat chat = this.chatEJB.getChat(message.getChat().getId());
		this.checkIfEmployeeIsMember(chat, employee);
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		
		JsonObjectBuilder result = Json.createObjectBuilder();
		result.add("content", message.getContent());
		result.add("edited", message.getEdited());
		if(message.getMember() == null) {
			// if the sender is currently no member
			result.add("my", false);
		} else {
			result.add("my", message.getMember().getEmployee().getId() == uid);
		}
		result.add("timestamp", formatter.format(message.getTimestamp()));
		
		JsonArrayBuilder receivedList = Json.createArrayBuilder();
		// get received and read events
		for(ReceivedEvent recv : this.eventsEJB.getReceivedEvents(message.getId())) {
			JsonObjectBuilder received = Json.createObjectBuilder();
			received.add("uid", recv.getMember().getEmployee().getId());
			received.add("firstname", recv.getMember().getEmployee().getFirstname());
			received.add("lastname", recv.getMember().getEmployee().getLastname());
			

			JsonObjectBuilder timestamps = Json.createObjectBuilder();
			timestamps.add("received", formatter.format(recv.getTimestamp()));
			ReadEvent read = this.eventsEJB.getReadEvent(message.getId(), recv.getMember().getEmployee().getId());
			if(read != null) {
				timestamps.add("read", formatter.format(read.getTimestamp()));
			}
			
			received.add("timestamp", timestamps.build());
			receivedList.add(received.build());
		}
		
		result.add("received", receivedList.build());
		
		return Response.ok(result.build()).build();
	}
	
	/**
	 * check if the employee is a member of the chat
	 * @param chat id of the chat
	 * @param employee id of the employee
	 */
	private void checkIfEmployeeIsMember(Chat chat, Employee employee) {
		// check if employee is a member
		try {
			boolean isMember = false;
			for(Member m : employee.getMemberIn()) {
				if(m.getChat().getId() == chat.getId()) {
					isMember = true;
					break;
				}
			}
			
			if(!isMember) {
				throw new NotAuthorizedException(null);
			}
		}catch(NullPointerException exception) {
			throw new NotAuthorizedException(null);
		}
	}
}

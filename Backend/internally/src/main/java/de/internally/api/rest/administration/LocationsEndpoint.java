package de.internally.api.rest.administration;

import java.util.List;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.internally.ejb.database.LocationEJB;
import de.internally.entities.database.Location;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/administration/locations
 * location management
 * @author simonmaddahi
 * @see LocationEJB
 */
@Singleton
@Path("/administration/locations")
public class LocationsEndpoint {
	@Inject
	private LocationEJB locationEJB;

	/**
	 * load all locations
	 * @return HTTP Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response getLocations() {
		JsonArrayBuilder builder = Json.createArrayBuilder();
		List<Location> list = locationEJB.getAll();
		for (Location l : list) {
			JsonObjectBuilder request = Json.createObjectBuilder();
			request.add("id", l.getId());
			request.add("description", l.getDescription());
			request.add("address", l.getAddress());
			request.add("postcode", l.getPostcode());
			request.add("city", l.getCity());
			request.add("country", l.getCountry());
			builder.add(request.build());
		}

		return Response.ok(builder.build()).build();
	}

	/**
	 * add a location
	 * @param request parsed request data (application/json)
	 * @return HTTP Response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response addLocation(JsonObject request) {
		Location Location = new Location();
		Location.setDescription(request.getString("description"));
		Location.setAddress(request.getString("address"));
		Location.setPostcode(request.getString("postcode"));
		Location.setCity(request.getString("city"));
		Location.setCountry(request.getString("country"));

		locationEJB.saveLocation(Location);
		return Response.created(null).build();
	}

	/**
	 * update a location
	 * @param id id of the location
	 * @param request parsed request data (application/json)
	 * @return HTTP Response
	 */
	@Path("/{id}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response editLocation(@PathParam("id") int id, JsonObject request) {
		Location location = locationEJB.getLocation(id);
		location.setDescription(request.getString("description"));
		location.setAddress(request.getString("address"));
		location.setPostcode(request.getString("postcode"));
		location.setCity(request.getString("city"));
		location.setCountry(request.getString("country"));

		locationEJB.saveLocation(location);
		return Response.noContent().build();
	}

	/**
	 * delete a location
	 * @param id id of the location
	 * @return HTTP Response
	 */
	@Path("/{id}")
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response dropLocation(@PathParam("id") int id) {
		Location Location = locationEJB.getLocation(id);
		locationEJB.deleteLocation(Location);
		return Response.noContent().build();
	}
}

package de.internally.api.rest.securechat;

import java.util.List;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import de.internally.ejb.database.EcdhEJB;
import de.internally.ejb.database.EmployeeEJB;
import de.internally.ejb.database.EncryptedMessagesEJB;
import de.internally.entities.database.Employee;
import de.internally.entities.database.EncryptedMessage;
import de.internally.helper.security.RsaHelper;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/secure_chat/messages
 * secure chat Messages
 * @author janbellenberg
 * @see EncryptedMessagesEJB
 * @see EcdhEJB
 * @see EmployeeEJB
 */
@Singleton
@Path("/secure_chat/messages")
public class MessageEndpoint {
	
	@Inject
	private EncryptedMessagesEJB messagesEJB;
	
	@Inject
	private EcdhEJB ecdhEJB;
	
	@Inject
	private EmployeeEJB employeeEJB;
	
	/**
	 * get overview of the messages
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.USER)
	public Response getMessages(@HeaderParam("X-UID") int uid) {
		List<Object[]> messages = this.messagesEJB.getMessagesForEmployee(uid);
		JsonArrayBuilder resultBuilder = Json.createArrayBuilder();
		
		for(Object[] message : messages) {
			JsonObjectBuilder employeeBuilder = Json.createObjectBuilder();
			employeeBuilder.add("id", (int) message[1]);
			employeeBuilder.add("firstname", String.valueOf(message[2]));
			employeeBuilder.add("lastname", String.valueOf(message[3]));
			
			JsonObjectBuilder messageBuilder = Json.createObjectBuilder();
			messageBuilder.add("sender", employeeBuilder.build());
			messageBuilder.add("next", (int) message[0]);
			messageBuilder.add("count", (Long) message[4]);
			
			resultBuilder.add(messageBuilder.build());
		}

		return Response.ok(resultBuilder.build()).build();
	}
	
	/**
	 * get message details
	 * @param id id of the message
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@GET
	@Path("/{id}")
	@Secured(level = SecurityLevel.USER)
	public Response getMessage(@PathParam("id") int id, @HeaderParam("X-UID") int uid) {
		
		JsonObjectBuilder builder = Json.createObjectBuilder();
		EncryptedMessage message = this.messagesEJB.getMessage(id);
		
		// when an new key was generated
		if(message.getSenderKey() != null && message.getReceiverKey() != null) {
			JsonObjectBuilder ecdh = Json.createObjectBuilder();
			ecdh.add("sender", message.getSenderKey().getId());
			ecdh.add("receiver", message.getReceiverKey().getId());
			builder.add("ecdh", ecdh.build());
		}

		builder.add("content", message.getContent());
		builder.add("signature", message.getSignature());
		builder.add("server_signature", RsaHelper.sign(message.getContent()));
		
		return Response.ok(builder.build()).build();
	}

	/**
	 * add a new message
	 * @param uid id of the employee (sender)
	 * @param data parsed request data (application/json)
	 * @return HTTP Response
	 */
	@POST
	@Secured(level = SecurityLevel.USER)
	public Response sendMessage(@HeaderParam("X-UID") int uid, JsonObject data) {
		EncryptedMessage message = new EncryptedMessage();
		message.setContent(data.getString("content"));
		message.setSignature(data.getString("signature"));
		
		Employee sender = this.employeeEJB.getEmployee(uid);
		Employee receiver = this.employeeEJB.getEmployee(data.getInt("receiver"));
		
		message.setSender(sender);
		message.setReceiver(receiver);
		
		// verify the senders identity
		if (!RsaHelper.verify(message.getContent(), message.getSignature(), sender.getRsaPublic())) {
			throw new BadRequestException();
		}
		
		// if a new key was generated
		if(data.containsKey("ecdh")) {
			JsonObject ecdh = data.getJsonObject("ecdh");

			message.setSenderKey(this.ecdhEJB.getPreKey(ecdh.getInt("sender")));
			message.setReceiverKey(this.ecdhEJB.getPreKey(ecdh.getInt("receiver")));
		}

		this.messagesEJB.saveMessage(message);

		return Response.status(Status.CREATED).build();
	}
	
	/**
	 * delete a message by id
	 * @param id id of the message
	 * @param uid id of the employee
	 * @return HTTP Response
	 */
	@DELETE
	@Path("/{id}")
	@Secured(level = SecurityLevel.USER)
	public Response deleteMessage(@PathParam("id") int id, @HeaderParam("X-UID") int uid) {
		EncryptedMessage message = this.messagesEJB.getMessage(id);
		
		if(uid != message.getReceiver().getId()) {
			return Response.status(Status.UNAUTHORIZED).build();
		}

		this.messagesEJB.deleteMessage(message);
		
		if(message.getSenderKey() != null && message.getReceiverKey() != null) {
			this.ecdhEJB.deletePreKey(message.getReceiverKey());
			
			if(message.getReceiverKey().getId() != message.getSenderKey().getId()) {
				this.ecdhEJB.deletePreKey(message.getSenderKey());
			}
		}
		
		
		return Response.status(Status.NO_CONTENT).build();
	}
}

package de.internally.api.rest.administration;

import java.util.List;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.internally.ejb.database.GroupEJB;
import de.internally.entities.database.Group;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/administration/groups
 * Group management
 * @author simonmaddahi
 * @see GroupEJB
 */
@Singleton
@Path("/administration/groups")
public class GroupEndpoint {

	@Inject
	private GroupEJB groupEJB;

	/**
	 * load all groups
	 * @return HTTP Response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response getGroups() {
		JsonArrayBuilder builder = Json.createArrayBuilder();
		List<Group> list = groupEJB.getAll();
		for (Group g : list) {
			JsonObjectBuilder request = Json.createObjectBuilder();
			request.add("id", g.getId());
			request.add("name", g.getName());
			request.add("admin", g.getAdministration());
			request.add("twoFA", g.getTwoFA());
			builder.add(request.build());
		}
		return Response.ok(builder.build()).build();
	}

	/**
	 * add a group
	 * @param request parsed request data (application/json)
	 * @return HTTP Response
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response addGroup(JsonObject request) {

		Group group = new Group();
		group.setName(request.getString("name"));
		group.setAdministration(request.getBoolean("admin"));
		group.setTwoFA(request.getBoolean("twoFA"));

		groupEJB.saveGroup(group);
		return Response.created(null).build();
	}

	/**
	 * update a group
	 * @param id id of the group
	 * @param request parsed request data (application/json)
	 * @return HTTP Response
	 */
	@Path("/{id}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response editGroup(@PathParam("id") int id, JsonObject request) {

		Group group = groupEJB.getGroup(id);
		group.setName(request.getString("name"));
		group.setAdministration(request.getBoolean("admin"));
		group.setTwoFA(request.getBoolean("twoFA"));

		groupEJB.saveGroup(group);
		return Response.noContent().build();
	}

	/**
	 * delete a group
	 * @param id id of the group
	 * @return HTTP Response
	 */
	@Path("/{id}")
	@DELETE
	@Secured(level = SecurityLevel.ADMIN)
	public Response dropGroup(@PathParam("id") int id) {
		Group Group = groupEJB.getGroup(id);
		groupEJB.deleteGroup(Group);
		return Response.noContent().build();
	}
}

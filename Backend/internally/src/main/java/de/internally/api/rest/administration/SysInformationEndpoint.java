package de.internally.api.rest.administration;

import java.net.Inet4Address;
import java.net.Inet6Address;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.internally.helper.ServerEnvironment;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * REST-API endpoint for /api/v1/administration/informations
 * read system information
 * @author janbellenberg
 */
@Path("/administration/information")
public class SysInformationEndpoint {

	/**
	 * get java informations
	 * @return HTTP Response
	 */
	@GET
	@Path("/java")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response getJavaInformation() {
		final String[] properties = { "vm.name", "vendor", "version" };
		
		JsonObjectBuilder result = Json.createObjectBuilder();
		for (String prop : properties) {
			result.add(prop, System.getProperty("java." + prop));
		}
		
		result.add("internally_version", new ServerEnvironment().getSoftwareVersion());
		
		return Response.ok(result.build()).build();
	}

	/**
	 * get network information
	 * @return HTTP Response
	 */
	@GET
	@Path("/network")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response getNetworkInformation() {

		JsonObjectBuilder result = Json.createObjectBuilder();
		String hostname = ServerEnvironment.getHostname();

		Inet4Address[] ipv4 = ServerEnvironment.getIPv4Addresses(hostname);
		Inet6Address[] ipv6 = ServerEnvironment.getIPv6Addresses(hostname);

		JsonArrayBuilder ipv4Builder = Json.createArrayBuilder();
		JsonArrayBuilder ipv6Builder = Json.createArrayBuilder();
		
		for(Inet4Address ip : ipv4) {
			ipv4Builder.add(ip.getHostAddress().toString());
		}
		
		for(Inet6Address ip : ipv6) {
			ipv6Builder.add(ip.getHostAddress().toString());
		}
		
		result.add("hostname", hostname);
		result.add("ipv4", ipv4Builder.build());
		result.add("ipv6", ipv6Builder.build());
		return Response.ok(result.build()).build();
	}

	/**
	 * get os information
	 * @return HTTP Response
	 */
	@GET
	@Path("/os")
	@Produces(MediaType.APPLICATION_JSON)
	@Secured(level = SecurityLevel.ADMIN)
	public Response getOsInformation() {
		final String[] properties = { "name", "arch", "version" };
		
		JsonObjectBuilder result = Json.createObjectBuilder();
		for (String prop : properties) {
			result.add(prop, System.getProperty("os." + prop));
		}
		
		result.add("user", System.getProperty("user.name"));
		return Response.ok(result.build()).build();
	}
}

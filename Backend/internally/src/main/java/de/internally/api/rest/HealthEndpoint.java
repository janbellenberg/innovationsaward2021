package de.internally.api.rest;

import javax.ejb.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * REST-API endpoint for /api/v1/health
 * healthcheck for docker container
 * @author janbellenberg
 *
 */
@Singleton
@Path("/health")
public class HealthEndpoint {

	/**
	 * send that the server is running
	 * @return HTTP Response (always 200 OK)
	 */
	@GET
	public Response answerHealthRequest() {
		return Response.ok().build();
	}
}

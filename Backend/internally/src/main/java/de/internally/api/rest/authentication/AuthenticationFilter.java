package de.internally.api.rest.authentication;

import java.lang.reflect.Method;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import de.internally.ejb.database.EmployeeEJB;
import de.internally.entities.database.Employee;
import de.internally.helper.security.JWT;
import de.internally.helper.security.Secured;
import de.internally.helper.security.SecurityLevel;

/**
 * request filter for checking the credentials of the employee
 * @author jan.bellenberg
 *
 */
@Provider
public class AuthenticationFilter implements ContainerRequestFilter {

	@Context
	private ResourceInfo resourceInfo;

	@Inject
	private EmployeeEJB employeeEJB;

	public static final String COOKIE_NAME = "internally-jwt";

	/**
	 * check jwt from cookie and stop request if invalid
	 * @param requestContext HTTP-Request
	 */
	@Override
	public void filter(ContainerRequestContext requestContext) {
		if(requestContext.getHeaders().containsKey("X-UID"))
			requestContext.getHeaders().remove("X-UID");
		
		Response error = Response.status(Status.FORBIDDEN).build();
		Response unauthorized = Response.status(Status.UNAUTHORIZED).build();

		Method method = resourceInfo.getResourceMethod();
		if (method.isAnnotationPresent(Secured.class)) {
			
			// if cookie not set
			final Map<String, Cookie> cookies = requestContext.getCookies();
			if(!cookies.containsKey(COOKIE_NAME)) {
				throw new NotAuthorizedException(unauthorized);
			}

			JWT parsed = null;

			// parse jwt
			try {
				parsed = JWT.parse(cookies.get(COOKIE_NAME).getValue());
			} catch (NoSuchAlgorithmException ex) {
				ex.printStackTrace();
				throw new NotAuthorizedException(error);
			}

			// if jwt is invalid
			if (parsed == null) {
				throw new NotAuthorizedException(error);
			}
			
			int id = parsed.getPayload().getInt("sub");
			
			// if admin session is expired
			if(id == 0 && parsed.getPayload().getInt("iat") + 600 < System.currentTimeMillis() / 1000L) {
				requestContext.abortWith(Response.status(Status.UNAUTHORIZED).entity("{ \"message\": \"Ihre Sitzung ist abgelaufen\" }").build());
				return;
			}
			
			boolean admin = false;
			boolean user = false;
			
			Employee e = null;
			if(id != 0) {
				// load user information
				e = this.employeeEJB.getEmployee(id);
				admin = e.getGroup().getAdministration();
				user = true;
			} else {
				// set admin information
				admin = true;
				user = false;
			}

			Secured rolesAnnotation = method.getAnnotation(Secured.class);
			Set<SecurityLevel> rolesSet = new HashSet<SecurityLevel>(Arrays.asList(rolesAnnotation.level()));

			if (rolesSet.contains(SecurityLevel.ADMIN) && !admin) {
				throw new NotAuthorizedException(error);
			}
			
			if(rolesSet.contains(SecurityLevel.USER) && !user) {
				throw new NotAuthorizedException(error);
			}
			
			if(rolesSet.contains(SecurityLevel.BOTH) && !(user || admin)) {
				throw new NotAuthorizedException(error);
			}

			
			requestContext.getHeaders().add("X-UID", String.valueOf(id));
		}
	}
}

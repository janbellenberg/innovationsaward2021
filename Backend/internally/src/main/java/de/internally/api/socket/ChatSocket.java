package de.internally.api.socket;

import java.io.IOException;
import java.io.StringReader;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonPatchBuilder;
import javax.json.JsonReader;
import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import de.internally.api.socket.helper.ChatEndpointConfigurator;
import de.internally.ejb.SocketManagerEJB;
import de.internally.ejb.database.ChatEJB;
import de.internally.ejb.database.EmployeeEJB;
import de.internally.ejb.database.MemberEJB;
import de.internally.ejb.database.MessageEventsEJB;
import de.internally.ejb.database.PlainMessageEJB;
import de.internally.entities.ChatSession;
import de.internally.entities.database.Chat;
import de.internally.entities.database.Employee;
import de.internally.entities.database.Member;
import de.internally.entities.database.PlainMessage;
import de.internally.entities.database.ReadEvent;
import de.internally.entities.database.ReceivedEvent;
import de.internally.helper.security.JWT;

/**
 * websocket endpoint for chat
 * @author janbellenberg
 * @see ChatEndpointConfigurator
 * @see ChatEJB
 * @see PlainMessageEJB
 * @see SocketManagerEJB
 * @see MessageEventsEJB
 */
@ServerEndpoint(value = "/socket/chat", configurator = ChatEndpointConfigurator.class)
public class ChatSocket {

	@Inject
	private SocketManagerEJB sessionRegistry;

	@Inject
	private ChatEJB chatEJB;

	@Inject
	private EmployeeEJB employeeEJB;

	@Inject
	private PlainMessageEJB messageEJB;
	
	@Inject
	private MessageEventsEJB eventsEJB;
	
	@Inject
	private MemberEJB memberEJB;

	private Timer t;

	/**
	 * initializes the timer for checking ping
	 */
	@PostConstruct
	public void init() {
		t = new Timer();
		t.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				Set<ChatSession> sessions = sessionRegistry.getAllChatSessions();

				try {
				for (ChatSession s : sessions) {
					Calendar calendar = Calendar.getInstance();
					calendar.add(Calendar.SECOND, -20);

					if (calendar.getTimeInMillis() > s.getLastPing()) {
						try {
							s.getSession().close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				} catch(ConcurrentModificationException ignore) {}
			}
		}, 0, 1000);
	}

	/**
	 * cancel timer on destroy
	 */
	@PreDestroy
	public void shutdown() {
		t.cancel();
	}

	/**
	 * initialize a new connection and check jwt
	 * @param session the new session
	 * @param conf configuration of the endpoint
	 * @throws IOException when closing the connection fails
	 */
	@OnOpen
	public void open(Session session, EndpointConfig conf) throws IOException {
		String jwtRaw = (String) conf.getUserProperties().get("jwt");

		if (jwtRaw == null || jwtRaw.trim() == "") {
			session.close();
			return;
		}

		JWT jwt = null;
		try {
			jwt = JWT.parse(jwtRaw);
		} catch (NoSuchAlgorithmException e) {
			session.close();
		}

		// check if jwt is valid
		if (jwt == null) {
			session.close();
			return;
		}

		int chatId = 0;
		
		if(conf.getUserProperties().containsKey("chat")) {
			chatId = Integer.parseInt((String) conf.getUserProperties().get("chat"));
		}

		if(chatId != 0) {
			Member member = this.memberEJB.getMember(chatId, jwt.getPayload().getInt("sub"));
	
			if (member == null) {
				session.close();
				return;
			}
		}

		ChatSession s = new ChatSession(session, jwt, chatId);
		sessionRegistry.addChatSession(s);
	}

	/**
	 * removes session from list when closing
	 * @param session the session that is ending
	 * @param reason reason for the close event
	 */
	@OnClose
	public void close(Session session, CloseReason reason) {
		sessionRegistry.removeChatSession(session);
	}

	/**
	 * when a client sends data through the websocket
	 * @param s the session of the connection
	 * @param msg the data that has been send through the connection
	 */
	@OnMessage
	public void onMessage(Session s, String msg) {
		ChatSession session = this.sessionRegistry.getChatSession(s);
		
		if(session == null)
			return;

		// read data
		JsonReader reader = Json.createReader(new StringReader(msg));
		JsonObject data = reader.readObject();
		reader.close();

		switch (data.getString("method")) {
		case "ping":
			session.setLastPing();
			break;
		case "add":
			addMessage(session, data);
			break;
		case "update":
			updateMessage(session, data);
			break;
		case "delete":
			deleteMessage(session, data);
			break;
		}
	}

	/**
	 * a user adds a message
	 * @param session data of session
	 * @param payload json-parsed data the server received
	 */
	private void addMessage(ChatSession session, JsonObject payload) {
		int uid = session.getJwt().getPayload().getInt("sub");
		int chatId = session.getChatId();

		// validate the request data
		Employee sender = this.employeeEJB.getEmployee(uid);
		Chat chat = this.chatEJB.getChat(chatId);

		if (chat == null) {
			return;
		}

		Member member = this.memberEJB.getMember(chatId, uid);

		if (member == null) {
			return;
		}
		
		// create a new instance for the message
		PlainMessage message = new PlainMessage();
		message.setChat(chat);
		message.setMember(member);
		message.setContent(payload.getString("content"));
		message.setTimestamp(new Date());
		
		// check for timed message
		if(payload.containsKey("timestamp")) {
			try {
				message.setRelease(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(payload.getString("timestamp")));
			} catch (ParseException e) {
				e.printStackTrace();
				return;
			}
		}

		// save the message to the database
		int mid = this.messageEJB.saveMessage(message);
		message = this.messageEJB.getMessage(mid);
		
		// build entity
		JsonObjectBuilder jsonSender = Json.createObjectBuilder();
		jsonSender.add("id", sender.getId());
		jsonSender.add("username", message.getMember().getEmployee().getUsername());
		jsonSender.add("firstname", sender.getFirstname());
		jsonSender.add("lastname", sender.getLastname());

		JsonPatchBuilder patch = Json.createPatchBuilder();
		patch.add("/sender", jsonSender.build());
		patch.add("/id", mid);
		patch.add("/title", chat.getName());

		payload = patch.build().apply(payload);
		
		Set<ChatSession> all = this.sessionRegistry.getAllChatSessions();
		
		// schedule send for timed messages
		if(payload.containsKey("timestamp")) {
			LocalDateTime sendTime = message.getRelease().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			long diff = ChronoUnit.MILLIS.between(LocalDateTime.now(), sendTime);
			
			final JsonObject p = payload;
			
			if(diff > 0) {
				Timer t = new Timer();
				t.schedule(new TimerTask(){
					public void run() {
						for (ChatSession s : all) {
							if ((s.getJwt().getPayload().getInt("sub") != uid // for other users: pwa notification
									&& s.getChatId() == 0) || s.getChatId() == chatId) { // for all users in specific chat: add message

								s.getSession().getAsyncRemote().sendText(p.toString());
							}
						}
					}
				}, diff);
				return;
			}
		}

		// broadcast message
		for (ChatSession s : all) {
			if ((s.getJwt().getPayload().getInt("sub") != uid // for other users: pwa notification
					&& s.getChatId() == 0) || s.getChatId() == chatId) { // for all users in specific chat: add message
				
				Member m = this.memberEJB.getMember(chatId, s.getJwt().getPayload().getInt("sub"));
				
				if(m == null) {
					continue;
				}
				

				// received & read events
				ReadEvent reEv = this.eventsEJB.getReadEvent(message.getId(), s.getJwt().getPayload().getInt("sub"));
				ReceivedEvent recvEv = this.eventsEJB.getReceivedEvent(message.getId(), s.getJwt().getPayload().getInt("sub"));
				
				if(recvEv == null) {
					ReceivedEvent received = new ReceivedEvent();
					received.setPlainMessage(message);
					received.setMember(m);
					received.setTimestamp(new Date());
					this.eventsEJB.saveReceivedEvent(received);
				}
				
				if(s.getChatId() != 0 && reEv == null) {
					ReadEvent read = new ReadEvent();
					read.setPlainMessage(message);
					read.setMember(m);
					read.setTimestamp(new Date());
					this.eventsEJB.saveReadEvent(read);
				}


				s.getSession().getAsyncRemote().sendText(payload.toString());
			}
		}
	}

	/**
	 * a user updates a message
	 * @param session data of session
	 * @param payload json-parsed data the server received
	 */
	private void updateMessage(ChatSession session, JsonObject payload) {
		int uid = session.getJwt().getPayload().getInt("sub");
		int chatId = session.getChatId();

		// load message
		PlainMessage message = this.messageEJB.getMessage(payload.getInt("id"));

		if (message == null) {
			return;
		}

		// if wrong chat or not sender
		if (message.getChat().getId() != chatId || message.getMember().getEmployee().getId() != uid) {
			return;
		}

		// update message
		message.setContent(payload.getString("content"));
		this.messageEJB.saveMessage(message);

		// broadcast message
		Set<ChatSession> all = this.sessionRegistry.getAllChatSessions();
		for (ChatSession s : all) {
			if (s.getChatId() == chatId) {
				s.getSession().getAsyncRemote().sendText(payload.toString());
			}
		}
	}

	/**
	 * a user deletes a message
	 * @param session data of session
	 * @param payload json-parsed data the server received
	 */
	private void deleteMessage(ChatSession session, JsonObject payload) {
		int uid = session.getJwt().getPayload().getInt("sub");
		int chatId = session.getChatId();

		// load message
		PlainMessage message = this.messageEJB.getMessage(payload.getInt("id"));

		if (message == null) {
			return;
		}

		// if wrong chat or not sender
		if (message.getChat().getId() != chatId || message.getMember().getEmployee().getId() != uid) {
			return;
		}

		// delete message
		this.messageEJB.deleteMessage(message);

		// broadcast message
		Set<ChatSession> all = this.sessionRegistry.getAllChatSessions();
		for (ChatSession s : all) {
			if (s.getChatId() == chatId) {
				s.getSession().getAsyncRemote().sendText(payload.toString());
			}
		}
	}
}

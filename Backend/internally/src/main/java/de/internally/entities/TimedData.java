package de.internally.entities;

import java.util.Timer;
import java.util.TimerTask;

/**
 * wrapper class for data that has a limited life time
 * @author janbellenberg
 *
 * @param <T> type of the data that should be wrapped
 */
public class TimedData<T> {
	private T data;
	private Timer expireTimer;
	
	/**
	 * initializes an new instance of TimedData
	 * @param data data that should be wrapped
	 * @param destroyTask task to dispose the data
	 * @param interval life time in milliseconds
	 */
	public TimedData(T data, TimerTask destroyTask, long interval) {
		this.data = data;
		this.expireTimer = new Timer();
		
		expireTimer.schedule(destroyTask, interval);
	}

	public T getData() {
		return this.data;
	}
	
	/**
	 * cancel dispose timer 
	 */
	public void abortTimer() {
		this.expireTimer.cancel();
	}
}

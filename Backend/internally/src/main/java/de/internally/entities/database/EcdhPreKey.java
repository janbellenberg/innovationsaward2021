package de.internally.entities.database;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the ecdh_prekeys database table.
 * @author generated by JPA
 */
@Entity
@Table(name="ecdh_prekeys")
@NamedQueries({

@NamedQuery(name="EcdhPreKey.find", query="SELECT k FROM EcdhPreKey k WHERE k.id = :id"),
@NamedQuery(name="EcdhPreKey.findNext", query="SELECT k FROM EcdhPreKey k JOIN k.employee e WHERE e.id = :id AND size(k.receiveUsages) < 1 AND size(k.sendUsages) < 1")
})
public class EcdhPreKey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	@Column(name="private")
	private String privateComponent;

	@Lob
	@Column(name="public")
	private String publicComponent;

	@Lob
	private String signature;

	//bi-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name="employee")
	private Employee employee;

	//bi-directional many-to-one association to EncryptedMessage
	@OneToMany(mappedBy="receiverKey")
	private List<EncryptedMessage> receiveUsages;

	//bi-directional many-to-one association to EncryptedMessage
	@OneToMany(mappedBy="senderKey")
	private List<EncryptedMessage> sendUsages;

	public EcdhPreKey() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPrivateComponent() {
		return this.privateComponent;
	}

	public void setPrivateComponent(String privateComponent) {
		this.privateComponent = privateComponent;
	}

	public String getPublicComponent() {
		return this.publicComponent;
	}

	public void setPublicComponent(String publicComponent) {
		this.publicComponent = publicComponent;
	}

	public String getSignature() {
		return this.signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public Employee getEmployee() {
		return this.employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<EncryptedMessage> getReceiveUsages() {
		return this.receiveUsages;
	}

	public void setReceiveUsages(List<EncryptedMessage> receiveUsages) {
		this.receiveUsages = receiveUsages;
	}

	public EncryptedMessage addReceiveUsage(EncryptedMessage receiveUsage) {
		getReceiveUsages().add(receiveUsage);
		receiveUsage.setReceiverKey(this);

		return receiveUsage;
	}

	public EncryptedMessage removeReceiveUsage(EncryptedMessage receiveUsage) {
		getReceiveUsages().remove(receiveUsage);
		receiveUsage.setReceiverKey(null);

		return receiveUsage;
	}

	public List<EncryptedMessage> getSendUsages() {
		return this.sendUsages;
	}

	public void setSendUsages(List<EncryptedMessage> sendUsages) {
		this.sendUsages = sendUsages;
	}

	public EncryptedMessage addSendUsage(EncryptedMessage sendUsage) {
		getSendUsages().add(sendUsage);
		sendUsage.setSenderKey(this);

		return sendUsage;
	}

	public EncryptedMessage removeSendUsage(EncryptedMessage sendUsage) {
		getSendUsages().remove(sendUsage);
		sendUsage.setSenderKey(null);

		return sendUsage;
	}

}
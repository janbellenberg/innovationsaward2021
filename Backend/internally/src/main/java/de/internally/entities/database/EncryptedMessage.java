package de.internally.entities.database;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the encrypted_messages database table.
 * @author generated by JPA
 */
@Entity
@Table(name="encrypted_messages")
@NamedQueries({
	@NamedQuery(name="EncryptedMessage.find", query="SELECT e FROM EncryptedMessage e WHERE e.id = :id"),
	@NamedQuery(name="EncryptedMessage.findForEmployee", query="SELECT e.id, s.id, s.firstname, s.lastname, count(e) FROM EncryptedMessage e JOIN e.receiver r JOIN e.sender s WHERE r.id = :employee GROUP BY e.receiver")
})
public class EncryptedMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private String content;

	@Lob
	private String signature;

	//bi-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name="receiver")
	private Employee receiver;

	//bi-directional many-to-one association to Employee
	@ManyToOne
	@JoinColumn(name="sender")
	private Employee sender;

	//bi-directional many-to-one association to EcdhPreKey
	@ManyToOne
	@JoinColumn(name="sender_key")
	private EcdhPreKey senderKey;

	//bi-directional many-to-one association to EcdhPreKey
	@ManyToOne
	@JoinColumn(name="receiver_key")
	private EcdhPreKey receiverKey;

	public EncryptedMessage() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSignature() {
		return this.signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public Employee getReceiver() {
		return this.receiver;
	}

	public void setReceiver(Employee receiver) {
		this.receiver = receiver;
	}

	public Employee getSender() {
		return this.sender;
	}

	public void setSender(Employee sender) {
		this.sender = sender;
	}

	public EcdhPreKey getSenderKey() {
		return senderKey;
	}

	public void setSenderKey(EcdhPreKey senderKey) {
		this.senderKey = senderKey;
	}

	public EcdhPreKey getReceiverKey() {
		return receiverKey;
	}

	public void setReceiverKey(EcdhPreKey receiverKey) {
		this.receiverKey = receiverKey;
	}

}
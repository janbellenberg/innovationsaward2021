package de.internally.entities.database;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the read_events database table.
 * @author generated by JPA
 */
@Entity
@Table(name="read_events")
@NamedQuery(name="ReadEvent.find", query="SELECT r FROM ReadEvent r WHERE r.member.employee.id = :member AND r.plainMessage.id = :message")
public class ReadEvent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="read_time")
	private Date timestamp;

	//bi-directional many-to-one association to Member
	@ManyToOne
	@JoinColumn(name="member")
	private Member member;

	//bi-directional many-to-one association to PlainMessage
	@ManyToOne
	@JoinColumn(name="message")
	private PlainMessage plainMessage;

	public ReadEvent() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public PlainMessage getPlainMessage() {
		return this.plainMessage;
	}

	public void setPlainMessage(PlainMessage plainMessage) {
		this.plainMessage = plainMessage;
	}

}
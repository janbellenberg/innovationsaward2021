package de.internally.entities;

import java.time.ZonedDateTime;

import javax.websocket.Session;

import de.internally.helper.security.JWT;

/**
 * entity for a connection session with the chat websocket
 * @author janbellenberg
 *
 */
public class ChatSession {

	private Session session;
	private JWT jwt;
	private int chatId;
	private long lastPing;
	
	/**
	 * initialize a new instance
	 * @param session WebSocket session
	 * @param jwt parsed and valid JWT
	 * @param chatId id of the chat the clients wants connect to
	 */
	public ChatSession(Session session, JWT jwt, int chatId) {
		this.session = session;
		this.jwt = jwt;
		this.chatId = chatId;
		this.lastPing = 0;
	}

	public Session getSession() {
		return session;
	}
	
	public JWT getJwt() {
		return jwt;
	}

	public int getChatId() {
		return chatId;
	}

	public long getLastPing() {
		return lastPing;
	}

	public void setLastPing() {
		this.lastPing = ZonedDateTime.now().toInstant().toEpochMilli();
	}
	
	
}

package de.internally.entities.database;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the received_events database table.
 * @author generated by JPA
 */
@Entity
@Table(name="received_events")
@NamedQueries({
	@NamedQuery(name="ReceivedEvent.find", query="SELECT r FROM ReceivedEvent r WHERE r.member.employee.id = :member AND r.plainMessage.id = :message"),
	@NamedQuery(name="ReceivedEvent.findAll", query="SELECT r FROM ReceivedEvent r WHERE r.plainMessage.id = :message")
})
public class ReceivedEvent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="receive_time")
	private Date timestamp;

	//bi-directional many-to-one association to Member
	@ManyToOne
	@JoinColumn(name="member")
	private Member member;

	//bi-directional many-to-one association to PlainMessage
	@ManyToOne
	@JoinColumn(name="message")
	private PlainMessage plainMessage;

	public ReceivedEvent() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Member getMember() {
		return this.member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public PlainMessage getPlainMessage() {
		return this.plainMessage;
	}

	public void setPlainMessage(PlainMessage plainMessage) {
		this.plainMessage = plainMessage;
	}

}
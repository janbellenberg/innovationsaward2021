package de.internally;

import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import de.internally.ejb.SocketManagerEJB;
import de.internally.ejb.database.MessageEventsEJB;
import de.internally.ejb.database.PlainMessageEJB;
import de.internally.entities.ChatSession;
import de.internally.entities.database.PlainMessage;
import de.internally.entities.database.ReceivedEvent;
import de.internally.helper.ConfigurationHelper;
import de.internally.helper.ServerEnvironment;

/**
 * class for the initialization of the application
 * @author janbellenberg
 * @see PlainMessageEJB
 * @see SocketManagerEJB
 */
@Singleton
@Startup
public class Initialization {
	
	@Inject
	private PlainMessageEJB messageEJB;
	
	@Inject
	private MessageEventsEJB eventsEJB;
	
	@Inject
	private SocketManagerEJB sessionRegistry;
	
	private List<Timer> timedMessages;
	
	/**
	 * startup message and initial checks
	 */
	@PostConstruct()
	public void init() {
		// Startup message
		
		Logger logger = Logger.getLogger("de.internally");
		logger.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("%%%%%%                                                    %%%%%%");
		logger.info("%%%%%%                                                    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%%%            %%%%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%   internally   %%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%%%            %%%%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    %%%%%%");
		logger.info("%%%%%%              %%%%                                  %%%%%%");
		logger.info("%%%%%%              %%                                    %%%%%%");
		logger.info("%%%%%%%%%%%%%%%%              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("%%%%%%%%%%%%%%%%          %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("%%%%%%%%%%%%%%%%        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("%%%%%%%%%%%%%%%%      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("%%%%%%%%%%%%%%%%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("%%%%%%%%%%%%%%%%  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		logger.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
		
		logger.info("");
		logger.info("======================== > internally < ========================");
		logger.info(">> by Jan Bellenberg & Simon Maddahi");
		logger.info("");
		
		this.loadMailSettings();
		this.checkDataIntegrity();
		
		logger.info("Data integrity validated");
		
		if(ServerEnvironment.isInDocker())
			logger.info("Docker environment detected");
		
		logger.info("For README, please visit https://" + ServerEnvironment.getHostname() + ":8443/readme");
		
		this.loadTimedMessages();
	}
	
	/**
	 * loads the timed messages and schedules the send event
	 */
	private void loadTimedMessages() {
		// delay execution for clients to connect
		new Timer().schedule(new TimerTask(){
			public void run() {
				
				timedMessages = new ArrayList<Timer>();
				List<PlainMessage> messages = messageEJB.getTimedMessages(); 
				for(PlainMessage m : messages) {
					
					LocalDateTime sendTime = m.getRelease().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
					long diff = ChronoUnit.MILLIS.between(LocalDateTime.now(), sendTime);
					
					if(diff <= 0) {
						sendMessage(m);
					}
					else {
						Timer t = new Timer();
						t.schedule(new TimerTask(){
							public void run() {
								sendMessage(m);
								timedMessages.remove(t);
							}
						}, diff);
						
						timedMessages.add(t);
					}
				}
				
			}}, 5000);
		
	}
	
	/**
	 * method for sending a timed message
	 * @param message instance of the message that should be sent
	 */
	private void sendMessage(PlainMessage message) {
		
		ReceivedEvent received = new ReceivedEvent();
		received.setPlainMessage(message);
		received.setMember(message.getMember());
		received.setTimestamp(new Date());
		this.eventsEJB.saveReceivedEvent(received);
		
		JsonObjectBuilder jsonSender = Json.createObjectBuilder();
		jsonSender.add("id", message.getMember().getEmployee().getId());
		jsonSender.add("username", message.getMember().getEmployee().getUsername());
		jsonSender.add("firstname", message.getMember().getEmployee().getFirstname());
		jsonSender.add("lastname", message.getMember().getEmployee().getLastname());
		
		JsonObjectBuilder jsonMessage = Json.createObjectBuilder();
		jsonMessage.add("method", "add");
		jsonMessage.add("id", message.getId());
		jsonMessage.add("title", message.getChat().getName());
		jsonMessage.add("content", message.getContent());
		jsonMessage.add("sender", jsonSender.build());
		
		final String json = jsonMessage.build().toString();
		
		Set<ChatSession> all = this.sessionRegistry.getAllChatSessions();
		for (ChatSession s : all) {
			if ((s.getJwt().getPayload().getInt("sub") != message.getMember().getEmployee().getId() // for other users: pwa notification
					&& s.getChatId() == 0) || s.getChatId() == message.getChat().getId()) { // for all users in specific chat: add message

				s.getSession().getAsyncRemote().sendText(json);
			}
		}
		
		message.setRelease(null);
		this.messageEJB.saveMessage(message);
	}

	/**
	 * loading the mail configurations into the system properties
	 */
	private void loadMailSettings() {
		JsonObject smtpConfiguration = ConfigurationHelper.getMailConfig();
		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		properties.setProperty("mail.smtp.host", smtpConfiguration.getString("host"));
		properties.setProperty("mail.smtp.port", smtpConfiguration.getString("port"));
	}
	
	/**
	 * checks the integrity of the data folder
	 */
	private void checkDataIntegrity() {
		String dataPath = System.getProperty("jboss.server.data.dir") + "/internally";
		if(!new File(dataPath).exists() || !new File(dataPath).isDirectory())
			throw new RuntimeException("configuration directory missing");

		if(!ConfigurationHelper.validateConfiguration())
			throw new RuntimeException("configuration file missing or damaged");
		
		if(!new File(dataPath + "/profile-images").exists()) {
			throw new RuntimeException("Folder 'profile-images' doesn't exist");
		}

		if(!new File(dataPath + "/mail-presets/2fa.html").exists() || 
				!new File(dataPath + "/mail-presets/login.html").exists()) {
			throw new RuntimeException("Mail preset is missing");
		}
	}
}

package de.internally.ejb.database;

import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.internally.entities.database.Member;

/**
 * class for managing database operations for chat members
 * @author janbellenberg
 * @see Member
 */
@Singleton
public class MemberEJB {

	@PersistenceContext
	private EntityManager em;

	/**
	 * get all members of the chat
	 * @param chat id of the chat (FK)
	 * @return list of members
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<Member> getAll(int chat) {
		Query q = em.createNamedQuery("Member.findAll");
		q.setParameter("chat", chat);
		return (List<Member>) q.getResultList();
	}

	/**
	 * get member by id
	 * @param id id of the member (PK)
	 * @return instance of the member
	 */
	@Lock(LockType.READ)
	public Member getMember(int id) {
		Query q = em.createNamedQuery("Member.findMember");
		q.setParameter("member", id);
		return (Member) q.getSingleResult();
	}
	
	/**
	 * gets the member with the user id in the chat
	 * @param chatId id of the chat (FK)
	 * @param employeeId id of the user (FK)
	 * @return instance of the member
	 */
	public Member getMember(int chatId, int employeeId) {
		try {
			Query q = em.createNamedQuery("Member.find");
			q.setParameter("chat", chatId);
			q.setParameter("employee", employeeId);
			return (Member) q.getSingleResult();
		} catch(NoResultException ex) {
			return null;
		}
		
	}

	/**
	 * adds / updates the member in the database
	 * @param member instance of the member that should be updated
	 */
	@Lock(LockType.WRITE)
	public void saveMember(Member member) {
		em.merge(member);
	}

	/**
	 * deletes a member from the database
	 * @param member instance of the member that should be deleted
	 */
	@Lock(LockType.WRITE)
	public void deleteMember(Member member) {
		Query q = em.createQuery("DELETE FROM Member where id = :id");
		q.setParameter("id", member.getId());
		q.executeUpdate();
	}
}

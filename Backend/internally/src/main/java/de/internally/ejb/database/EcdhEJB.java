package de.internally.ejb.database;

import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.internally.entities.database.EcdhPreKey;
import de.internally.entities.database.Key;
import de.internally.entities.database.PlainMessage;

/**
 * class for managing database operations for ecdh pre-keys
 * @author janbellenberg
 * @see PlainMessage
 */
@Singleton
public class EcdhEJB {
	
	@PersistenceContext
	private EntityManager em;

	/**
	 * gets the next ecdh prekey for the employee
	 * @param employee id of the employee (FK)
	 * @return instance of the ecdh prekey
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public EcdhPreKey getNext(int employee) {
		Query q = em.createNamedQuery("EcdhPreKey.findNext");
		q.setParameter("id", employee);
		List<EcdhPreKey> keys = q.getResultList();

		if (keys.size() < 1) {
			return null;
		}

		return keys.get(0);
	}

	/**
	 * get ecdh prekey by the id
	 * @param id id of the ecdh prekey (PK)
	 * @return instance of the ecdh prekey
	 */
	public EcdhPreKey getPreKey(int id) {
		Query q = em.createNamedQuery("EcdhPreKey.find");
		q.setParameter("id", id);
		return (EcdhPreKey) q.getSingleResult();
	}

	/**
	 * add / update an ecdh prekey in the datase
	 * @param key instance of the prekey
	 */
	public void saveEcdhKey(EcdhPreKey key) {
		em.merge(key);
	}

	/**
	 * deletes an ecdh prekey from the database
	 * @param key instance of the ecdh prekey
	 */
	public void deletePreKey(EcdhPreKey key) {
		em.remove(em.merge(key));
	}

	/**
	 * get key by owner, peer and purpose
	 * @param owner id of the employee who created the key
	 * @param peer id of the other employee
	 * @param send if it is a send or receive key
	 * @return instance of the key
	 */
	public Key getKey(int owner, int peer, boolean send) {
		try {
			Query q = em.createNamedQuery("Keys.find");
			q.setParameter("owner", owner);
			q.setParameter("peer", peer);
			q.setParameter("send", send);
			return (Key) q.getSingleResult();
		} catch (NoResultException exception) {
			return null;
		}
	}

	/**
	 * add / update a key in the database
	 * @param key instance of the key
	 */
	public void saveKey(Key key) {
		em.merge(key);
	}
	
	/**
	 * deletes all the ecdh prekeys of an employee
	 * @param uid id of the employee
	 */
	public void deleteAllMyPreKeys(int uid) {
		Query q = em.createQuery("DELETE FROM EcdhPreKey WHERE employee.id = :uid");
		q.setParameter("uid", uid);
		q.executeUpdate();
	}
	
	/**
	 * deletes all the used keys of an employee
	 * @param uid id of the employee
	 */
	public void deleteAllMyKeys(int uid) {
		Query q = em.createQuery("DELETE FROM Key WHERE owner.id = :uid OR peer.id = :uid");
		q.setParameter("uid", uid);
		q.executeUpdate();
	}

}

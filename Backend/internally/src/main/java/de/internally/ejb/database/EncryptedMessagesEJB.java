package de.internally.ejb.database;

import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.internally.entities.database.EncryptedMessage;
import de.internally.entities.database.PlainMessage;

/**
 * class for managing database operations for encrypted messages
 * @author janbellenberg
 * @see PlainMessage
 */
@Singleton
public class EncryptedMessagesEJB {

	@PersistenceContext
	private EntityManager em;

	/**
	 * get all encrypted messages for the employee
	 * @param employee id of the employee (FK)
	 * @return list of messages
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getMessagesForEmployee(int employee) {
		Query q = em.createNamedQuery("EncryptedMessage.findForEmployee");
		q.setParameter("employee", employee);
		return q.getResultList();
	}

	/**
	 * get message by id
	 * @param id id of the message (PK)
	 * @return instance of the message
	 */
	public EncryptedMessage getMessage(int id) {
		Query q = em.createNamedQuery("EncryptedMessage.find");
		q.setParameter("id", id);
		return (EncryptedMessage) q.getSingleResult();
	}

	/**
	 * add / update a message in the database
	 * @param message instance of the message
	 * @return updated instance of the message
	 */
	public EncryptedMessage saveMessage(EncryptedMessage message) {
		return em.merge(message);
	}

	/**
	 * delete a message from the database
	 * @param message instance of the message
	 */
	public void deleteMessage(EncryptedMessage message) {
		em.remove(em.merge(message));
	}
	
	/**
	 * delete all messages from a employee
	 * @param uid id of the employee (FK)
	 */
	public void deleteAllMyMessages(int uid) {
		Query q = em.createQuery("DELETE FROM EncryptedMessage WHERE sender.id = :uid OR receiver.id = :uid");
		q.setParameter("uid", uid);
		q.executeUpdate();
	}

}

package de.internally.ejb.database;

import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.internally.entities.database.Group;
import de.internally.entities.database.PlainMessage;

/**
 * class for managing database operations for groups
 * @author janbellenberg
 * @see PlainMessage
 */
@Singleton
public class GroupEJB {
	
	@PersistenceContext
	private EntityManager em;

	/**
	 * get all groups
	 * @return list of groups
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<Group> getAll() {
		Query q = em.createNamedQuery("Group.findAll");
		return (List<Group>) q.getResultList();
	}

	/**
	 * get group by id
	 * @param id id of the group (PK)
	 * @return instance of the group
	 */
	@Lock(LockType.READ)
	public Group getGroup(int id) {
		Query q = em.createNamedQuery("Group.findGroup");
		q.setParameter("group", id);
		return (Group) q.getSingleResult();
	}

	/**
	 * add / update group in the database
	 * @param group instance of the group
	 */
	@Lock(LockType.WRITE)
	public void saveGroup(Group group) {
		em.merge(group);
	}

	/**
	 * delete a group from the database
	 * @param group instance of the group
	 */
	@Lock(LockType.WRITE)
	public void deleteGroup(Group group) {
		em.remove(em.merge(group));
	}

}

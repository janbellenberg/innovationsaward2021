package de.internally.ejb.database;

import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.internally.entities.database.Highlight;
import de.internally.entities.database.PlainMessage;

/**
 * class for managing database operations for highlights
 * @author janbellenberg
 * @see PlainMessage
 */
@Singleton
public class HighlightsEJB {

	@PersistenceContext
	private EntityManager em;
	
	/**
	 * get highlight by message and employee
	 * @param message id of the message (FK)
	 * @param employee id of the employee (FK)
	 * @return instance of the highlight
	 */
	@Lock(LockType.READ)
	public Highlight getHighlight(int message, int employee) {
		try {
			Query q = em.createNamedQuery("Highlight.find");
			q.setParameter("message", message);
			q.setParameter("member", employee);
			return (Highlight) q.getSingleResult();
		} catch(NoResultException exception) {
			return null;
		}
	}
	
	/**
	 * get all highlights of an employee
	 * @param employee id of the employee (FK)
	 * @return list of highlights
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<Highlight> getHighlights(int employee) {
		try {
			Query q = em.createNamedQuery("Highlight.findForEmployee");
			q.setParameter("member", employee);
			return (List<Highlight>) q.getResultList();
		} catch(NoResultException exception) {
			return null;
		}
	}

	/**
	 * add / update highlight in the database
	 * @param h instance of the highlight
	 */
	@Lock(LockType.WRITE)
	public void saveHighlight(Highlight h) {
		em.merge(h);
	}

	/**
	 * delete highlight from the database
	 * @param h instance of the highlight
	 */
	@Lock(LockType.WRITE)
	public void deleteHighlight(Highlight h) {
		em.remove(em.merge(h));
	}

}

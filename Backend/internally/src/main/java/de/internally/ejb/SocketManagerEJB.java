package de.internally.ejb;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.websocket.Session;

import de.internally.entities.ChatSession;

/**
 * class for managing open sockets (performance + chat)
 * @author janbellenberg
 * @see ChatSession
 */
@Singleton
public class SocketManagerEJB {
	private final Set<Session> performanceSessions = new HashSet<>();
	private final Set<ChatSession> chatSessions = new HashSet<>();

	@Lock(LockType.READ)
	public Set<Session> getAllPerformanceSessions() {
		return Collections.unmodifiableSet(this.performanceSessions);
	}

	@Lock(LockType.WRITE)
	public void addPerformanceSession(Session s) {
		performanceSessions.add(s);
	}

	@Lock(LockType.WRITE)
	public void removePerformanceSession(Session s) {
		performanceSessions.remove(s);
	}

	@Lock(LockType.READ)
	public Set<ChatSession> getAllChatSessions() {
		return Collections.unmodifiableSet(this.chatSessions);
	}

	@Lock(LockType.READ)
	public ChatSession getChatSession(Session s) {
		for (ChatSession c : this.chatSessions) {
			if (c.getSession().equals(s)) {
				return c;
			}
		}

		return null;
	}

	@Lock(LockType.WRITE)
	public void addChatSession(ChatSession s) {
		chatSessions.add(s);
	}

	@Lock(LockType.WRITE)
	public void removeChatSession(Session s) {
		this.chatSessions.removeIf((session) -> session.getSession().equals(s));
	}

}

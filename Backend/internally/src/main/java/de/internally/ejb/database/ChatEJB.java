package de.internally.ejb.database;

import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.internally.entities.database.Chat;

/**
 * class for managing database operations for chats
 * @author janbellenberg
 * @see Chat
 */
@Singleton
public class ChatEJB {

	@PersistenceContext
	private EntityManager em;

	/**
	 * get chat by id (PK)
	 * @param id id of the chat (PK)
	 * @return instance of the chat
	 */
	@Lock(LockType.READ)
	public Chat getChat(int id) {
		try {
			Query q = em.createNamedQuery("Chat.findChat");
			q.setParameter("chat", id);
			return (Chat) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * get all chats where the employee is a member
	 * @param employee id of the employee (PK)
	 * @return list of chats
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<Chat> getChats(int employee) {
		Query q = em.createNamedQuery("Chat.findChatOfUser");
		q.setParameter("employee", employee);
		return (List<Chat>) q.getResultList();
	}

	/**
	 * add / update chat in database
	 * @param chat instance of the chat
	 * @return updated instance of the chat
	 */
	@Lock(LockType.WRITE)
	public Chat saveChat(Chat chat) {
		return em.merge(chat);
	}

	/**
	 * delete a chat from the database
	 * @param chat chat that should be deleted
	 */
	@Lock(LockType.WRITE)
	public void deleteChat(Chat chat) {
		em.remove(em.merge(chat));
	}
}

package de.internally.ejb.database;

import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.internally.entities.database.Location;
import de.internally.entities.database.PlainMessage;

/**
 * class for managing database operations for locations
 * @author janbellenberg
 * @see PlainMessage
 */
@Singleton
public class LocationEJB {
	@PersistenceContext
	private EntityManager em;

	/**
	 * get all locations
	 * @return list of all locations
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<Location> getAll() {
		Query q = em.createNamedQuery("Location.findAll");
		return (List<Location>) q.getResultList();
	}

	/**
	 * get location by id
	 * @param id id of location (PK)
	 * @return instance of the location
	 */
	@Lock(LockType.READ)
	public Location getLocation(int id) {
		Query q = em.createNamedQuery("Location.findLocation");
		q.setParameter("location", id);
		return (Location) q.getSingleResult();
	}

	/**
	 * add / update a location in the database
	 * @param location instance of the location
	 */
	@Lock(LockType.WRITE)
	public void saveLocation(Location location) {
		em.merge(location);
	}

	/**
	 * delete location from database
	 * @param location instance of the location
	 */
	@Lock(LockType.WRITE)
	public void deleteLocation(Location location) {
		em.remove(em.merge(location));
	}

}

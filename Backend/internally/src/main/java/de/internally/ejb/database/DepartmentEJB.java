package de.internally.ejb.database;

import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.internally.entities.database.Department;
import de.internally.entities.database.PlainMessage;

/**
 * class for managing database operations for plain messages
 * @author janbellenberg
 * @see PlainMessage
 */
@Singleton
public class DepartmentEJB {

	@PersistenceContext
	private EntityManager em;

	/**
	 * get all departments
	 * @return list of all departments
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<Department> getAll() {
		Query q = em.createNamedQuery("Department.findAll");
		return (List<Department>) q.getResultList();
	}

	/**
	 * get department by id
	 * @param id id of the department (PK)
	 * @return instance of the department
	 */
	@Lock(LockType.READ)
	public Department getDepartment(int id) {
		Query q = em.createNamedQuery("Department.findDepartment");
		q.setParameter("department", id);
		return (Department) q.getSingleResult();
	}

	/**
	 * adds / updates the department in the database
	 * @param department instance of the department
	 */
	@Lock(LockType.WRITE)
	public void saveDepartment(Department department) {
		em.merge(department);
	}

	/**
	 * deletes the department from the database
	 * @param department instance of the department
	 */
	@Lock(LockType.WRITE)
	public void deleteDepartment(Department department) {
		em.remove(em.merge(department));
	}

}

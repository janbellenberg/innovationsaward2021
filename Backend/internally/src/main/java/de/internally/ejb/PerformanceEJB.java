package de.internally.ejb;

import java.lang.management.ManagementFactory;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.websocket.Session;

/**
 * class for periodically sending performance data to the clients
 * @author janbellenberg
 * @see SocketManagerEJB
 */
@Singleton
@Startup
public class PerformanceEJB {

	@Inject
	SocketManagerEJB sessions;

	Timer timer;

	MBeanServer mbs = null;
	ObjectName name = null;

	/**
	 * constructor - initialize instance of PerformanceEJB
	 */
	public PerformanceEJB() {
		this.timer = new Timer();

		try {
			this.mbs = ManagementFactory.getPlatformMBeanServer();
			this.name = ObjectName.getInstance("java.lang:type=OperatingSystem");
		} catch (MalformedObjectNameException | NullPointerException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * PostConstruct - schedule sending of performance data
	 */
	@PostConstruct
	private void init() {
		if (this.mbs != null && this.name != null) {
			this.timer.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					sendServerPerformance();
				}
			}, 0, 200);
		}
	}

	/**
	 * PreDestroy - cancel timer
	 */
	@PreDestroy
	private void cleanUp() {
		this.timer.cancel();
	}

	/**
	 * collect informations and broadcast it to the clients
	 */
	@Lock(LockType.READ)
	public void sendServerPerformance() {
		Set<Session> sessions = this.sessions.getAllPerformanceSessions();

		if (sessions.size() > 0) {
			JsonObjectBuilder data = Json.createObjectBuilder();
			data.add("cpu", this.getCpuUsage());
			data.add("ram", this.getRamUsage());
			data.add("sock", this.sessions.getAllChatSessions().size());
			String toSend = data.build().toString();

			for (Session s : sessions) {
				s.getAsyncRemote().sendText(toSend);
			}
		}
	}

	/**
	 * load current cpu usage
	 * @return cpu usage in %
	 */
	private byte getCpuUsage() {
		try {
			// request data
			AttributeList list = this.mbs.getAttributes(this.name, new String[] { "SystemCpuLoad" });

			if (list.isEmpty())
				return 0;

			// get value
			Attribute attr = (Attribute) list.get(0);
			double value = (Double) attr.getValue();

			if (value == -1.0)
				return 0;

			// calculate result
			return (byte) ((int) (value * 1000) / 10.0);

		} catch (InstanceNotFoundException | ReflectionException ex) {
			ex.printStackTrace();
			return 0;
		}
	}

	/**
	 * load current ram usage
	 * @return used ram in %
	 */
	private byte getRamUsage() {
		try {
			// request total space and free space
			AttributeList list = this.mbs.getAttributes(this.name,
					new String[] { "FreePhysicalMemorySize", "TotalPhysicalMemorySize" });

			if (list.isEmpty())
				return 0;

			// get values
			Attribute attr = (Attribute) list.get(0);
			long freeRamSize = (Long) attr.getValue();

			attr = (Attribute) list.get(1);
			long totalRamSize = (Long) attr.getValue();

			// calculate result
			long usedRamSize = totalRamSize - freeRamSize;
			return (byte) ((double) usedRamSize / (double) totalRamSize * 100.0);

		} catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}
	}
}

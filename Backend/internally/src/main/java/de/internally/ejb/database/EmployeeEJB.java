package de.internally.ejb.database;

import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.internally.entities.database.Employee;
import de.internally.entities.database.PlainMessage;

/**
 * class for managing database operations for employees
 * @author janbellenberg
 * @see PlainMessage
 */
@Singleton
public class EmployeeEJB {
	@PersistenceContext
	private EntityManager em;

	/**
	 * get all employee matching the filter
	 * @param username filter for the username attribute
	 * @param firstname filter for the firstname attribute
	 * @param lastname filter for the lastname attribute
	 * @param email filter for the email attribute
	 * @param department id of the department attribute (0 = all)
	 * @param location id of the location attribute (0 = all)
	 * @param group id oft he group attribute (0 = all)
	 * @return list of employees matching the filters
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<Employee> filterAll(String username, String firstname, String lastname, String email, int department,
			int location, int group) {

		String joins = "";
		// build conditions with string parameters
		String conditions = "e.username like :username "
				+ "AND e.firstname like :firstname "
				+ "AND e.lastname like :lastname "
				+ "AND e.email like :email ";

		// check if department is specified
		if (department > 0) {
			joins += "JOIN e.department d ";
			conditions += "AND d.id like :department ";
		}

		// check if location is specified
		if (location > 0) {
			joins += "JOIN e.location l ";
			conditions += "AND l.id like :location ";
		}

		// check if group is specified
		if (group > 0) {
			joins += "JOIN e.group g ";
			conditions += "AND g.id like :group";
		}

		// build query
		Query q = em.createQuery("SELECT e FROM Employee e " + joins + "WHERE " + conditions);

		q.setParameter("username", username);
		q.setParameter("firstname", firstname);
		q.setParameter("lastname", lastname);
		q.setParameter("email", email);

		if (department > 0)
			q.setParameter("department", department);

		if (location > 0)
			q.setParameter("location", location);

		if (group > 0)
			q.setParameter("group", group);

		return (List<Employee>) q.getResultList();
	}

	/**
	 * get all used salt values
	 * @return list of the salt values used by all employees
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<String> getSalts() {
		Query q = em.createNamedQuery("Employee.findSalts");
		return (List<String>) q.getResultList();
	}

	/**
	 * get employee by id
	 * @param id id of the employee (PK)
	 * @return instance of the employee
	 */
	@Lock(LockType.READ)
	public Employee getEmployee(int id) {
		Query q = em.createNamedQuery("Employee.findEmployee");
		q.setParameter("employee", id);
		return (Employee) q.getSingleResult();
	}

	/**
	 * get employee by username
	 * @param username username of the employee
	 * @return instance of the employee
	 */
	@Lock(LockType.READ)
	public Employee getEmployee(String username) {
		Query q = em.createNamedQuery("Employee.findEmployeeByUsername");
		q.setParameter("employee", username);
		return (Employee) q.getSingleResult();
	}

	/**
	 * add / update an employee in the database
	 * @param employee instance of the employee
	 */
	@Lock(LockType.WRITE)
	public void saveEmployee(Employee employee) {
		em.merge(employee);
	}

	 /**
	  * delete an employee from the database
	  * @param employee instance of the employee
	  */
	@Lock(LockType.WRITE)
	public void deleteEmployee(Employee employee) {
		em.remove(em.merge(employee));
	}

	/**
	 * get all employees by the query
	 * @param query search query for username, lastname + firstname
	 * @return list of employees matching the query
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<Employee> getAllByQuery(String query) {
		Query q = em.createNamedQuery("Employee.find");
		q.setParameter("query", query);
		q.setMaxResults(50);
		return (List<Employee>) q.getResultList();
	}

}

package de.internally.ejb.database;

import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.internally.entities.database.ReadEvent;
import de.internally.entities.database.ReceivedEvent;

/**
 * class for managing database operations for read / received events
 * @author janbellenberg
 */
@Singleton
public class MessageEventsEJB {

	@PersistenceContext
	private EntityManager em;

	/**
	 * get all received events for the message
	 * @param message id of the message (FK)
	 * @return list of received events
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<ReceivedEvent> getReceivedEvents(int message) {
		try {
			Query q = em.createNamedQuery("ReceivedEvent.findAll");
			q.setParameter("message", message);
			return (List<ReceivedEvent>) q.getResultList();

		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * get read event by message and member
	 * @param message id of the message (FK)
	 * @param employee id of the employee (FK)
	 * @return instance of the read event
	 */
	@Lock(LockType.READ)
	public ReadEvent getReadEvent(int message, int employee) {
		try {
			Query q = em.createNamedQuery("ReadEvent.find");
			q.setParameter("message", message);
			q.setParameter("member", employee);
			return (ReadEvent) q.getSingleResult();

		} catch (NoResultException e) {
			return null;
		}
	}
	
	/**
	 * get received event by message and member
	 * @param message id of the message (FK)
	 * @param employee id of the employee (FK)
	 * @return instance of the received event
	 */
	@Lock(LockType.READ)
	public ReceivedEvent getReceivedEvent(int message, int employee) {
		try {
			Query q = em.createNamedQuery("ReceivedEvent.find");
			q.setParameter("message", message);
			q.setParameter("member", employee);
			return (ReceivedEvent) q.getSingleResult();

		} catch (NoResultException e) {
			return null;
		}
	}

	@Lock(LockType.WRITE)
	public void saveReceivedEvent(ReceivedEvent event) {
		em.merge(event);
	}

	@Lock(LockType.WRITE)
	public void saveReadEvent(ReadEvent event) {
		em.merge(event);
	}
}

package de.internally.ejb.database;

import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.internally.entities.database.Notification;
import de.internally.entities.database.PlainMessage;

/**
 * class for managing database operations for notifications
 * @author janbellenberg
 * @see PlainMessage
 */
@Singleton
public class NotificationEJB {

	@PersistenceContext
	private EntityManager em;
	
	/**
	 * loads all notifications for the department and the location
	 * @param department id of the department (FK)
	 * @param location id of the location (FK)
	 * @return list of notifications
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<Notification> getNotficiations(int department, int location) {
		Query q = em.createNamedQuery("Notification.findAll");
		q.setParameter("department", department);
		q.setParameter("location", location);
		return (List<Notification>) q.getResultList();
	}

	/**
	 * get notification by id
	 * @param id id of the notification (PK)
	 * @return instance of the notifications
	 */
	@Lock(LockType.READ)
	public Notification getNotficiation(int id) {
		Query q = em.createNamedQuery("Notification.find");
		q.setParameter("id", id);
		return (Notification) q.getSingleResult();
	}

	/**
	 * add / update a notification in the database
	 * @param notification instance of the notification
	 */
	@Lock(LockType.WRITE)
	public void saveNotification(Notification notification) {
		em.merge(notification);
	}

	/**
	 * deletes a notification from the database
	 * @param notification instance of the notification
	 */
	@Lock(LockType.WRITE)
	public void deleteNotification(Notification notification) {
		em.remove(em.merge(notification));
	}
}

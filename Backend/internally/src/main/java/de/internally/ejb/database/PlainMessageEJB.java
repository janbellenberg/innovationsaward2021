package de.internally.ejb.database;

import java.util.List;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import de.internally.entities.database.PlainMessage;

/**
 * class for managing database operations for plain messages
 * @author janbellenberg
 * @see PlainMessage
 */
@Singleton
public class PlainMessageEJB {

	@PersistenceContext
	private EntityManager em;

	/**
	 * get messages of the chat that has been send before the lastIndex
	 * @param lastIndex id of the oldest message that has already been loaded
	 * @param chat id of the chat
	 * @return list of plain messages (max. 20)
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<PlainMessage> get(int lastIndex, int chat) {
		Query q;
		if(lastIndex == 0) {
			q = em.createNamedQuery("PlainMessage.findLatest");
			q.setParameter("chat", chat);
		} else {
			q = em.createNamedQuery("PlainMessage.findNext");
			q.setParameter("last", lastIndex);
			q.setParameter("chat", chat);
		}
		q.setMaxResults(20);
		
		return (List<PlainMessage>) q.getResultList();
	}

	/**
	 * get single message by id
	 * @param id id (PK) of the message
	 * @return instance of the message
	 */
	@Lock(LockType.READ)
	public PlainMessage getMessage(int id) {
		try {
			Query q = em.createNamedQuery("PlainMessage.findMessage");
			q.setParameter("message", id);
			return (PlainMessage) q.getSingleResult();

		} catch (NoResultException e) {
			return null;
		}
	}

	/**
	 * adds / updates the instance of a message in the database
	 * @param message instance of the message
	 * @return id (PK) of the message after the operation
	 */
	@Lock(LockType.WRITE)
	public int saveMessage(PlainMessage message) {
		return em.merge(message).getId();
	}

	/**
	 * deletes a message from the database
	 * @param message instance of the message that should be deleted
	 */
	@Lock(LockType.WRITE)
	public void deleteMessage(PlainMessage message) {
		Query q = em.createQuery("DELETE FROM PlainMessage WHERE id = :id");
		q.setParameter("id", message.getId());
		q.executeUpdate();
	}

	/**
	 * loads all messages that are marked as timed
	 * @return list of messages
	 */
	@SuppressWarnings("unchecked")
	@Lock(LockType.READ)
	public List<PlainMessage> getTimedMessages() {
		Query q = em.createNamedQuery("PlainMessage.findTimed");
		return (List<PlainMessage>) q.getResultList();
	}
}

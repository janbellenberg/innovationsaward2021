package de.internally.helper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * helper class for the configuration file
 * settings.json
 * @author janbellenberg
 *
 */
public class ConfigurationHelper {

	private ConfigurationHelper() { }

	/**
	 * read and parse entire configuration file
	 * @return parsed configuration
	 */
	private static JsonObject readConfigurationFile() {
		try {
			BufferedReader br = new BufferedReader(
					new FileReader(System.getProperty("jboss.server.data.dir") + "/internally/settings.json"));
			JsonReader reader = Json.createReader(br);
			JsonObject configuration = reader.readObject();
			reader.close();

			return configuration;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	/**
	 * check if configuration file contains all informations
	 * @return if the file is valid
	 */
	public static boolean validateConfiguration() {
		JsonObject conf = readConfigurationFile();
		
		if(conf == null || 
				!conf.containsKey("default-password") || 
				!conf.containsKey("login-notification") || 
				!conf.containsKey("2fa") || 
				!conf.containsKey("admin") || 
				!conf.containsKey("smtp")) { 
			return false;
		}
		
		conf = conf.getJsonObject("smtp");
		
		if(!conf.containsKey("username") || 
				!conf.containsKey("password") || 
				!conf.containsKey("host") || 
				!conf.containsKey("port") || 
				!conf.containsKey("from")) {
			return false;
		}
		
		return true;
	}

	/**
	 * loads the mails configuration
	 * @return mail configuration
	 */
	public static JsonObject getMailConfig() {
		return readConfigurationFile().getJsonObject("smtp");
	}

	/**
	 * checks, if 2FA is enabled
	 * @return if 2FA is enabled
	 */
	public static boolean isTwoFactorAuthEnabled() {
		return readConfigurationFile().getBoolean("2fa");
	}
	 
	/**
	 * checks, if login notifications are enabled
	 * @return if login notifications are enabled
	 */
	public static boolean isLoginNotificationEnabled() {
		return readConfigurationFile().getBoolean("login-notification");
	}
	
	/**
	 * loads the default password
	 * @return default password
	 */
	public static String getDefaultPassword() {
		return readConfigurationFile().getString("default-password");
	}
	
	/**
	 * loads configuration of the admin user
	 * @return configuration of the admin user
	 */
	public static JsonObject getAdmin() {
		return readConfigurationFile().getJsonObject("admin");
	}
}

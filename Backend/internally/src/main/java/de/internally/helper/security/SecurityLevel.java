package de.internally.helper.security;

/**
 * rights required to call the jax-rs resource
 * @author janbellenberg
 *
 */
public enum SecurityLevel {
	ADMIN,
	USER,
	BOTH
}

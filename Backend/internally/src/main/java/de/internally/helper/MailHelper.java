package de.internally.helper;

import javax.json.JsonObject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * helper class for sending mails
 * @author janbellenberg
 *
 */
public class MailHelper {
	
	private MailHelper() { }

	/**
	 * send a mail
	 * @param reciever receiver email-address
	 * @param subject subject of the email
	 * @param data content of the email
	 */
	public static void sendMail(String reciever, String subject, String data) {
		// create new thread
		new Thread(() -> {
			try {
				// load mail configuration
				JsonObject smtpConfig = ConfigurationHelper.getMailConfig();
	
				Session session = javax.mail.Session.getInstance(System.getProperties(), new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(smtpConfig.getString("username"), smtpConfig.getString("password"));
					}
				});
	
				// build message
				MimeMessage m = new MimeMessage(session);
				Address from = new InternetAddress(smtpConfig.getString("from"));
				Address[] to = new InternetAddress[] { new InternetAddress(reciever) };
				m.setFrom(from);
				m.setRecipients(Message.RecipientType.TO, to);
				m.setSubject(subject);
				m.setSentDate(new java.util.Date());
				m.setContent(data, "text/html; charset=UTF-8");
				Transport.send(m);
			} catch(MessagingException exception) {
				exception.printStackTrace();
			}
		}).start();
	}
}

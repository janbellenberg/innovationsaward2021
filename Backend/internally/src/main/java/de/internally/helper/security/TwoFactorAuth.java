package de.internally.helper.security;

import java.util.HashMap;
import java.util.TimerTask;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

import de.internally.entities.TimedData;

/**
 * manager class for two factor authentication (2FA)
 * @author janbellenberg
 *
 */
@Singleton
public class TwoFactorAuth {
	private HashMap<String, TimedData<String>> data;
	
	public TwoFactorAuth() {
		this.data = new HashMap<String, TimedData<String>>();
	}
	
	/**
	 * initialize the two factor authentication for a user when he logs in
	 * @param username the username of the user
	 * @return the verifications code
	 */
	@Lock(LockType.WRITE)
	public String start(String username) {
		if(this.data.containsKey(username))
			this.stop(username);
		
		// generate secure random string
		String content = SecurityHelper.generateSecureRandomString(10);
		
		// schedule disposal
		TimedData<String> data = new TimedData<String>(content, new TimerTask() {
			public void run() {
				TwoFactorAuth.this.data.remove(username);
			}
		}, 300000); // = 5 Minutes
		
		// store data
		this.data.put(username, data);
		
		return content;
	}

	/**
	 * remove the 2FA data for the user
	 * @param username according username
	 */
	@Lock(LockType.WRITE)
	public void stop(String username) {
		data.get(username).abortTimer();
		data.remove(username);
	}

	/**
	 * check the verifications code of a user
	 * @param username according username
	 * @param data according verifications code
	 * @return if request succeeded
	 */
	@Lock(LockType.READ)
	public boolean validate(String username, String data) {
		if(!this.data.containsKey(username))
			return false;
		
		return this.data.get(username).getData().equals(data);
	}
}
package de.internally.helper;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

/**
 * helper class for the server environment
 * @author janbellenberg
 *
 */
public class ServerEnvironment {

	/**
	 * check if application is running in a docker container
	 * @return is in docker environment
	 */
	public static boolean isInDocker() {
		try (Stream<String> stream = Files.lines(Paths.get("/proc/1/cgroup"))) {
			return stream.anyMatch(line -> line.contains("/docker"));
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * gets the hostname of the server
	 * @return hostname
	 */
	public static String getHostname() {
		
		// get hostname of host os in docker
		String hostInDocker = System.getenv("HOST");
		try {
			if(isInDocker() && hostInDocker != null) {
				// if in docker return hostname of host os
				return hostInDocker;
			}
			
			// else, return own hostname
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "localhost";
		}
	}

	/**
	 * load version of the software from the pom.xml file
	 * (only works after maven build)
	 * @return string of the verion
	 */
	public String getSoftwareVersion() {
		// just works with maven build
		try {
			MavenXpp3Reader reader = new MavenXpp3Reader();
			
			// load pom from resources
			Model model = reader.read(new InputStreamReader(
					this.getClass().getResourceAsStream("/META-INF/maven/de.internally/internally/pom.xml")));

			return model.getVersion();
		} catch (IOException | XmlPullParserException e) {
			e.printStackTrace();
			return "";
		} catch (NullPointerException e) {
			// if pom.xml is not in the resources, the application was not build using maven
			return "DEBUGGING";
		}
	}
	
	/**
	 * loads all ipv4 addresses for the hostname
	 * @param hostname hostname of the device
	 * @return list of ipv4 addresses
	 */
	public static Inet4Address[] getIPv4Addresses(String hostname) {
		try {
			InetAddress[] addresses = Inet4Address.getAllByName(hostname);
			List<InetAddress> result = new ArrayList<>();
			for(InetAddress a : addresses) {
				if(a instanceof Inet4Address) {
					result.add(a);
				}
			}
			
			return result.toArray(new Inet4Address[0]);
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * loads all ipv6 addresses for the hostname
	 * @param hostname hostname of the device
	 * @return list of ipv6 addresses
	 */
	public static Inet6Address[] getIPv6Addresses(String hostname) {
		try {
			InetAddress[] addresses = Inet6Address.getAllByName(hostname);
			List<InetAddress> result = new ArrayList<>();
			for(InetAddress a : addresses) {
				if(a instanceof Inet6Address) {
					result.add(a);
				}
			}
			
			return result.toArray(new Inet6Address[0]);
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return null;
		}
	}
}

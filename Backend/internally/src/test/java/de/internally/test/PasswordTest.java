package de.internally.test;

import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import de.internally.helper.security.SecurePassword;

/**
 * test class for password hashing
 * @author janbellenberg
 * @see SecurePassword
 */
public class PasswordTest {
	
	/**
	 * test, if the hash function is working properly
	 */
	@Test
	public void testHash() {
		String password = "test";
		String hash = "1d8ea7a0842ffa5cd6d574fe26feb433539a2ddc634fd277c34f51bdb90e84e019db72b8050208a30c86f5447ba80b695f337b47c694c1b969adadc490118a40";
		String salt = "TjFG0BtI2NEtpyzN7EIU";
		
		Assertions.assertEquals(hash, SecurePassword.generateHash(password, salt, "7"));
	}
	
	/**
	 * test, if the password matching function is working properly
	 */
	@Test
	public void testMatchPassword() {
		String hash = "1d8ea7a0842ffa5cd6d574fe26feb433539a2ddc634fd277c34f51bdb90e84e019db72b8050208a30c86f5447ba80b695f337b47c694c1b969adadc490118a40";
		String salt = "TjFG0BtI2NEtpyzN7EIU";
		
		Assertions.assertTrue(SecurePassword.matchPassword("test", hash, salt));
		Assertions.assertFalse(SecurePassword.matchPassword("internally", hash, salt));
	}
	
	/**
	 * test, if the length of the salt is correct
	 */
	@Test
	public void testSaltGen() {
		Assertions.assertEquals(20, SecurePassword.generateSalt(new ArrayList<String>()).length());
	}

}

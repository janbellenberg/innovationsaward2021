package de.internally.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import de.internally.helper.security.RsaHelper;

/**
 * test class for rsa signature and verification
 * @author janbellenberg
 * @see RsaHelper
 */
public class RsaTest {
	
	/**
	 * signs data and test if verification succeeds
	 */
	@Test
	public void testServerSignature() {
		String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA20F8VTzoe8MdEb7/+LEr"
				+ "bipoDyKMAuBkdRmzJRrxSmkGX44GnacX38vhJvcJJp7sFYNZALofRMxIcX7U+fXZ"
				+ "oDc1xJKaIgGBqZRse/KV4kllVDKdXjUd2K8O2OVhEZpi4aHuQfqRYmylWxwmETqQ"
				+ "Lwzrg08ae1XElXTgARL/2AW9PtHnLQYayZM8B63iUr/prdXxd57nvEnkdCHwx2zq"
				+ "tPGq28b7xFvD+NSouyouJWWmCVjPjCmPNSWg/HivNlo/m7yJs7SEnDfEMUBCa4V0"
				+ "vMPTTvPxJaryE8uM9k5dlMY9meD6es14ng7+h5b8viyqsQHUMhePzeozHkq8ZNq1"
				+ "gwIDAQAB";
		
		String signature = RsaHelper.sign("test");
		Assertions.assertFalse(signature == null);
		
		Assertions.assertTrue(RsaHelper.verify("test", signature, publicKey));
	}
}

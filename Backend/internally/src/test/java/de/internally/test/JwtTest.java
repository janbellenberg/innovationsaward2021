package de.internally.test;

import java.security.NoSuchAlgorithmException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import de.internally.helper.security.JWT;

/**
 * test class for json web token
 * @author janbellenberg
 * @see JWT
 */
public class JwtTest {

	/**
	 * test, if the parsing method is working properly
	 */
	@Test
	public void testJwtValidation() {
		 String jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsImlhdCI6MTYxODM5NTYzOX0.fOLbQpfu_T7UV7HerWHrFJAWPiSTO8F9rh6wg90SHbI";
			try {
				JWT parsed = JWT.parse(jwt);
				Assertions.assertFalse(parsed == null);
				Assertions.assertTrue(parsed.getPayload().containsKey("sub"));
				Assertions.assertEquals(1, parsed.getPayload().getInt("sub"));

			} catch (NoSuchAlgorithmException e) {
				Assertions.assertTrue(false);
				e.printStackTrace();
			}
	}
	
	/**
	 * test if jwt is not parsed when signature is invalid
	 */
	@Test
	public void testJwtWrongSignature() {
		String jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEsImlhdCI6MTYxODM5NTYzOX0.tyocxjGQPDoeIBorKI__ud5lC3gs0MquPArxzg9AoUc";
		try {
			JWT parsed = JWT.parse(jwt);
			Assertions.assertTrue(parsed == null);
			
		} catch (NoSuchAlgorithmException e) {
			Assertions.assertTrue(false);
			e.printStackTrace();
		}
	}
}

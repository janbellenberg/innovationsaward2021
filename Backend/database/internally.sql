CREATE DATABASE IF NOT EXISTS `internally` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE internally;

CREATE TABLE IF NOT EXISTS `departments` (
  `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  CONSTRAINT unique_name UNIQUE (`name`)
);

CREATE TABLE IF NOT EXISTS `locations` (
  `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `description` VARCHAR(50) NOT NULL,
  `address` VARCHAR(50) NOT NULL,
  `postcode` VARCHAR(5) NOT NULL,
  `city` VARCHAR(20) NOT NULL,
  `country` VARCHAR(2) NOT NULL DEFAULT "DE",
  CONSTRAINT unique_desc UNIQUE (`description`)
);

CREATE TABLE IF NOT EXISTS `groups` (
  `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `name` VARCHAR(20) NOT NULL,
  `administration` TINYINT(1) NOT NULL DEFAULT 0,
  `twoFA` TINYINT(1) NOT NULL DEFAULT 0,
  CONSTRAINT unique_name UNIQUE (`name`),
  CONSTRAINT check_admin CHECK (`administration` = 0 OR `administration` = 1),
  CONSTRAINT check_twoFA_group CHECK (`twoFA` = 0 OR `twoFA` = 1)
);

CREATE TABLE IF NOT EXISTS `employees` (
  `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL,
  `firstname` VARCHAR(100) NOT NULL,
  `lastname` VARCHAR(100) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `department` INT UNSIGNED NOT NULL,
  `location` INT UNSIGNED NOT NULL,
  `password` VARCHAR(128) NOT NULL,
  `salt` VARCHAR(20) NOT NULL,
  `gid` INT UNSIGNED NOT NULL,
  `twoFA` TINYINT(1) NOT NULL DEFAULT 0,
  `design` TINYINT(1) NOT NULL DEFAULT 0,
  `disconnect` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `status_note` VARCHAR(100) NOT NULL DEFAULT "Hey there, I'm using internally!",
  `status` TINYINT(1) NOT NULL DEFAULT 1,
  `rsa_public` BLOB DEFAULT NULL,
  `rsa_private` BLOB DEFAULT NULL,
  `master_key` BLOB DEFAULT NULL,
  CONSTRAINT unique_username UNIQUE (`username`),
  CONSTRAINT unique_email UNIQUE (`email`),
  CONSTRAINT unique_salt UNIQUE (`salt`),
  CONSTRAINT check_email CHECK (`email` LIKE "%@%.%"),
  CONSTRAINT check_twoFA CHECK (`twoFA` = 0 OR `twoFA` = 1),
  CONSTRAINT check_design CHECK (`design` = 0 OR `design` = 1),
  CONSTRAINT check_status CHECK (`status` BETWEEN 1 AND 3),
  CONSTRAINT check_rsa CHECK ((`rsa_public` IS NULL AND `rsa_private` IS NULL) OR (`rsa_public` IS NOT NULL AND `rsa_private` IS NOT NULL)),
  CONSTRAINT fk_department FOREIGN KEY (`department`) REFERENCES `departments` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT fk_location FOREIGN KEY (`location`) REFERENCES `locations` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT fk_group FOREIGN KEY (`gid`) REFERENCES `groups` (`id`) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS `ecdh_prekeys` (
  `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `employee` INT UNSIGNED NOT NULL,
  `public` BLOB NOT NULL,
  `private` BLOB NOT NULL,
  `signature` BLOB NOT NULL,
  CONSTRAINT unique_public UNIQUE (`public`),
  CONSTRAINT unique_private UNIQUE (`private`),
  CONSTRAINT unique_signature UNIQUE (`signature`),
  CONSTRAINT fk_owner FOREIGN KEY (`employee`) REFERENCES `employees` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `message_keys` (
  `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `owner` INT UNSIGNED NOT NULL,
  `peer` INT UNSIGNED NOT NULL,
  `send` TINYINT(1) UNSIGNED NOT NULL,
  `content` BLOB NOT NULL,
  `signature` BLOB NOT NULL,
  `usages` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  CONSTRAINT check_direction CHECK (`send` = 0 OR `send` = 1),
  CONSTRAINT fk_key_owner FOREIGN KEY (`owner`) REFERENCES `employees` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_key_peer FOREIGN KEY (`peer`) REFERENCES `employees` (`id`) ON UPDATE CASCADE ON DELETE CASCADE  
);

CREATE TABLE IF NOT EXISTS `chats` (
  `id` INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `pinned` INT UNSIGNED DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `members` (
  `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `chat` INT UNSIGNED NOT NULL,
  `employee` INT UNSIGNED NOT NULL,
  `member_since` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT unique_member UNIQUE (`chat`, `employee`),
  CONSTRAINT fk_chat FOREIGN KEY (`chat`) REFERENCES `chats` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_member FOREIGN KEY (`employee`) REFERENCES `employees` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `plain_messages` (
  `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `content` varchar(1000) DEFAULT '',
  `sender` INT UNSIGNED,
  `receiver` INT UNSIGNED NOT NULL,
  `send_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
  `edited` TINYINT(1) NOT NULL DEFAULT 0,
  `release_time` DATETIME DEFAULT NULL,
  CONSTRAINT check_edited CHECK (`edited` = 0 OR `edited` = 1),
  CONSTRAINT fk_plain_sender FOREIGN KEY (`sender`) REFERENCES `members` (`id`) ON UPDATE CASCADE ON DELETE SET NULL,
  CONSTRAINT fk_plain_receiver FOREIGN KEY (`receiver`) REFERENCES `chats` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE `chats` ADD CONSTRAINT fk_pinned_message FOREIGN KEY (`pinned`) REFERENCES `plain_messages` (`id`) ON UPDATE CASCADE ON DELETE CASCADE;

CREATE TABLE IF NOT EXISTS `encrypted_messages` (
  `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `content` BLOB NOT NULL,
  `signature` BLOB NOT NULL,
  `sender` INT UNSIGNED,
  `receiver` INT UNSIGNED NOT NULL,
  `sender_key` INT UNSIGNED,
  `receiver_key` INT UNSIGNED,
  CONSTRAINT check_keys CHECK ((`sender_key` IS NULL AND `receiver_key` IS NULL) OR (`sender_key` IS NOT NULL AND `receiver_key` IS NOT NULL)),
  CONSTRAINT fk_enc_sender FOREIGN KEY (`sender`) REFERENCES `employees` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_enc_receiver FOREIGN KEY (`receiver`) REFERENCES `employees` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_sender_pubkey FOREIGN KEY (`sender_key`) REFERENCES `ecdh_prekeys` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_receiver_pubkey FOREIGN KEY (`receiver_key`) REFERENCES `ecdh_prekeys` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `highlights` (
  `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `member` INT UNSIGNED NOT NULL,
  `message` INT UNSIGNED NOT NULL,
  CONSTRAINT unique_highlight UNIQUE (`member`, `message`),
  CONSTRAINT fk_highlight_employee FOREIGN KEY (`member`) REFERENCES `members` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_highlight_message FOREIGN KEY (`message`) REFERENCES `plain_messages` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `read_events` (
  `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `member` INT UNSIGNED NOT NULL,
  `message` INT UNSIGNED NOT NULL,
  `read_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT unique_read UNIQUE (`member`, `message`),
  CONSTRAINT fk_read_member FOREIGN KEY (`member`) REFERENCES `members` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_read_message FOREIGN KEY (`message`) REFERENCES `plain_messages` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `received_events` (
  `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `member` INT UNSIGNED NOT NULL,
  `message` INT UNSIGNED NOT NULL,
  `receive_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT unique_received UNIQUE (`member`, `message`),
  CONSTRAINT fk_received_member FOREIGN KEY (`member`) REFERENCES `members` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_received_message FOREIGN KEY (`message`) REFERENCES `plain_messages` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `content` VARCHAR(200) NOT NULL,
  `author` INT UNSIGNED NOT NULL,
  `same_department` TINYINT(1) NOT NULL DEFAULT 0,
  `same_location` TINYINT(1) NOT NULL DEFAULT 0,
  CONSTRAINT check_same_department CHECK (`same_department` = 0 OR `same_department` = 1),
  CONSTRAINT check_same_location CHECK (`same_location` = 0 OR `same_location` = 1),
  CONSTRAINT fk_author FOREIGN KEY (`author`) REFERENCES `employees` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

-- -- -- -- -- --
--    DATA     --
-- -- -- -- -- --

INSERT INTO `departments` (`name`) VALUES
("Marketing"),
("Java-Entwicklung");

INSERT INTO `locations` (`description`, `address`, `postcode`, `city`, `country`) VALUES 
("Essen", "Musterstr. 5", "45123", "Essen", "DE"),
("Duisburg", "Baumstr. 45", "47167", "Duisburg", "DE");

INSERT INTO `groups` (`name`, `administration`, `twoFA`) VALUES 
("Administrator", 1, 1),
("Benutzer", 0, 0);

INSERT INTO `employees` (`username`, `firstname`, `lastname`, `email`, `department`, `location`, `password`, `salt`, `gid`, `twoFA`, `design`, `status_note`, `status`, `rsa_public`, `rsa_private`) VALUES 
("janbellenberg", "Jan", "Bellenberg", "jan.bellenberg@outlook.de", 2, 1, "78089b987b20c9121de340a4e26dd0c2e6fd40fe474d20a75b9f392dd55c9dec2f55386ecd816a7c77acfdc96fc80556e02d62da6fc59df44682f80a1157a377", "1rhsYPQUGgyDfBXjlTHl", 1, 0, 0, ":-)", 1, "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAohaZoMrMX070LcUJnOgC\nJ4r9Vj/jfmGNQNuy33OaeTXU+t/MFFKsysE9swm9UuULWW3XZPf/AP5275g7LtXz\nHkN4BD6X8DZYLACpGXn91NYjdHbXYcs9/c1yBDFh9dqDlkM9idvExJOGQhYy9qUX\npoH9gVWpKBJLYlNfaf0h7jvw+YAVHqpMKRRLFuWYse36sxlbV/qvP81ciaN+lh1r\nwas+s5HStCBjsqgt0LXIiFyt5hOXTDNX/xCbQoMzDDr59rdG4sxNoS+jf0gX2Dzu\nZ56oURvL2Re4vJjzJXy/yJLPDgJ6QmdIvZD6Es5rVw1onc1Z2TgoG4hmpybiiHLZ\nIQIDAQAB\n-----END PUBLIC KEY-----\n", "0ab418a082436727dedaf90858d6827db712d21fcec0436892b2fedad8de0230f9bd5d11da63104c5a7b2b0afe93ed11486c9cfd97e931fded5b9956569f0bdab74e1ddbfdb7a65216a7da4f6636571ae67b0ec7ab9ad1191d3b0b7aca7c77642bc7dbed5c2f3ff5760eaed3790b88e8ddc7b803ceae3789c18aab0e1e013e2021b06d3b2f3aad248bf7388b64e865c7af1185f2a7148c276dcb6dfbac7a33c68932c52768b88d5b09f70d3cf23150a900599e4c261361e898b4ec006d81cffce75528930417a16bc8fea8f6d44ea336aa8aec4393ed7b2e4c4b93fea588ef62dbbd2a978a9ab25617d4558286a54688a6d994572d2ca2b6ac795e21ee0bfd399c4d30e2fc442afe3fb992e4c70025dd6f045f619ecde9a41cf6f90fe6fa6ceb749a84f47e15c9d9607ed11d51d658009066de3fad8569a38ed39fe13add74c1185bbf4f5eea6fd39d0ba90b847d133431909f01d07169e4ef27d77595801182ad79a6e62257d96c572a1c3c3d071f41dc61a6850469b36fece98df5f8aed1288aa9477574e1d44c3ef3f947edd9c510389e5c6eea738dfd58a145e27097da7f4b28f44e5f06851bef6d1f763435d5bce0b18cc9c4d3b263a37a0352c89daaa836e73ce6672f1ddb7ae784f98774f08810964a34935d08500984daad4318d5e3f8f65a79484adcfe591458f4460897f109301e9ae6d4132822e8d3f21d74d1354f034356f4683f96eb2e4612a5a9fccd18d8cd0b7193f10cf106bf6b2e369da9c4128aef2aec7c0510581988fc6e3b0d2e153804355cbd845883402385126555491921bd19731e325c0fb33314c480919ecc04417cdff19f4177bf42293e27cc21de34969e6fcee78bd398e9fac10ea251c9928c534f77654353485a35c11f759f5ce5d3b802427cb2390cf5ce49597840a00fa1528beef30a6f1379f2deb8d5022bf2d9c40b40d28b8a2bb01afd12838e49e63f56fa3a91abbe75683bac38f3940eb88c4a0eb65e3d079d46b5677de65095d12e970c16240fe4e85d79c971822f09dc3045aaf5974f3b42ac267f53bcab08e2b3544197e011c262dabab00b670294e0dcd631729381c2aa774b67def4b306ad2b7df80fb8cbb9fe75c16fe47e21583df0a6d966c887b5864c2d31994bddcc1a9b067a3f6e5b16a139a4ccc0097ee385737435684b6e266dd20544481f1cadc71c2853cf7779b7ba6e32fd8cec4524611d033b4b0ea4772158d33ffb7022872447e5406e4dcf2c031f1a2343908e66fa4646375e9a4585821a739ee92625da12b509f79c0bbd19eafff691f2ce9cdf540a79a15eb77e0e996a45d5d8eb9753ce880dda120912e4b66db5235998ad4c46d01b590661215c07c6f5aea9e67b6ecd6ee18690fc2b178edf98efb04b77d02d481e9db04addce94dd64cffb9c588974fb7d5e654230496fc7fd67497631f50595180a259e00b1bd1d71156844865470f770960aee73fc25ee079b9507b18dc5e282a6869721f736e133de75285be47e9e37c9a35ebcecad15ca2b87c5a8a3986a836e352ef8473076efa893514cab236276d919b3144d0cce71e309afb75cc54f99a36c59e5532f3010c2bea9f4cffa9de474783467d43b712a8fe7fff01e04deb004c3d7498c06baadee44255c43a557b8b7588778324ba3390b20e95ae6b858704be878527c1aae02b4fc7ef1b37dfa8299003a162ef717c3f4dd93ff0caeb738971c6f493865717e0c10d8beee3e1e53983e8cb4e14f5f432acbc90039fd48ff5c627bf8a7dbe4051368e318becaaff0e2aac3959259732b9c86c7bf93a8a0cc1b6f40fe1dd65d6a1211d1523b1416230a1b0cee4e5afd15c9816723d0b11f6a0b883cd6aabf91ab04b098b9cfac6d1603faf78659753bad8e1a91a56c16e57ed21ef0ac6a2d54ab47e5878a960205de1c3b2bb5ca0c00034037444057682880fed3f70bd5d39cc82d4ea213938bb215cad8ccbc47a7d4fdbc4f9cc5b2c81ff45a46f34f31735cdd552bd00588db89f1ee2bc4c279f087fabeca3d3b0f310e0b0838265754d6fbad3109603b4cea43e17360fbf527b6051d25b09932bc10759a9fae471b57b4263c971cd3645b8a666671fe7861da3c114730fb8dfff6d80e67fc7c0306a7d91b606f73b99cb7772dd588374716d447b8a692b515ff8430b24bf4a677b9d68f24c2e78c7e8f2995f63c634d5c4dcc7df8c6594128d33259194ea9f5c57fc3095535c6116ec610eec02123d174fbd9e5e8493ee6c1ae1c5ad5207f7c2257e6722c2b0f652552bb1580e1122351b4bdd48d4eb7065ade2df619b5ec06a0c31753b2cd78b75d2c2400bee2e82b3c5aae9f5c7d2d0dc8ad9d0c2cd4c69ca1f4d4"),
("simonmaddahi", "Simon", "Maddahi", "maddahi79@gmail.com", 2, 1, "326d3d391da7ae4c0b44e1756d4e2d148ed3c3a09c7df29db61feef40ef1629e62fb57c92074e9955c70eb4daf2fa804c738013c4c974df44a430bdc8404f7cd", "p2ez63qHQgXQpxSV0ZAr", 1, 0, 0, "...", 3, NULL, NULL);

INSERT INTO `chats` (`name`) VALUES ("Test");

INSERT INTO `members` (`chat`, `employee`) VALUES
(1, 1),
(1, 2);

INSERT INTO `plain_messages` (`content`, `sender`, `receiver`) VALUES
("Hallo", 1, 1);

INSERT INTO `highlights` (`member`, `message`) VALUES
(1, 1);

INSERT INTO `read_events` (`member`, `message`) VALUES
(2, 1);

INSERT INTO `notifications` (`content`, `author`) VALUES
("Das ist ein Test", 1);

-- -- -- -- -- --
--    USER     --
-- -- -- -- -- --

CREATE USER 'internally'@'%' IDENTIFIED BY 'gXg33Ep4urGp6bF2';
REVOKE ALL PRIVILEGES ON *.* FROM 'internally'@'%';
REVOKE GRANT OPTION ON *.* FROM 'internally'@'%';
GRANT SELECT, INSERT, UPDATE, DELETE ON `internally`.* TO 'internally'@'%';
FLUSH PRIVILEGES;
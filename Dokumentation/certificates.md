﻿# Generate root-ca certificate
openssl genrsa -aes256 -out ca-key.pem 4096
openssl req -x509 -new -nodes -extensions v3_ca -key ca-key.pem -days 365 -out ca-root.pem -sha512

# intermediate
openssl req -new -nodes -keyout internally-key.pem -out internally.csr -days 365 -config ./openssl.cnf
openssl ca -extensions v3_ca -days 365 -notext -md sha256 -in internally.csr -out internally.pem -extfile ./openssl.cnf -cert ./ca-root.pem -keyfile ./ca-key.pem -create_serial


# Generate certificate
openssl req -new -nodes -keyout server-key.pem -out server.csr -days 365 -config ./openssl.cnf -reqexts v3_req
openssl x509 -req -days 365 -in server.csr -CA internally.pem -CAkey internally-key.pem -CAcreateserial -out server.pem -extfile ./openssl.cnf -extensions v3_req

# Generate certificate chain
openssl pkcs12 -export -out server.pfx -in server.pem -inkey server-key.pem -certfile internally.pem

# Convert files
## PEM -> CRT
openssl x509 -outform der -in your-cert.pem -out your-cert.crt
## CRT -> PEM
openssl x509 -in mycert.crt -out mycert.pem -outform PEM
const CACHE_NAME = "v1.0.0";
const urlsToCache = ["offline.html", "offline.css", "particles.json", "particles.js", "favicon.ico", "raleway.ttf", "raleway-bold.ttf", "raleway-italic.ttf"];

const self = this;

// install service worker
self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then((cache) => {
        return cache.addAll(urlsToCache);
      })
  );
});

// listen for requests
self.addEventListener('fetch', function(event) {
  // don't cache api requests
  if(event.request.url.includes("/api")) {
    return;
  }

  event.respondWith(
    caches.match(event.request)   // try with cache first
      .then((response) =>  response || fetch(event.request))    // fallback to network
      .catch(() => caches.match('/offline.html'))   // fallback to offline.html
  );
});

// activate the sw
self.addEventListener('activate', (event) => {
  event.waitUntil(
    caches.keys().then((cacheNames) => Promise.all(
      cacheNames.map((cacheName) => {
        if(cacheName !== CACHE_NAME){
          return caches.delete(cacheName);
        }
        return caches;
      })
    ))
  );
});

let pingInterval = undefined;
let showedError = false;

// try to connect to the ws endpoint 
const connect = () => {
  // build connection url
  let isSecure = self.location.protocol.includes("https");
  const socket = new WebSocket(
    (isSecure ? "wss://": "ws://") + 
    self.location.hostname + 
    (isSecure ? ":8443" : ":8080") + 
    "/socket/chat");

  // when connection is successfully open
  socket.onopen = () => {
    socket.send(JSON.stringify({ method: "ping" }));
    if(showedError) {
      self.registration.showNotification("internally", {body: "Wieder online"});
    }
    showedError = false;
  };

  // when an error occurs
  const handleError = () => {
    if(pingInterval !== undefined) {
      clearInterval(pingInterval);
    }

    if(!showedError) {
      self.registration.showNotification("internally", {body: "Keine Verbindung zum Server"});
      showedError = true;
    }
    
    // try to reconnect after 30 sec.
    setTimeout(connect, 30000);
  };

  socket.onclose = (e) => {
    if(e.code !== 1000) // when the connection is not closed clean
      handleError();
  }
  socket.onerror = handleError;

  // when the client receives a message
  socket.onmessage = function (e) {
    let data = JSON.parse(e.data);

    if(data.method === "add") {
      const options = {
        body: data.content
      };

      var title = data.title + " (@" + data.sender.username + ")";
      self.registration.showNotification(title, options);   // show a notification
    }
  };

  // send ping signal
  pingInterval = setInterval(() => {
    if(socket.readyState === WebSocket.OPEN) {
      socket.send(JSON.stringify({ method: "ping" }));
    }
  }, 10000);
};

connect();
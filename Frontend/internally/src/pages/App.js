import React, { useState, useEffect } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import { updateUserInfo, showError, showWarn } from '../redux/Dispatcher';

import "../style/themes/light.css";
import "../style/themes/dark.css";
import "../style/colors.css";
import "../style/fonts.css";
import "../style/scrollbars.css";
import "../style/style.css";
import "../style/print.css";
import "../style/svg.css";

import Wait from '../components/Wait';
import Error from '../components/Error';

import Dashboard from './administration/dashboard';
import Employees from './administration/employees';
import Groups from './administration/groups';
import Departments from './administration/departments';
import Locations from './administration/locations';

import Login from './public/login';
import Logout from './public/logout';
import Info from './public/info';
import Readme from './public/readme';
import NotFound from './public/error/NotFound';
import Unauthorized from './public/error/Unauthorized';

import UserDashboard from './user/dashboard';
import Chat from './user/chat';
import Profile from './user/profile';
import UserEmployees from './user/employees';

import { uiInit } from '../api/Initialization';
import { resetSecureChat } from '../api/SecureChat';
import InitializationDialog from '../components/Dialog/InitializationDialog';

// list of pages that are public
const publicPages = ['/#/login', '/#/logout', '/#/info'];

/**
 * root component of the app
 * @function App
 * @see Dashboard
 * @see Employees
 * @see Groups
 * @see Departments
 * @see Locations
 * @see Login
 * @see Info
 * @see Readme
 * @see NotFound
 * @see Unauthorized
 * @see UserDashboard
 * @see Chat
 * @see Profile
 * @see UserEmployees
 */
const App = () => {
  const dispatch = useDispatch();
  const [showLogin, setShowLogin] = useState(true);
  const [user, setUser] = useState(useSelector(state => state.user));
  const [newRsa, setNewRsa] = useState(false);
  const [newMasterKey, setNewMasterKey] = useState(false);
  const [missingEcdh, setMissingEcdh] = useState(0);

  // check for permission for sending notification
  useEffect(() => {
    if (!("Notification" in window)) {
      dispatch(showError("Der Browser unterstützt keine Desktop-Benachrichtigungen"));
    } else if (Notification.permission !== "granted") {
      Notification.requestPermission().then((result) => {
        if (result === "granted") {
          new Notification("Die Anwendung ist nun online");
        } else{
          dispatch(showWarn("Bitte aktivieren Sie die Desktop-Benachrichtigungen"));
        }
      });
    }
  }, [dispatch]);

  // load ui-data 
  useEffect(() => {
    let href = window.location.href;
    if(!publicPages.some(p => href.includes(p))) {        // only for non-public pages
      uiInit().then(([userData, design, rsa, master, ecdh]) => {  // load data
        if(userData === null) {
          setShowLogin(true);
          return;
        }

        setShowLogin(false);
        dispatch(updateUserInfo(userData));
        setUser(userData);
        if(master && (!rsa || ecdh < 10)) {
          resetSecureChat().then(() => window.location.reload());
        } else {
          setNewRsa(rsa);
          setNewMasterKey(master);
          setMissingEcdh(ecdh);

          if(design) {
            document.querySelector("body").setAttribute("design", "dark");
          } else {
            document.querySelector("body").removeAttribute("design");
          }
        }
      });
    }
  }, [dispatch]);

  if(showLogin) {
    return (
      <>
        <Wait />
        <Error />
        <Login />
      </>
    );
  }

  return (
    <>
      <Wait />
      <Error />
      { // when the application needs to initialize secure chat
        (newRsa || newMasterKey || missingEcdh > 0) ? 
        <InitializationDialog
          missingEcdh={missingEcdh}
          newRsa={newRsa} 
          newMasterKey={newMasterKey} />
         : 
        <HashRouter>
          <Switch>
            <Route path="/login" exact component={Login}></Route>
            <Route path="/logout" exact component={Logout}></Route>
            <Route path="/info" exact component={Info}></Route>
            <Route path="/readme" exact component={Readme}></Route>
            
            <Route path="/" exact component={user.chat ? UserDashboard : Unauthorized}></Route>
            <Route path="/chat" exact component={user.chat ? Chat : Unauthorized}></Route>
            <Route path="/profile" exact component={user.chat ? Profile : Unauthorized}></Route>
            <Route path="/employees" exact component={user.chat ? UserEmployees : Unauthorized}></Route>

            <Route path="/administration" exact component={user.administration ? Dashboard : Unauthorized}></Route>
            <Route path="/administration/employees" exact component={user.administration ? Employees : Unauthorized}></Route>
            <Route path="/administration/groups" exact component={user.administration ? Groups : Unauthorized}></Route>
            <Route path="/administration/departments" exact component={user.administration ? Departments : Unauthorized}></Route>
            <Route path="/administration/locations" exact component={user.administration ? Locations : Unauthorized}></Route>

            <Route path="*" exact component={NotFound} />
          </Switch>
        </HashRouter>
      }
    </>
  );
}

export default App;
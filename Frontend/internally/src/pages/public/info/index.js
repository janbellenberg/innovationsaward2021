import React, {useState, useEffect} from 'react';
import GridLayout from '../../../components/GridLayout';

let lastValue = undefined;

/**
 * info page
 * @function Info
 * @see GridLayout
 */
const Info = () => {
  const [timer, setTimer] = useState(undefined);

  /**
   * update color easter egg
   */
  const updateColor = () => {
    let color = "";
    let random = undefined;
    do {
      random = parseInt(Math.random() * 100 % 4);
    } while(random === lastValue);
    lastValue = random;

    switch(random) {
      case 0: color = "accent"; break;
      case 1: color = "orange"; break;
      case 2: color = "green"; break;
      case 3: color = "yellow"; break;
      default: color = "accent"; break;
    }

    document.querySelector("main#grid-layout section:first-of-type").style.background="var(--" + color + ")";
  };

  // clear the interval when leaving the page 
  useEffect(() => {
    return () => clearInterval(timer);
  }, [timer]);

  return (
    <GridLayout title="Über internally">
      <img src="/icon-96.png" alt="Logo" onDoubleClick={() => {
        // just start a timer when none is running
        if(timer === undefined) {
          setTimer(setInterval(updateColor, 500));
        }
      }}/><p />
      <b>Firmeninterne Chatanwendung</b><p />
      Copyright &copy; 2021<br /> Jan Bellenberg &amp; Simon Maddahi<br />betreut von Herr Ganser
    </GridLayout>
  );
}

export default Info;
import React from 'react';
import {Link} from 'react-router-dom';
import GridLayout from '../../../components/GridLayout';
import { ReactComponent as Cloud } from "../../../svg/sad_cloud.svg";

import './Unauthorized.css';

/**
 * unauthorized page
 * @function Unauthorized
 * @see GridLayout
 */
const Unauthorized = () => {
  return (
    <GridLayout title={
      <span id="error-hover-effect"></span>
    } header="Upps...">
    <Cloud /> <p/>
      
      <div id="subtitle">
        Es scheint, als hätten Sie keinen Zugriff auf diese Seite!<p/>
        <em>401 - Unauthorized</em><p/>

        <Link to="/login">
          <input type="button" value="Zum Login"/> 
        </Link>

        <Link to="/">
          <input type="button" value="Zur Startseite"/>
        </Link>
      </div>
    </GridLayout>
  );
}

export default Unauthorized;
import React from 'react';
import {Link} from 'react-router-dom';
import GridLayout from '../../../components/GridLayout';
import { ReactComponent as Cloud } from "../../../svg/sad_cloud.svg";

import './NotFound.css';

/**
 * not found page
 * @function NotFound
 * @see GridLayout
 */
const NotFound = () => {

  let goBack = () => window.history.back();

  return (
    <GridLayout title={
      <span id="error-hover-effect"></span>
    } header="Upps...">
      <Cloud /> <p/>
      <div id="subtitle">
        Leider ist ein Fehler aufgetreten!<br/>
        Die Seite, die Sie suchen, konnte nicht gefunden werden<p/>
        <em>404 - Not Found</em><p/>

        <input type="button" value="Zurück" onClick={goBack} /> 
        <Link to="/">
          <input type="button" value="Zur Startseite"/>
        </Link>
      </div>
    </GridLayout>
  );
}

export default NotFound;
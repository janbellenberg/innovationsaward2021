import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {logout} from '../../../api/Authentication';

import GridLayout from '../../../components/GridLayout';
import { ReactComponent as LogoutIcon} from '../../../svg/Logout.svg';

/**
 * logout page
 * @function Logout
 * @see GridLayout
 */
const Logout = () => {
  const [success, setSuccess] = useState(false);

  // perform logout process
  useEffect(() => {
    logout().then((s) => setSuccess(s));
  }, []);

  return (
    <GridLayout title="Logout">
      <LogoutIcon style={{transform: 'scale(2)', margin: '1em 0'}} /><p/>
      { success ? 
        "Einen schönen Feierabend!" : 
        "Es ist ein Fehler aufgetreten"
      }
      <p/>
      <Link to="/login">Zurück zum Login</Link>
    </GridLayout>
  );
}

export default Logout;
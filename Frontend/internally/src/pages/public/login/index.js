import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import { login, STATUS_UNAUTHORIZED, STATUS_NOT_ACCEPTABLE } from '../../../api/Authentication';
import { STATUS_OK } from '../../../api/constants';


import GridLayout from '../../../components/GridLayout';
import PasswordToggle from '../../../components/PasswordToggle';
import { startTask } from '../../../redux/Dispatcher';
import "./Login.css";

/**
 * login page
 * @function Login
 * @see GridLayout
 * @see PasswordToggle
 */
const Login = () => {

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [code, setCode] = useState(undefined);
  const [failed, setFailed] = useState(false);
  const [passwordType, setPasswordType] = useState('password');

  const dispatch = useDispatch();

  /**
   * performs the login process
   */
 const doLogin = async () => {
    let data = {
      'username': username,
      'password': password,
      'twoFA': code
    }

    // send login request
    let status = await login(data);

    if(status === STATUS_OK) {    // when the login request succeeded
      window.location = username === "admin" ? "/#/administration" : "/#/";
      dispatch(startTask());
      window.location.reload();
    } else if (status === STATUS_UNAUTHORIZED) {  // when the credentials are wrong
      setFailed(true);
      setCode(undefined);
    } else if (status === STATUS_NOT_ACCEPTABLE) {    // when 2FA is required
      setFailed(false);
      setCode("");
    }
  }

  // for performing login on key press (enter)
  const handleKeyPress = (e) => e.key === 'Enter' ? doLogin() : null;

  return (
    <GridLayout title="Login">
      <div id="error" style={{display: failed && code === undefined ? 'block' : 'none'}}>
          Ihre Anmeldedaten sind nicht korrekt.
      </div><p/>

      <div id="login-form">
        {code === undefined ? 
          <>
            <input 
              value={username} 
              type="text" 
              placeholder="Benutzername"
              autoFocus="" 
              onChange={(e) => setUsername(e.target.value)} 
              onKeyPress={handleKeyPress} />

            <input 
              value={password} 
              type={passwordType}
              id="password"
              placeholder="Passwort" 
              onChange={(e) => setPassword(e.target.value)} 
              onKeyPress={handleKeyPress} />

            <PasswordToggle
              onShow={() => setPasswordType('text')}
              onHide={() => setPasswordType('password')} />
          </> :
          <input 
            value={code} 
            type="text"
            placeholder="Verifikationscode"
            autoFocus=""
            onChange={(e) => setCode(e.target.value)}
            onKeyPress={handleKeyPress} />
        }

        <input value="Anmelden" type="button" onClick={doLogin}/>
      </div>
    </GridLayout>
  );
}

export default Login;
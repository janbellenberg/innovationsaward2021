import React, {useState, useEffect} from 'react';
import ReactMarkdown from 'react-markdown';
import {startTask, stopTask} from '../../../redux/Dispatcher';
import store from '../../../redux/Store';

import { processTextResponse } from '../../../api';
import { STATUS_OK } from '../../../api/constants';
import Header from '../../../components/Header';

/**
 * readme page
 * @function Readme
 * @see Header
 * @see ReactMarkdown
 */
const Readme = () => {
  const [content, setContent] = useState("");

  // load readme file
  useEffect(() => {
    store.dispatch(startTask());
    fetch("/README.md").then(async response => {
      store.dispatch(stopTask());
      let markdown = await processTextResponse(response, STATUS_OK);
      setContent(markdown || "");   // set to content or empty string if undefined
    }).catch(() =>  store.dispatch(stopTask()));
  }, []);

  return (
    <>
      <Header title="README" />
      <main>
        <div className="wrapper" style={{display: "block", paddingRight: "1em", paddingBottom: "0"}}>
          <ReactMarkdown source={content} />
        </div>
      </main>
    </>
  );
};

export default Readme;
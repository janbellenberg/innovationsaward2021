import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {useSelector} from 'react-redux';

import ParticleBackground from '../../../components/ParticleBackground';

import NotificationList from '../../../components/Notification/List';
import Clock from '../../../components/Clock';
import StatusSwitch from '../../../components/StatusSwitch';

import Menu from '../../../components/DashboardMenu/User';
import './Dashboard.css';
import InboxElement from '../../../components/InboxElement';
import SecureMessageReceiveDialog from '../../../components/Dialog/SecureMessageReceiveDialog';
import SavedMessagesDialog from '../../../components/Dialog/SavedMessagesDialog';

import { loadMessageList, resetSecureChat } from '../../../api/SecureChat';

/**
 * user dashboard
 */
const UserDashboard = () => {

  const [savedMessagesDialog, setSavedMessagesDialog] = useState(false);
  const [selectedMessage, setSelectedMessage] = useState(undefined);
  const [secureMessages, setSecureMessages] = useState([]);

  /**
   * load notifications and secure messages
   * @function UserDashboard
   * @see ParticleBackground
   * @see NotificationList
   * @see Clock
   * @see Menu
   * @see StatusSwitch
   * @see InboxElement
   * @see SecureMessageReceiveDialog
   * @see SavedMessagesDialog
   */
  useEffect(() => {
    loadMessageList().then(result => setSecureMessages(result));
  }, []);

  const user = useSelector(s => s.user);
  const name = user.firstname + " " + user.lastname;

  return (
    <div id="dashboard">
      <aside>
        <ParticleBackground />
        <div id="content">
          <Clock/>
          <NotificationList />
        </div>
        <svg height="24" viewBox="0 0 24 24" width="24" onClick={() => document.querySelector("main").scrollIntoView({behavior: "smooth"})}><path d="M7.41,8.59L12,13.17l4.59-4.58L18,10l-6,6l-6-6L7.41,8.59z" fill="#fff"></path></svg>
      </aside>

      <main>
        <h1 id="welcome">Hallo, {name}</h1>
        <StatusSwitch />

        <section id="secure-chat">
          <fieldset>
            <legend>Sichere Nachrichten</legend>
            { secureMessages.length > 0 ? 
              secureMessages.map((message) => 
                <InboxElement key={message.next} item={message} onClick={() => setSelectedMessage(message)} />) : 
              <>
                <h2>
                  <span style={{margin: "0 0.7em 0 0.7em"}}>✔</span>
                  Keine verschlüsselten Nachrichten
                </h2>
              </>
            }
          </fieldset>
        </section><p/>

        <section style={{textAlign: "center"}}>
          <input type="button" value="Gespeicherte Nachrichten" onClick={() => setSavedMessagesDialog(true)} />
          <input type="button" value="Sicheren Chat zurücksetzen" onClick={() => resetSecureChat().then(() => window.location.reload())} />
          { (user !== null && user.administration) ? 
            <Link to="/administration"><input type="button" value="Administration öffnen"/></Link>
           : null
          }
        </section>

        <Menu/>
      </main>

      { selectedMessage === undefined ? null :
        <SecureMessageReceiveDialog
          onHide={() => {
            setSelectedMessage(undefined);
            loadMessageList().then(result => setSecureMessages(result));
          }}
          message={selectedMessage} />
      }

      {!savedMessagesDialog ? null :
        <SavedMessagesDialog
          onHide={() => setSavedMessagesDialog(false)} />
      }

    </div>
  );
}

export default UserDashboard;
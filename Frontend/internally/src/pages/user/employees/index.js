import React, {useState} from 'react';
import {useSelector} from 'react-redux';

import Header from '../../../components/Header';
import Navigation from '../../../components/Navigation/User';
import Property from '../../../components/Property';
import StatusIndicator from '../../../components/StatusIndicator';
import SideOverlay from '../../../components/SideOverlay';
import SecureMessageSendDialog from '../../../components/Dialog/SecureMessageSendDialog';
import { loadSecurityNumber } from '../../../api/SecureChat';
import { searchEmployees } from '../../../api/user/Employees';
import { loadForeignProfile } from '../../../api/user/Profile';

import './Employees.css';

/**
 * employees page
 * @function Employees
 * @see Header
 * @see StatusSwitch
 * @see Navigation
 * @see SideOverlay
 * @see Property
 * @see SecureMessageSendDialog
 */
const Employees = () => {

  const [searchResults, setSearchResults] = useState([]);
  const [selected, setSelected] = useState(undefined);
  const [showSecureMessageDialog, setShowSecureMessageDialog] = useState(false);
  const [showSideOverlay, setShowSideOverlay] = useState(true);

  const user = useSelector(s => s.user);

  /**
   * loads the employees matching the query
   * @param {string} query search query
   */
  const doSearch = (query) => {
    searchEmployees(query).then(result => setSearchResults(result));
  }

  /**
   * loads the selected employee
   * @param {int} id id of the employee
   */
  const loadEmployee = (id) => {
    loadForeignProfile(id).then(async result => {
      let sec = undefined;
      if(result.rsa_exist) {
        sec = await loadSecurityNumber(user.id, id);
      } 
      setSelected({...result, id: id, security_number: sec});
      setShowSideOverlay(false);
    })
  }

  return (
    <div id="employees">
      <Navigation active={4} />

      <div className="open-side" onClick={() => setShowSideOverlay(true)}>
        { window.innerWidth < 800 ? "➔" : "Mitarbeiter" }
      </div>

      <SideOverlay 
        open={showSideOverlay || selected === undefined}
        title="Mitarbeiter"
        searchBar
        onSearch={doSearch}
        onClose={() => setShowSideOverlay(false)}>

        <div id="employee-list">
          { searchResults.map((employee) => 
            <div 
              key={employee.id}
              className="employee-item"
              active={(selected !== undefined && selected.id === employee.id) ? "" : undefined}
              onClick={() => loadEmployee(employee.id) }>

              {employee.firstname} {employee.lastname}
            </div>
          ) }
        </div>
      </SideOverlay>

       
      <main>
        <Header title="Mitarbeiter" hasMenuButton={true}>
          { selected === undefined || !selected.rsa_exist ? null :
            <input
              type="button"
              onClick={() => setShowSecureMessageDialog(true)}
              value="Sichere Nachricht senden" />
          }
        </Header>

        { selected === undefined ? null :
          <div id="content-wrapper">
            <img
              id="profile-image"
              status={selected.status}
              src={"/api/v1/profile/" + selected.id + "/image/"}
              alt={selected.firstname + " " + selected.lastname} />
            
            <h1>{selected.firstname} {selected.lastname}</h1>
            <div id="uid">{selected.username}</div>
            <div id="status-note">{selected.message}</div>

            <StatusIndicator status={selected.status} />

            <Property title="E-Mail">{selected.email}</Property>
            <Property title="Abteilung">{selected.department}</Property>
            <Property title="Standort">{selected.location}</Property>
            <Property title="Gruppe">{selected.group}</Property>
          </div>
        }
      </main>

      { !showSecureMessageDialog ? null :
        <SecureMessageSendDialog
          employee={selected.id}
          security_number={selected.security_number}
          onHide={() => setShowSecureMessageDialog(false)} />
      }
    </div>
  );
}

export default Employees;
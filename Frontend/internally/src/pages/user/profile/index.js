import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {updateUserInfo} from '../../../redux/Dispatcher';

import Header from '../../../components/Header';
import StatusSwitch from '../../../components/StatusSwitch';
import Navigation from '../../../components/Navigation/User';
import Switch from '../../../components/Switch';
import Property from '../../../components/Property';

import './Profile.css';
import ImageDialog from '../../../components/Dialog/ImageDialog';
import PasswordDialog from '../../../components/Dialog/PasswortDialog';
import StatusTextDialog from '../../../components/Dialog/StatusTextDialog';

import {loadMyProfile} from '../../../api/user/Profile';
import {updateDesign, updateTwoFA} from '../../../api/user/Employees';

/**
 * profile page
 * @function Profile
 * @see Header
 * @see StatusSwitch
 * @see Navigation
 * @see Switch
 * @see Property
 * @see ImageDialog
 * @see PasswordDialog
 * @see StatusTextDialog
 */
const Profile = () => {
  const user = useSelector(s => s.user);
  const dispatch = useDispatch();

  const [infos, setInformations] = useState({});
  const [twoFA, setTwoFA] = useState(false);
  const [design, setDesign] = useState(user.design);
  const [imageSrc, setImageSrc] = useState("/api/v1/profile/" + user.id + "/image");
  const [showStatusDialog, setShowStatusDialog] = useState(false);
  const [showPictureDialog, setShowPictureDialog] = useState(false);
  const [showPasswordDialog, setShowPasswordDialog] = useState(false);

  useEffect(() => {
    loadProfile();
  }, [])

  /**
   * load profile
   */
  const loadProfile = () =>  {
    loadMyProfile().then(result => {
      setTwoFA(result.twoFA);
      delete result.twoFA;
      setInformations(result);
    });
  }

  return (
    <div id="profile">

      <Navigation active={2} />

      <main>
        <Header title="Mein Profil" hasMenuButton={true}>
          <input type="button" onClick={() => setShowStatusDialog(true)} value="Status ändern" />
          <input type="button" onClick={() => setShowPictureDialog(true)} value="Bild ändern" />
          <input type="button" onClick={() => setShowPasswordDialog(true)} value="Passwort ändern" />
        </Header>

        <div id="content-wrapper">
          <img id="profile-image" status={user.status} style={{backgroundImage: "url('" + imageSrc + "')"}} alt="" />
          
          <h1>{infos.firstname} {infos.lastname}</h1>
          <div id="uid">{infos.username}</div>
          <div id="status-note">{infos.message}</div>

          <StatusSwitch status={infos.status} />

          <Property title="E-Mail">{infos.email}</Property>
          <Property title="Abteilung">{infos.department}</Property>
          <Property title="Standort">{infos.location}</Property>
          <Property title="Gruppe">{infos.group}</Property>

          <div id="action-wrapper">
            { twoFA === undefined ? undefined : 
              <Switch title="Login in 2 Schritten" option1="Ja" option2="Nein" current={twoFA} onSwitch={(value) => {
                updateTwoFA(value).then(success => {
                  if(success) {
                    setTwoFA(value);
                  }
                })
              }} />
            }
            <Switch title="Design" option1="Hell" option2="Dunkel" current={!design} onSwitch={(value) => {
              value = !value;
              updateDesign(value).then(success => {
                if(success) {
                  let u = Object.assign({}, user);  //copy object
                  u.design = value;
                  dispatch(updateUserInfo(u));
                  setDesign(value);

                  if(value) {
                    document.querySelector("body").setAttribute("design", "dark");
                  } else {
                    document.querySelector("body").removeAttribute("design");
                  }
                }
              });
              }} />
          </div>
        </div>
      </main>

      { showStatusDialog ? 
        <StatusTextDialog
          current={infos.message}
          onUpdate={content => setInformations({...infos, message: content})}
          onHide={() => setShowStatusDialog(false)} /> : null
      }

      { showPictureDialog ? 
        <ImageDialog
          onUpdate={() => setImageSrc("/api/v1/profile/" + user.id + "/image?" + new Date().getTime())}
          onHide={() => setShowPictureDialog(false)} /> : null
      }

      { showPasswordDialog ? 
        <PasswordDialog
          onHide={() => setShowPasswordDialog(false)}/> : null
      }
    </div>
  );
}

export default Profile;
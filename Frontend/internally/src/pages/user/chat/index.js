import React, { useState, useEffect, useCallback, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { showWarn } from '../../../redux/Dispatcher';

import "./Chat.css";

import Header from '../../../components/Header';
import Navigation from '../../../components/Navigation/User';
import SideOverlay from '../../../components/SideOverlay';

import EditChatDialog from '../../../components/Dialog/EditChatDialog';
import MessageDetailsDialog from '../../../components/Dialog/MessageDetailsDialog';
import PinnedMessageDialog from '../../../components/Dialog/PinnedMessageDialog';
import TimedMessageDialog from '../../../components/Dialog/TimedMessageDialog';

import Message from '../../../components/Message';
import SendBar from '../../../components/SendBar';
import { addChat, loadChats, loadMessagesOfChat } from '../../../api/user/Chat';

let ws = undefined;
let pingTimer = undefined;

let host = window.location.hostname;
let port = window.location.port;
let protocol = window.location.protocol.includes("https") ? "wss://" : "ws://";

if(port === "3000" || port === "8081") // react debug port
  port = (protocol.includes("wss") ? 8443 : 8080);

/**
 * chat page
 * @function Chat
 * @see Header
 * @see SideOverlay
 * @see Navigation
 * @see EditChatDialog
 * @see MessageDetailsDialog
 * @see PinnedMessageDialog
 * @see TimedMessageDialog
 * @see Message
 * @see SendBar
 */
const Chat = () => {
  // STATE VARS
  const [chatID, setChatID] = useState(undefined);
  const [selectedMessage, setSelectedMessage] = useState(undefined);
  const [timedMessage, setTimedMessage] = useState(undefined);
  const [showSideBar, setShowSideBar] = useState(true);
  const [showChatDetailsDialog, setShowChatDetailsDialog] = useState(false);
  const [showPinnedDialog, setShowPinnedDialog] = useState(false);
  const [chats, setChats] = useState([]);
  const [messages, setMessages] = useState([]);
  const [canLoadMore, setCanLoadMore] = useState(true);

  const dispatch = useDispatch();
  const user = useSelector(s => s.user);
  const currentChat = chats.filter((chat) => chat.id === chatID)[0];
  const endRef = useRef(null);

  /**
   * send message with websocket
   * @param {string} message new message
   */
  const sendMessage = (message) => {
    let data = {
      method: "add",
      content: message
    }

    ws.send(JSON.stringify(data));
  }

  /**
   * send timed message with websocket
   * @param {string} message new message
   * @param {string} date date in yyyy-MM-dd
   * @param {string} time time in HH:mm
   */
  const sendTimedMessage = (message, date, time) => {
    let data = {
      method: "add",
      content: message,
      timestamp: date + " " + time
    }

    ws.send(JSON.stringify(data));
    setTimedMessage(undefined);
  }

  /**
   * updates selected message with websocket
   * @param {string} content new content of message
   */
  const updateMessage = (content) => {
    let data = {
      method: "update",
      id: selectedMessage,
      content: content
    }

    ws.send(JSON.stringify(data));
  }

  /**
   * deletes selected message with websocket
   */
  const deleteMessage = () => {
    let data = {
      method: "delete",
      id: selectedMessage
    }

    ws.send(JSON.stringify(data));
  }

  /**
   * load chats
   */
  const load = useCallback(() => {
    loadChats().then(result => setChats(result));
  }, []);

  /**
   * process a new received, message
   */
  const processMessage = useCallback((data) => {
    if(data.method === "add") {
      // ADD NEW MESSAGE
      let message = {
        id: data.id,
        content: data.content,
        sender: data.sender,
        star: false
      };

      setMessages([...messages, message]);    // add new message to the array
    } else if(data.method === "update") {
      // UPDATE MESSAGE CONTENT
      const index = messages.findIndex((item) => item.id === data.id);
      
      if(index >= 0) {
        setMessages([
          ...messages.slice(0, index),        // messages before
          {
            ...messages[index],               // object of existing message
            content: data.content             // with new content
          },
          ...messages.slice(index + 1)        // messages after
        ]);
      }
    } else if(data.method === "delete") {
      // DELETE MESSAGE
      setMessages(messages.filter((item) => item.id !== data.id));
    }
  }, [messages]);

  const sendPing = () => {
    if(ws.readyState === WebSocket.OPEN) {
      ws.send(JSON.stringify({ method: "ping" }));
    }
  };
  
  // LOAD CHATS
  useEffect(() => {
    load();
  }, [load]);

  // LOAD MESSAGES WHEN CHAT CHANGED
  useEffect(() => {
    if(chatID !== undefined) {
      loadMessagesOfChat(chatID)
        .then(result => {
          setCanLoadMore(result.length >= 20);
          return result;
        })
        .then(result => setMessages(result))
        .then(() => endRef.current?.scrollIntoView());
    }
  }, [chatID]);

  /**
   * connect to chat when selected chat changes
   */
  useEffect(() => {
    if(currentChat !== undefined) {
      ws = new WebSocket(
        protocol + 
        host + ":" + 
        port + 
        "/socket/chat?id=" + currentChat.id);
      
      ws.onopen = sendPing;
      ws.onmessage = (e) => processMessage(JSON.parse(e.data));
      ws.onclose = (e) => {
        if(e.code !== 1000 && e.wasClean) {
          dispatch(showWarn("Verbindung zum Server getrennt"));
        }
      };
      pingTimer = setInterval(sendPing, 10000);

      return () => {
        clearInterval(pingTimer);
        ws.close();
        ws = null;
      };
    }
  }, [currentChat, processMessage, dispatch]);

  return (
    <div id="chat">
      <Navigation active={1} />

      <div className="open-side" onClick={() => setShowSideBar(true)}>
        { window.innerWidth < 800 ? "➔" : "Chats" }
      </div>

      <SideOverlay
        open={showSideBar || currentChat === undefined}
        title="Chats"
        onClose={() => setShowSideBar(false)}>
        
        <div id="add-notification-btn" style={{top: "3em"}} onClick={() => {
          addChat().then(id => {
            if(id !== undefined) {
              load();
              setChatID(id);
            }
          });
        }}>+</div>

        <div id="chats-list" style={{overflowX: "auto", marginTop: "3em"}}>
          { chats.filter((c) => c.unread).concat(   // first show unread messages
              chats.filter((c) => !c.unread)
            ).map((chat) => 
            <div className="chat-item" 
              key={chat.id}
              active={chatID === chat.id ? "" : undefined} 
              unread={chat.unread ? "" : undefined} 
              onClick={() => {
                setChatID(chat.id);
                setShowSideBar(false);
              }}>

              <strong>{chat.name}</strong>
              { // show last message
                chat.last
              }
            </div>
          ) }
        </div>
      </SideOverlay>

      <main>
        <Header title={currentChat === undefined ? "Chat" : currentChat.name} hasMenuButton={true}>
          { currentChat === undefined ? null : 
            <>
              {canLoadMore ? 
                <input
                  type="button"
                  onClick={() => {
                    loadMessagesOfChat(chatID, messages[0].id).then(result => {
                      setMessages([...result, ...messages]);
                      setCanLoadMore(result.length >= 20);
                    });
                  }}
                  value="Mehr laden" />
              : null }

              <input
                type="button"
                onClick={() => setShowChatDetailsDialog(true)}
                value="Chat bearbeiten" />

              <input
                type="button"
                onClick={() => setShowPinnedDialog(true)}
                value=" "
                id="show-pin-dialog" />
            </>
          }
        </Header>

        <section id="chat-wrapper">
          { messages.map((message) => 
            <Message 
              key={message.id}
              content={message.content} 
              sender={message.sender.firstname + " " + message.sender.lastname}
              star={message.highlight}
              me={message.sender.id === user.id}
              onClick={() => setSelectedMessage(message.id)} />
          ) }
          <span ref={endRef} ></span>
        </section>

        {currentChat === undefined ? <div id="placeholder"></div> : 
          <SendBar
            onSend={sendMessage}
            onTimedSend={setTimedMessage} />
        }
      </main>

      { currentChat === undefined ? null : 
        <>
          { !showChatDetailsDialog ? null : 
            <EditChatDialog 
              chat={chats[chats.findIndex(item => item.id === chatID)]}
              onDelete={() => {
                setChatID(undefined);
                setShowChatDetailsDialog(false);
                load();
              }}
              onRename={(title) => {
                const index = chats.findIndex((item) => item.id === chatID);
                setChats([
                  ...chats.slice(0, index),
                  {
                    ...chats[index],
                    name: title
                  },
                  ...chats.slice(index + 1)
                ]);
              }}
              onHide={() => setShowChatDetailsDialog(false)} />
          }

          { selectedMessage === undefined ? null :
            <MessageDetailsDialog
              messageID={selectedMessage}
              onHighlight={() => {
                
                const index = messages.findIndex((item) => item.id === selectedMessage);
                setMessages([
                  ...messages.slice(0, index),        // messages before
                  {
                    ...messages[index],               // object of existing message
                    highlight: true
                  },
                  ...messages.slice(index + 1)        // messages after
                ]);
              }}
              onUpdateMessage={updateMessage}
              onDeleteMessage={deleteMessage}
              onHide={() => setSelectedMessage(undefined)} />
          }

          { !showPinnedDialog ? null : 
            <PinnedMessageDialog
              chatID={chatID}
              onHide={() => setShowPinnedDialog(false)} />
          }

          { timedMessage === undefined ? null : 
            <TimedMessageDialog
              message={timedMessage}
              onSend={sendTimedMessage}
              onHide={() => setTimedMessage(undefined)} />
          }
        </>
      }
    </div>
  );
}

export default Chat;
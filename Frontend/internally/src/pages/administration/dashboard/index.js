import React, {useState, useEffect, useReducer, useCallback} from 'react';
import { useSelector } from "react-redux";

import "./Dashboard.css";

import Clock from '../../../components/Clock';
import ParticleBackground from '../../../components/ParticleBackground';
import NotificationList from '../../../components/Notification/List';
import Menu from '../../../components/DashboardMenu/Administration';
import LineChart from '../../../components/LineChart';
import SettingsDialog from '../../../components/Dialog/SettingsDialog';

import NetworkInfo from '../../../components/ServerInfo/NetworkInfo';
import JavaInfo from '../../../components/ServerInfo/JavaInfo';
import OSInfo from '../../../components/ServerInfo/OSInfo';

import {getOsInformations, getNetworkInformations, getJavaInformations} from '../../../api/administration/Dashboard';

/**
 * administration dashboard
 * @function Dashboard
 * @see Clock
 * @see ParticleBackground
 * @see NotificationList
 * @see Menu
 * @see LineChart
 * @see SettingsDialog
 * @see NetworkInfo
 * @see JavaInfo
 * @see OSInfo
 */
const Dashboard = () => {

  // reducer for cpu and ram values
  const systemReducer = (state, action) => {
    if(action.type === "ADD_VALUE") {
      state.splice(0, 0, action.payload);
      state.splice(60 * 5 + 2, 1);
      return state;
    } else if(action.type === "CLEAR") {
      return [];
    } else {
      throw new Error();
    }
  }

  const [chart, setChart] = useState(null);

  const [showSettings, setShowSettings] = useState(false);

  let [os, setOsInfo] = useState(undefined);
  let [java, setJavaInfo] = useState(undefined);
  let [network, setNetworkInfo] = useState(undefined);
  let [sockets, setSockets] = useState(0);


  // useCallback is required to fix a re-initialization bug (when the state changes)!
  let [cpu, dispatchCPU] = useReducer(
    useCallback((state, action) => 
      systemReducer(state, action), [])
    , []);

  let [ram, dispatchRAM] = useReducer(
    useCallback((state, action) => 
      systemReducer(state, action), [])
    , []);

  let user = useSelector(state => state.user);

  /**
   * load server information
   */
  useEffect(() => {
    getOsInformations().then(result => setOsInfo(result));
    getNetworkInformations().then(result => setNetworkInfo(result));
    getJavaInformations().then(result => setJavaInfo(result));
  }, []);

  /**
   * connect to performance ws
   */
  useEffect(() => {
    let host = window.location.hostname;
    let port = window.location.port;
    let protocol = window.location.protocol.includes("https") ? "wss://" : "ws://";

    if(port === "3000" || port === "8081") // react debug port
      port = (protocol.includes("wss") ? 8443 : 8080);

    let socket = new WebSocket(protocol + host + ":" + port + "/socket/performance");
    socket.onmessage = (e) => {
      let data = JSON.parse(e.data);
      setSockets(data.sock);
      
      if(chart !== null && chart.chartInstance !== null) {
        dispatchCPU({type: "ADD_VALUE", payload: data.cpu});
        dispatchRAM({type: "ADD_VALUE", payload: data.ram});
        chart.chartInstance.update();
      }
    };

    return chart !== null ? null : () => {socket.close()};

  }, [chart]);

  return (
    <div id="dashboard" className="administration">
      <aside>
        <ParticleBackground />

        <div id="content">
          <Clock/>
          <NotificationList />
        </div>

        <svg height="24" viewBox="0 0 24 24" width="24" onClick={() => document.querySelector("main").scrollIntoView({behavior: "smooth"})}>
          <path d="M7.41,8.59L12,13.17l4.59-4.58L18,10l-6,6l-6-6L7.41,8.59z" fill="#fff"></path>
        </svg>
      </aside>

      <main>
        <h1 id="welcome">Hallo, {user.firstname} {user.lastname}</h1>

        <section id="infos">
          { os === undefined ? null : <OSInfo data={os} /> }
          { network === undefined ? null : <NetworkInfo data={{...network, sockets: sockets}} /> }
          { java === undefined ? null : <JavaInfo data={java} /> }
        </section>

        <section style={{textAlign: "center"}}>
          <input 
            type="button" 
            value="Einstellungen"
            onClick={() => setShowSettings(true)} />
        </section><p/>

        <LineChart onStart={(ref) => {
           if(chart === null && ref !== null) {
            setChart(ref);
          }
        }}
          border1="#4e7d96" init1={cpu}
          border2="#f35322" init2={ram}/>

        <Menu/>
      </main>

      { !showSettings ? null 
        : <SettingsDialog 
          onHide={() => setShowSettings(false)} />
      }
    </div>
  );
}

export default Dashboard;
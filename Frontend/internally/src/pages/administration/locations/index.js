import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {loadLocations, addLocation, updateLocation, deleteLocation} from '../../../api/administration/Locations';

import Empty from '../../../components/Empty';
import Header from '../../../components/Header';
import Navigation from '../../../components/Navigation/Administration';
import Related from "../../../components/Related";

import LocationDialog from "../../../components/Dialog/LocationDialog";
import LocationElement from '../../../components/Element/LocationElement';

const navigationItems = Object.freeze({
  dashboard: 0,
  users: 1,
  departments: 2,
  locations: 4,
  groups: 8
});

/**
 * adminstration locations page
 * @function Locations
 * @see Empty
 * @see Header
 * @see Navigation
 * @see Related
 * @see LocationDialog
 * @see LocationElement
 */
const Locations = () => {

  const [selected, setSelected] = useState(undefined);
  const [data, setData] = useState({});

  /**
   * load locations
   */
  useEffect(() => {
    loadLocations().then(result => setData(result));
  }, []);

  return (
    <div className="administration">
      <Header title="Standorte">
        <input type="button" onClick={() => setSelected(null)} value="Standort hinzufügen" />
      </Header>

      <Related>
        <li><Link to="/administration/departments">Abteilungen</Link></li>
      </Related>

      <Navigation active={navigationItems.locations}></Navigation>

      <main>
        <div className="wrapper">
        { data === undefined || data.length < 1 ? 
            <Empty description="Standorte"/> : 
            Array.from(data).map((location) => 
              <LocationElement 
                key={location.id}
                location={location} 
                onEdit={(item) => setSelected(item)} />
            )
          }
        </div>
      </main>

      {selected === undefined ? null :
        <LocationDialog
          location={selected}
          put={selected === null}
          onHide={() => setSelected(undefined)} 
          onAdd={async (location) => {
            // add location and reload
            await addLocation(location);
            setSelected(undefined);
            setData(await loadLocations());
          }}
          onUpdate={async (id, location) => {
            // update location and reload
            await updateLocation(id, location);
            setSelected(undefined);
            setData(await loadLocations());
          }}
          onDelete={async (id) => {
            // delete location and reload
            await deleteLocation(id);
            setSelected(undefined);
            setData(await loadLocations());
          }} />
      }

    </div>
  );
}

export default Locations;
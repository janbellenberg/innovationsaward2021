import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import Empty from '../../../components/Empty';
import Header from '../../../components/Header';
import Navigation from '../../../components/Navigation/Administration';
import Related from "../../../components/Related";

import EmployeeDialog from "../../../components/Dialog/EmployeesDialog";
import SearchEmployeeDialog from "../../../components/Dialog/SearchEmployeeDialog";
import EmployeeElement from '../../../components/Element/EmployeeElement';

import { searchEmployees, addEmployee, updateEmployee, deleteEmployee, resetPassword } from '../../../api/administration/Employees';
import { loadDepartments } from '../../../api/administration/Departments';
import { loadLocations } from '../../../api/administration/Locations';
import { loadGroups } from '../../../api/administration/Groups';
import { resetSecureChat } from '../../../api/SecureChat';

const navigationItems = Object.freeze({
  dashboard: 0,
  employees: 1,
  departments: 2,
  locations: 4,
  groups: 8
});

/**
 * administration employees page
 * @function Employees
 * @see Empty
 * @see Header
 * @see Navigation
 * @see Related
 * @see EmployeeDialog
 * @see EmployeeElement
 */
const Employees = () => {

  const [data, setData] = useState(undefined);
  const [groups, setGroups] = useState(undefined);
  const [departments, setDepartments] = useState(undefined);
  const [locations, setLocations] = useState(undefined);
  const [selected, setSelected] = useState(undefined);
  const [search, setSearch] = useState(true);

  /**
   * load groups, departments, locations for drop-down
   */
  useEffect(() => {
    loadGroups().then(result => setGroups(result));
    loadDepartments().then(result => setDepartments(result));
    loadLocations().then(result => setLocations(result));
  }, []);

  return (
    <div className="administration">
      <Header title="Mitarbeiter">
        <input type="button" onClick={() => setSelected(null)} value="Mitarbeiter hinzufügen" />
        <input type="button" onClick={() => setSearch(true)} value="Mitarbeiter suchen" />
      </Header>

      <Related>
        <li><Link to="/administration/groups">Gruppen</Link></li>
      </Related>

      <Navigation active={navigationItems.employees}></Navigation>

      <main>
        <div className="wrapper">
          { data === undefined || data.length < 1 ? 
            <Empty description="Mitarbeiter"/> : 
            Array.from(data).map((employee) => 
              <EmployeeElement 
                key={employee.id}
                employee={employee}
                groups={groups}
                departments={departments}
                locations={locations} 
                onEdit={(item) => setSelected(item)} />
            )
          }
        </div>
      </main>

      {selected === undefined ||
        groups === undefined ||
        departments === undefined ||
        locations === undefined ? null :
        <EmployeeDialog 
          put={selected == null} 
          onHide={() => setSelected(undefined)} 
          employee={selected} 
          groups={Array.from(groups)} 
          departments={Array.from(departments)} 
          locations={Array.from(locations)} 
          onAdd={async (employee) => {
            // add employee and reload
            await addEmployee(employee);
            setSelected(undefined);
            setData(await searchEmployees(employee));
          }} 
          onUpdate={async (id, employee) => {
            // add employee and reload
            await updateEmployee(id, employee);
            setSelected(undefined);
            setData(await searchEmployees(employee));
          }} 
          onDelete={async (id) => {
            // delete employee and show search dialog
            await deleteEmployee(id);
            setSelected(undefined);
            setData(undefined);
            setSearch(true);
          }} 
          onResetPassword={(id) => {
            resetPassword(id);
            resetSecureChat(id);
          }}/>
      }

      {!search ||
        groups === undefined ||
        departments === undefined ||
        locations === undefined ? null :
        <SearchEmployeeDialog
          groups={Array.from(groups)} 
          departments={Array.from(departments)} 
          locations={Array.from(locations)} 
          onHide={() => setSearch(false)} 
          onSearch={async (employee) => {
            setData(await searchEmployees(employee));
            setSearch(false);
          }}/>
      }
    </div>
  );
}

export default Employees;
import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {loadDepartments, addDepartment, updateDepartment, deleteDepartment} from '../../../api/administration/Departments';

import Empty from '../../../components/Empty';
import Header from '../../../components/Header';
import Navigation from '../../../components/Navigation/Administration';
import Related from "../../../components/Related";

import DepartmentDialog from "../../../components/Dialog/DepartmentDialog";
import DepartmentElement from '../../../components/Element/DepartmentElement';

const navigationItems = Object.freeze({
  dashboard: 0,
  users: 1,
  departments: 2,
  locations: 4,
  groups: 8
});

/**
 * administration department page
 * @function Departments
 * @see Empty
 * @see Header
 * @see Navigation
 * @see Related
 * @see DepartmentDialog
 * @see DepartmentElement
 */
const Departments = () => {

  const [selected, setSelected] = useState(undefined);
  const [data, setData] = useState(undefined);

  /**
   * load departments
   */
  useEffect(() => {
    loadDepartments().then(result => setData(result));
  }, []);

  return (
    <div className="administration">
      <Header title="Abteilungen">
        <input type="button" onClick={() => setSelected(null)} value="Abteilung hinzufügen" />
      </Header>

      <Related>
        <li><Link to="/administration/locations">Standorte</Link></li>
      </Related>

      <Navigation active={navigationItems.departments}></Navigation>

      <main>
        <div className="wrapper">
          { data === undefined || data.length < 1 ? 
            <Empty description="Abteilungen"/> : 
            Array.from(data).map((department) => 
              <DepartmentElement 
                key={department.id}
                department={department} 
                onEdit={(item) => setSelected(item)} />
            )
          }
        </div>
      </main>

      {selected === undefined ? null :
        <DepartmentDialog 
          department={selected} 
          put={selected === null} 
          onHide={() => setSelected(undefined)} 
          onAdd={async (name) => {
            // add department and reload
            await addDepartment({name: name});
            setSelected(undefined);
            setData(await loadDepartments());
          }} 
          onUpdate={async (id, name) => {
            // update department and reload
            await updateDepartment(id, {name: name});
            setSelected(undefined);
            setData(await loadDepartments());
          }} 
          onDelete={async (id) => {
            // delete department and reload
            await deleteDepartment(id);
            setSelected(undefined);
            setData(await loadDepartments());
          }} /> 
      }
    </div>
  );
}

export default Departments;
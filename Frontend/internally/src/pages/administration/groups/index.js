import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {loadGroups, addGroup, updateGroup, deleteGroup} from '../../../api/administration/Groups';

import Empty from '../../../components/Empty';
import Header from '../../../components/Header';
import Navigation from '../../../components/Navigation/Administration';
import Related from "../../../components/Related";

import GroupDialog from "../../../components/Dialog/GroupDialog";
import GroupElement from '../../../components/Element/GroupElement';

const navigationItems = Object.freeze({
  dashboard: 0,
  users: 1,
  departments: 2,
  locations: 4,
  groups: 8
});

/**
 * administration groups page
 * @function Groups
 * @see Empty
 * @see Header
 * @see Navigation
 * @see Related
 * @see GroupDialog
 * @see GroupElement
 */
const Groups = () => {
  const [selected, setSelected] = useState(undefined);
  const [data, setData] = useState({});

  /**
   * load groups
   */
  useEffect(() => {
    loadGroups().then(result => setData(result));
  }, []);

  return (
    <div className="administration">
      <Header title="Gruppen">
        <input type="button" onClick={() => setSelected(null)} value="Gruppe hinzufügen" />
      </Header>

      <Related>
        <li><Link to="/administration/users">Benutzer</Link></li>
      </Related>

      <Navigation active={navigationItems.groups}></Navigation>

      <main>
        <div className="wrapper">
          { data === undefined || data.length < 1 ? 
            <Empty description="Gruppen"/> :
            Array.from(data).map((group) => 
              <GroupElement 
                key={group.id} 
                group={group} 
                onEdit={(item) => setSelected(item)} />
            )
          }
        </div>
      </main>

      {selected === undefined ? null :
        <GroupDialog 
          group={selected} 
          put={selected === null} 
          onHide={() => setSelected(undefined)} 
          onAdd={async (group) => {
            // add group and reload
            await addGroup(group);
            setSelected(undefined);
            setData(await loadGroups());
          }} 
          onUpdate={async (id, group) => {
            // update group and reload
            await updateGroup(id, group);
            setSelected(undefined);
            setData(await loadGroups());
          }} 
          onDelete={async (id) => {
            // delete group and reload
            await deleteGroup(id);
            setSelected(undefined);
            setData(await loadGroups());
          }} />
      }

    </div>
  );
}

export default Groups;
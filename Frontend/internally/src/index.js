import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import Store from './redux/Store';

import App from './pages/App';

ReactDOM.render(
  <Provider store={Store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

if('serviceWorker' in navigator)
  navigator.serviceWorker.register('/service-worker.js');
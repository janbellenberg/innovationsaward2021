import {createStore} from 'redux';
import Reducers from './reducers';

const DEBUG = true;

let extension = undefined;

// use Redux Dev Tools https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=de
if(DEBUG)
  extension = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

// create redux store
let Store = createStore(Reducers, extension);

export default Store;
/**
 * (REDUX) start task (wait screen)
 * @returns action for dispatch
 * @function startTask
 */
export const startTask = () => {
  return {
    type: 'START_TASK'
  }
};

/**
 * (REDUX) stop task (wait screen)
 * @returns action for dispatch
 * @function stopTask
 */
export const stopTask = () => {
  return {
    type: 'STOP_TASK'
  }
};

/**
 * (REDUX) shows error message
 * @param {string} error error message
 * @returns action for dispatch
 * @function showError
 */
export const showError = (error) => {
  let payload = {
    details: error,
    warn: false
  };

  return {
    type: 'SHOW_ERROR',
    payload: payload
  }
};

/**
 * (REDUX) shows warn message
 * @param {string} error error / warn message
 * @returns action for dispatch
 * @function showWarn
 */
export const showWarn = (error) => {
  let payload = {
    details: error,
    warn: true
  };

  return {
    type: 'SHOW_ERROR',
    payload: payload
  }
};

/**
 * (REDUX) hides error / warn message
 * @returns action for dispatch
 * @function hideError
 */
export const hideError = () => {
  return {
    type: 'HIDE_ERROR'
  }
};


/**
 * (REDUX) set user information for global access
 * @param {object} data user information
 * @returns action for dispatch
 * @function updateUserInfo
 */
export const updateUserInfo = (data) => {
  return {
    type: 'UPDATE_USER',
    payload: data
  }
}

/**
 * (REDUX) deletes user information
 * @returns action for dispatch
 * @function logout
 */
export const logout = () => {
  return {
    type: 'LOGOUT'
  }
};
/**
 * Update error message
 * @param {string} state current message
 * @param {object} action action to execute
 * @returns new state
 * @function ErrorReducer
 */
const ErrorReducer = (state = null, action) => {
  switch(action.type) {
    case 'SHOW_ERROR': return action.payload;
    case 'HIDE_ERROR': return null;
    default: return state;
  }
}

export default ErrorReducer;
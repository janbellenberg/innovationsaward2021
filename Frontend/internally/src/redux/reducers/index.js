import ErrorReducer from './ErrorReducer';
import UserInfoReducer from './UserInfoReducer';
import WaitReducer from './WaitReducer';

import {combineReducers} from 'redux';

/**
 * combine all reducers
 * @constant Reducers
 */
const Reducers = combineReducers({
  error: ErrorReducer,
  user: UserInfoReducer,
  waiting: WaitReducer
});

export default Reducers;
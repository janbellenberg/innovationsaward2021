/**
 * Update count of running tasks
 * @param {int} state current count of running tasks
 * @param {object} action action to execute
 * @returns new state
 * @function WaitReducer
 */
const WaitReducer = (state = 0, action) => {
  if(state < 0)
    state = 0;

  switch(action.type) {
    case 'START_TASK': return state + 1;
    case 'STOP_TASK': return state - 1;
    default: return state;
  }
}

export default WaitReducer;
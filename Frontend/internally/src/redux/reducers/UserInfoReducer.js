/**
 * 
 * @param {object} state current state
 * @param {object} action action to execute
 * @returns new state
 * @function UserInfoReducer
 */
const UserInfoReducer = (state = {}, action) => {
  switch(action.type) {
    case 'UPDATE_USER': return action.payload;
    case 'LOGOUT': return {};
    default: return state;
  }
};

export default UserInfoReducer;
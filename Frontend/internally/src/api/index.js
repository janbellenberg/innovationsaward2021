import {showError, showWarn} from '../redux/Dispatcher';
import store from '../redux/Store';

/**
 * processes a http response and parses json
 * @param {object} response result from <await fetch>
 * @param {int} expectedStatus expected HTTP statuscode
 * @returns parsed json response body
 * @function processJsonResponse
 */
export const processJsonResponse = async (response, expectedStatus) => {
  let data = await parseJSON(response, true);
  data = data || {};

  if(data === undefined)
    return undefined;

  if(response.status !== expectedStatus) {

    const action = response.ok ? 
      showWarn("Die Schnittstelle scheint veraltet zu sein") : // if the response is ok, but not as exacted -> maybe deprecated
      showError(data.message || response.statusText);         // show error message or if this is undefined then show http status text

    store.dispatch(action);
    
    return undefined;
  }

  return data;
};

/**
 * processes a http response and extracts plain text content
 * @param {object} response result from <await fetch>
 * @param {int} expectedStatus expected HTTP statuscode
 * @returns request body
 * @function processTextResponse
 */
export const processTextResponse = async (response, expectedStatus) => {
  let data = await response.text();

  if(data === undefined)
    return undefined;

  if(response.status !== expectedStatus) {
    let json = await parseJSON(response, true);
    json = json || {};

    const action = response.ok ? 
      showWarn("Die Schnittstelle scheint veraltet zu sein") : // if the response is ok, but not as exacted -> maybe deprecated
      showError(json.message || response.statusText);         // show error message or if this is undefined then show http status text

    store.dispatch(action);
    
    return undefined;
  }

  return data;
};

/**
 * processes a http response with no body
 * @param {object} response result from <await fetch>
 * @param {int} expectedStatus expected HTTP statuscode
 * @returns true -> success, false -> failure
 * @function processPlainResponse
 */
export const processPlainResponse = async (response, expectedStatus) => {
  if(response.status !== expectedStatus) {
    let data = await parseJSON(response, false);
    data = data || {};

    const action = response.ok ? 
      showWarn("Die Schnittstelle scheint veraltet zu sein") : // if the response is ok, but not as exacted -> maybe deprecated
      showError(data.message || response.statusText);         // show error message or if this is undefined then show http status text

    store.dispatch(action);

    return false;
  }

  return true;
};

/**
 * 
 * @param {object} response result from <await fetch> 
 * @param {boolean} showErrorOnFailure if error message should be shown
 * @returns parsed json
 * @function parseJSON
 */
const parseJSON = async (response, showErrorOnFailure) => {
  try {
    return await response.json();
  } catch(e) {
    if(showErrorOnFailure) {
      store.dispatch(
        showError(response.statusText || "Es ist ein unbekannter Fehler aufgetreten")
      );
    }

    return undefined;
  }
}
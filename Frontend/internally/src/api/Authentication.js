import {startTask, stopTask} from '../redux/Dispatcher';
import store from '../redux/Store';

import {processPlainResponse} from '.';
import {STATUS_OK, ROOT_URI} from './constants';

export const STATUS_UNAUTHORIZED = 401;
export const STATUS_NOT_ACCEPTABLE = 406;

const AUTH_URI = ROOT_URI + "/authentication";

/**
 * performs the login process
 * @param {object} data form data
 * @returns response http statuscode
 * @function Login
 * @see processPlainResponse
 */
export const login = async (data) => {
  store.dispatch(startTask());

  let response = await fetch(AUTH_URI + "/login", {
    method: "POST", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data)
  });

  store.dispatch(stopTask());

  if(response.status === STATUS_UNAUTHORIZED ||       // credentials are wrong
      response.status === STATUS_NOT_ACCEPTABLE ||    // two factor auth is required but missing
      response.status === STATUS_OK) {                // everything is ok
    
    return response.status;
  }

  processPlainResponse(response, STATUS_OK);
};

/**
 * performs the logout process
 * @returns if the request succeeded
 * @function logout
 * @see processPlainResponse
 */
export const logout = async () => {
  store.dispatch(startTask());

  let response = await fetch(AUTH_URI + "/logout", {
    method: "DELETE" 
  });

  store.dispatch(stopTask());

  return await processPlainResponse(response, STATUS_OK);
};
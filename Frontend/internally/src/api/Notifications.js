import {startTask, stopTask} from '../redux/Dispatcher';
import store from '../redux/Store';

import {processJsonResponse, processPlainResponse} from '.';
import {STATUS_OK, STATUS_CREATED, STATUS_NO_CONTENT, ROOT_URI} from './constants';

const BASE_URI = ROOT_URI + "/notifications";

/**
 * loads the notifications
 * @returns list of notifications
 * @function loadNotifications
 * @see processJsonResponse
 */
export const loadNotifications = async () => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI);
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || [];
};

/**
 * creates a new notification
 * @param {object} data data of the new notification
 * @function addNotification
 * @see processPlainResponse
 */
export const addNotification = async (data) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI, {
    method: "POST", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data)
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_CREATED);
};

/**
 * deletes a notification
 * @param {int} id id of the notification
 * @function deleteNotification
 * @see processPlainResponse
 */
export const deleteNotification = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id, {
    method: "DELETE"
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};
import {startTask, stopTask, showError} from '../redux/Dispatcher';
import store from '../redux/Store';

import {processJsonResponse, processPlainResponse} from '.';
import {STATUS_OK, STATUS_CREATED, ROOT_URI, STATUS_NO_CONTENT} from './constants';

import AES from '../encryption/AES';
import RSA from '../encryption/RSA';
import ECDH from '../encryption/ECDH';
import Hash from '../encryption/Hash';

const BASE_URI = ROOT_URI + "/secure_chat";

/**
 * used algorithms for the dialogs to display
 */
export const algorithms = {
  encryption: {
    name: " AES ",
    details: "(256 Bit CBC mit PKCS5)"
  },
  verification: {
    name: " RSA ",
    details: ""
  },
  keyExchange: {
    name: " ECDH ",
    details: "(secp521r1)"
  },
  keyDerivation: {
    name: " PPBKDF2 ",
    details: ""
  }
}

/**
 * Load the public keys and calculates the security number
 * @param {int} e1 id of the first employee
 * @param {int} e2 id of the other employee
 * @returns the calculated security number
 * @function loadSecurityNumber
 * @see processJsonResponse
 * @see RSA
 * @see Hash
 */
export const loadSecurityNumber = async (e1, e2) => {
  store.dispatch(startTask());
  const rsa = new RSA();
  const hash = new Hash();

  // GET FIRST PUBLIC KEY
  let res = await fetch(BASE_URI + "/rsa/" + e1);
  let rsaResponse = await processJsonResponse(res, STATUS_OK);
  if(rsaResponse === undefined){
    store.dispatch(stopTask());
    return null;
  }

  const val1 = rsaResponse.public;

  // verify salt server signature
  if(!rsa.verifyServerAuthority(val1, rsaResponse["server_signature"])) {
    store.dispatch(stopTask());
    store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
    return false;
  }

  // GET SECOND PUBLIC KEY
  res = await fetch(BASE_URI + "/rsa/" + e2);
  rsaResponse = await processJsonResponse(res, STATUS_OK);
  if(rsaResponse === undefined){
    store.dispatch(stopTask());
    return null;
  }

  const val2 = rsaResponse.public;

  // verify salt server signature
  if(!rsa.verifyServerAuthority(val2, rsaResponse["server_signature"])) {
    store.dispatch(stopTask());
    store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
    return false;
  }

  const security_number = e1 < e2 ? 
    hash.getSHA512Hash(val1 + val2) : 
    hash.getSHA512Hash(val2 + val1);

  store.dispatch(stopTask());

  return security_number;
};

/**
 * loads the salt value from the server
 * @param {int} id employee id
 * @returns the salt value
 * @function loadSalt
 * @see processJsonResponse
 */
export const loadSalt = async (id) => {
  store.dispatch(startTask());
  let res = await fetch(ROOT_URI + "/employees/" + id + "/salt");
  const response = await processJsonResponse(res, STATUS_OK);
  store.dispatch(stopTask());

  return response === undefined ? undefined : response.salt;
}

/**
 * deletes all secure messages, the rsa keys, all ecdh pre-keys and all message keys
 * @function resetSecureChat
 * @see processPlainResponse
 */
export const resetSecureChat = async () => {
  store.dispatch(startTask());
  let res = await fetch(BASE_URI + "/reset", {method: "POST"});
  await processPlainResponse(res, STATUS_NO_CONTENT);
  store.dispatch(stopTask());
};

/**
 * loads the master key of the current user and validates it
 * @returns null if invalid, else the key
 * @function loadMasterKey
 * @see processJsonResponse
 * @see RSA
 */
export const loadMasterKey = async () => {
  store.dispatch(startTask());
  let res = await fetch(BASE_URI + "/master");
  const response = await processJsonResponse(res, STATUS_OK);
  store.dispatch(stopTask());

  return response === undefined || !new RSA().verifyServerAuthority(response.master_key, response.server_signature) ? undefined : response.master_key;
}

/**
 * Loads the rsa private key
 * @returns the rsa private of the current user
 * @function loadRsaPrivate
 * @see processJsonResponse
 */
export const loadRsaPrivate = async () => {
  store.dispatch(startTask());
  let res = await fetch(BASE_URI + "/rsa");
  const response = await processJsonResponse(res, STATUS_OK);
  store.dispatch(stopTask());

  return response === undefined ? undefined : response.private;
};

/**
 * Loads a list with the new messages from the server
 * @returns list of unread, encrypted messages
 * @function loadMessageList
 * @see processJsonResponse
 */
export const loadMessageList = async () => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/messages");
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || [];
};

/**
 * Encrypts a message and sends it
 * @param {string} content not encrypted content
 * @param {int} receiverID id of the receiver
 * @param {int} senderID id of the sender
 * @param {string} password password of the sender
 * @returns true on success, else false
 * @function sendSecureMessage
 * @see AES
 * @see RSA
 * @see ECDH
 * @see processJsonResponse
 * @see processPlainResponse
 */
export const sendSecureMessage = async (content, receiverID, senderID, password) => {
  store.dispatch(startTask());

  try {
    // fetch salt
    let res = await fetch(ROOT_URI + "/employees/" + senderID + "/salt");
    const saltResponse = await processJsonResponse(res, STATUS_OK);
    if(saltResponse === undefined){
      store.dispatch(stopTask());
      return false;
    }

    const salt = saltResponse.salt;

    let master = await loadMasterKey();
    if(master === undefined) {
      store.dispatch(stopTask());
      return false;
    }

    // init algorithms
    const aes = new AES(salt);
    const rsa = new RSA();
    const ecdh = new ECDH();
    const pKey = aes.deriveKey(aes.decrypt(master, aes.deriveKey(password.trim())));
    let message = {};

    // load rsa private
    res = await fetch(BASE_URI + "/rsa");
    const rsaResponse = await processJsonResponse(res, STATUS_OK);
    if(rsaResponse === undefined){
      store.dispatch(stopTask());
      return false;
    }

    const rsaPrivate = aes.decrypt(rsaResponse.private, pKey);
    const rsaPublic = rsaResponse.public;

    // verify rsa server signature
    if(!rsa.verifyServerAuthority(rsaResponse.private, rsaResponse["server_signature"])) {
      store.dispatch(stopTask());
      store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
      return false;
    }

    // verify salt server signature
    if(!rsa.verifyServerAuthority(salt, saltResponse["server_signature"])) {
      store.dispatch(stopTask());
      store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
      return false;
    }

    // load ratchet state
    res = await fetch(BASE_URI + "/key/" + receiverID + "/send");
    let currentKey = res.status === 416 ? null : (await processJsonResponse(res, STATUS_OK));
    if(currentKey === undefined){
      store.dispatch(stopTask());
      return false;
    }

    if(currentKey !== null) {
      // verify key signature (from employee)
      if(!rsa.verify(currentKey.key, currentKey.signature, rsaPublic)) {
        store.dispatch(stopTask());
        store.dispatch(showError("Die Signatur des Schlüssels ist falsch"));
        return false;
      }

      // verify key signature (from server)
      if(!rsa.verifyServerAuthority(currentKey.key, currentKey["server_signature"])) {
        store.dispatch(stopTask());
        store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
        return false;
      }
      delete currentKey["server_signature"];
    }

    // when new key needed
    if(currentKey === null || currentKey.usages > 10) {

      // load my next private ecdh component
      res = await fetch(BASE_URI + "/ecdh/next");
      const myNextPrivate = await processJsonResponse(res, STATUS_OK);
      if(myNextPrivate === undefined){
        store.dispatch(stopTask());
        return false;
      }
      
      // verify ecdh signature and delete if invalid
      if(!rsa.verify(myNextPrivate.private, myNextPrivate.signature, rsaPublic)) {
        res = await fetch(BASE_URI + "/ecdh/" + myNextPrivate.id, {method: "DELETE"});
        await processPlainResponse(res, STATUS_OK);

        store.dispatch(stopTask());
        store.dispatch(showError("Ein Teil der Daten konnte nicht verifiziert werden"));
        return false;
      }

      // verify ecdh server signature
      if(!rsa.verifyServerAuthority(myNextPrivate.private, myNextPrivate["server_signature"])) {
        store.dispatch(stopTask());
        store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
        return false;
      }

      // load receiver next public ecdh component
      res = await fetch(BASE_URI + "/ecdh/" + receiverID + "/next");
      const receiverNextPublic = await processJsonResponse(res, STATUS_OK);
      if(receiverNextPublic === undefined){
        store.dispatch(stopTask());
        return false;
      }

      // verify ecdh server signature
      if(!rsa.verifyServerAuthority(receiverNextPublic.public, receiverNextPublic["server_signature"])) {
        store.dispatch(stopTask());
        store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
        return false;
      }

      // calculate ecdh secret
      currentKey = {
        key: ecdh.getSecret(
          aes.decrypt(myNextPrivate.private, pKey),
          receiverNextPublic.public
        ),
        usages: 0
      };

      // store used ecdh values
      message.ecdh = {
        sender: myNextPrivate.id,
        receiver: receiverNextPublic.id
      };

    } else {
      currentKey.key = aes.decrypt(currentKey.key, pKey);
    }

    // calculate new key
    currentKey.key = aes.deriveKey(currentKey.key);
    currentKey.usages++;

    // encrypt & sign message
    message.content = aes.encrypt(content, currentKey.key);
    message.signature = rsa.sign(message.content, rsaPrivate);

    currentKey.key = aes.encrypt(currentKey.key, pKey);
    currentKey.signature = rsa.sign(currentKey.key, rsaPrivate);

    // send message
    message.receiver = receiverID;
    res = await fetch(BASE_URI + "/messages", {
      method: "POST", 
      headers: {'Content-Type': 'application/json'}, 
      body: JSON.stringify(message)
    });

    if(!(await processPlainResponse(res, STATUS_CREATED))) {
      store.dispatch(stopTask());
      return false;
    }

    // update key
    res = await fetch(BASE_URI + "/key/" + receiverID + "/send", {
      method: "PUT", 
      headers: {'Content-Type': 'application/json'}, 
      body: JSON.stringify(currentKey)
    });
    
    store.dispatch(stopTask());
    return await processPlainResponse(res, STATUS_OK);
  } catch(e) {
    store.dispatch(stopTask());
    return false;
  }
};

/**
 * Receives the message and decrypts it
 * @param {int} messageID id of the message
 * @param {int} receiverID id of the receiver
 * @param {int} senderID id of the sender
 * @param {string} password password of the receiver
 * @returns true on success, else false
 * @function receiveSecureMessage
 * @see AES
 * @see RSA
 * @see ECDH
 * @see processJsonResponse
 * @see processPlainResponse
 */
export const receiveSecureMessage = async (messageID, receiverID, senderID, password) => {
  store.dispatch(startTask());

  try {
    // fetch salt
    let res = await fetch(ROOT_URI + "/employees/" + senderID + "/salt");
    const saltResponse = await processJsonResponse(res, STATUS_OK);
    if(saltResponse === undefined){
      store.dispatch(stopTask());
      return undefined;
    }

    // fetch salt
    res = await fetch(ROOT_URI + "/employees/" + receiverID + "/salt");
    const saltResponse2 = await processJsonResponse(res, STATUS_OK);
    if(saltResponse2 === undefined){
      store.dispatch(stopTask());
      return undefined;
    }

    const senderSalt = saltResponse.salt;
    const receiverSalt = saltResponse2.salt;

    let master = await loadMasterKey();
    if(master === undefined) {
      store.dispatch(stopTask());
      return false;
    }

    // init algorithms
    const aes = new AES(receiverSalt);
    const aesSender = new AES(senderSalt);
    const rsa = new RSA();
    const ecdh = new ECDH();
    const pKey = aes.deriveKey(aes.decrypt(master, aes.deriveKey(password)));

    // ======= LOAD RSA =======
    // load my rsa
    res = await fetch(BASE_URI + "/rsa");
    let rsaResponse = await processJsonResponse(res, STATUS_OK);
    if(rsaResponse === undefined){
      store.dispatch(stopTask());
      return undefined;
    }

    const myRsaPublic = rsaResponse.public;
    const myRsaPrivate = aes.decrypt(rsaResponse.private, pKey);

    // verify rsa server signature
    if(!rsa.verifyServerAuthority(rsaResponse.private, rsaResponse["server_signature"])) {
      store.dispatch(stopTask());
      store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
      return undefined;
    }

    res = await fetch(BASE_URI + "/rsa/" + senderID);
    rsaResponse = await processJsonResponse(res, STATUS_OK);
    if(rsaResponse === undefined){
      store.dispatch(stopTask());
      return undefined;
    }
    const senderRsaPublic = rsaResponse.public;

    // verify rsa server signature
    if(!rsa.verifyServerAuthority(rsaResponse.public, rsaResponse["server_signature"])) {
      store.dispatch(stopTask());
      store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
      return undefined;
    }

    // verify salt server signature
    if(!rsa.verifyServerAuthority(senderSalt, saltResponse["server_signature"])) {
      store.dispatch(stopTask());
      store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
      return undefined;
    }

    // verify salt server signature
    if(!rsa.verifyServerAuthority(receiverSalt, saltResponse2["server_signature"])) {
      store.dispatch(stopTask());
      store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
      return undefined;
    }

    // ======= LOAD MESSAGE =======
    res = await fetch(BASE_URI + "/messages/" + messageID);
    let message = await processJsonResponse(res, STATUS_OK);

    // verify message sender signature
    if(!rsa.verify(message.content, message.signature, senderRsaPublic)) {
      store.dispatch(stopTask());
      store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
      return undefined;
    }

    // verify message server signature
    if(!rsa.verifyServerAuthority(message.content, message["server_signature"])) {
      store.dispatch(stopTask());
      store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
      return undefined;
    }

    // ======= LOAD CURRENT KEY =======
    let currentKey = null;
    if(message.ecdh === undefined) {
      // load ratchet state
      res = await fetch(BASE_URI + "/key/" + senderID + "/receive");
      currentKey = res.status === 416 ? null : (await processJsonResponse(res, STATUS_OK));
      if(currentKey === undefined){
        store.dispatch(stopTask());
        return undefined;
      }

      if(currentKey !== null) {
        // verify key signature (from employee)
        if(!rsa.verify(currentKey.key, currentKey.signature, myRsaPublic)) {
          store.dispatch(stopTask());
          store.dispatch(showError("Die Signatur des Schlüssels ist falsch"));
          return undefined;
        }

        // verify key signature (from server)
        if(!rsa.verifyServerAuthority(currentKey.key, currentKey["server_signature"])) {
          store.dispatch(stopTask());
          store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
          return undefined;
        }
        delete currentKey["server_signature"];
      }
    }

    // when new key needed
    if((currentKey === null || currentKey.usages > 10) && message.ecdh !== undefined) {

      // ======= LOAD SENDER ECDH =======
      // load my next private ecdh component
      res = await fetch(BASE_URI + "/ecdh/" + message.ecdh.sender);
      const senderKey = await processJsonResponse(res, STATUS_OK);
      if(senderKey === undefined){
        store.dispatch(stopTask());
        return undefined;
      }
      
      // verify ecdh signature and delete if invalid
      if(!rsa.verify(senderKey.private, senderKey.signature, senderRsaPublic)) {
        store.dispatch(stopTask());
        store.dispatch(showError("Ein Teil der Daten konnte nicht verifiziert werden"));
        return undefined;
      }

      // verify ecdh server signature
      if(!rsa.verifyServerAuthority(senderKey.private, senderKey["server_signature"])) {
        store.dispatch(stopTask());
        store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
        return undefined;
      }

      // ======= LOAD RECEIVER ECDH =======
      // load receiver next public ecdh component
      res = await fetch(BASE_URI + "/ecdh/" + message.ecdh.receiver);
      const receiverKey = await processJsonResponse(res, STATUS_OK);
      if(receiverKey === undefined){
        store.dispatch(stopTask());
        return undefined;
      }

      // verify ecdh signature and delete if invalid
      if(!rsa.verify(receiverKey.private, receiverKey.signature, myRsaPublic)) {
        store.dispatch(stopTask());
        store.dispatch(showError("Ein Teil der Daten konnte nicht verifiziert werden"));
        return undefined;
      }

      // verify ecdh server signature
      if(!rsa.verifyServerAuthority(receiverKey.private, receiverKey["server_signature"])) {
        store.dispatch(stopTask());
        store.dispatch(showError("Die Identität des Servers konnte nicht überprüft werden"));
        return undefined;
      }

      // calculate ecdh secret
      currentKey = {
        key: ecdh.getSecret(
          aes.decrypt(receiverKey.private, pKey),
          senderKey.public
        ),
        usages: 0
      };
    } else {
      currentKey.key = aes.decrypt(currentKey.key, pKey);
    }

    // ======= DECRYPT =======
    // calculate new key
    currentKey.key = aesSender.deriveKey(currentKey.key);
    currentKey.usages++;

    // decrypt message
    const content = aesSender.decrypt(message.content, currentKey.key);
    
    // ======= UPDATE ON SERVER =======
    currentKey.key = aes.encrypt(currentKey.key, pKey);
    currentKey.signature = rsa.sign(currentKey.key, myRsaPrivate);

    // delete message
    res = await fetch(BASE_URI + "/messages/" + messageID, {method: "DELETE"});
    await processPlainResponse(res, STATUS_NO_CONTENT);
    if(!await processPlainResponse(res, STATUS_NO_CONTENT)) {
      store.dispatch(stopTask());
      return undefined;
    }

    // update key
    res = await fetch(BASE_URI + "/key/" + senderID + "/receive", {
      method: "PUT", 
      headers: {'Content-Type': 'application/json'}, 
      body: JSON.stringify(currentKey)
    });
    
    store.dispatch(stopTask());
    return await processPlainResponse(res, STATUS_OK) ? content : undefined;
  } catch(e) {
    store.dispatch(stopTask());
    return undefined;
  }
};
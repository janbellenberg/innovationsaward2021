export const STATUS_OK = 200;
export const STATUS_CREATED = 201;
export const STATUS_NO_CONTENT = 204;
export const STATUS_PARTIAL_CONTENT = 206;

export const ROOT_URI = "/api/v1";
export const USER_URI = ROOT_URI + "/user";
export const ADMINISTRATION_URI = ROOT_URI + "/administration";

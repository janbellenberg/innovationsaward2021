import {startTask, stopTask} from '../redux/Dispatcher';
import store from '../redux/Store';

import {processJsonResponse, processPlainResponse} from '.';
import {STATUS_OK, STATUS_CREATED, STATUS_NO_CONTENT, ROOT_URI} from './constants';

/**
 * loads the ui-init data
 * @returns [user data, design, should generate rsa key, should generate master key, missing ecdh pre-keys]
 * @function uiInit
 * @see processJsonResponse
 */
export const uiInit = async () => {

  if(window.location.port === "3000") {   // debugging
    return [{
      id: 1,
      username: "janbellenberg",
      firstname: "Jan",
      lastname: "Bellenberg",
      email: "jan.bellenberg@outlook.de",
      administration: true,
      chat: true,
      status: 1
    }, false, false, 0];
  }

  store.dispatch(startTask());
  let response = await fetch(ROOT_URI + "/ui-init");
  store.dispatch(stopTask());

  if(response.status === 401) {
    return [
      null,
      false,
      false,
      0
    ];
  }

  let data = await processJsonResponse(response, STATUS_OK);

  if(data === undefined) 
    return [{
      id: -1,
      username: "",
      firstname: "",
      lastname: "",
      email: "",
      administration: false,
      chat: false,
      status: 1
    }, false, false, 0];

  let design = data.design;
  let newRsa = data.new_rsa;
  let newMasterKey = data.new_master_key;
  let missingEcdh = data.missing_ecdh;

  delete data.missing_ecdh;
  delete data.new_rsa;
  delete data.new_master_key;

  return [data, design, newRsa, newMasterKey, missingEcdh];
};

/**
 * updates the rsa keypair of the current user
 * @param {object} keypair rsa keypair
 * @function updateRsaKeys
 * @see processPlainResponse
 */
export const updateRsaKeys = async (keypair) => {
  store.dispatch(startTask());
  let response = await fetch(ROOT_URI + "/secure_chat/rsa", {
    method: 'PUT',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(keypair)
  });
  store.dispatch(stopTask());

  processPlainResponse(response, STATUS_NO_CONTENT);
}

/**
 * updates the master key
 * @param {object} keypair new master key
 * @returns if request succeeded
 * @function updateMasterKey
 * @see processPlainResponse
 */
export const updateMasterKey = async (key) => {
  store.dispatch(startTask());
  let response = await fetch(ROOT_URI + "/secure_chat/master", {
    method: 'PATCH',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(key)
  });
  store.dispatch(stopTask());

  return processPlainResponse(response, STATUS_NO_CONTENT);
}

/**
 * adds an new ecdh pre-key
 * @param {object} keypair ecdh keypair
 * @function addEcdhPreKey
 * @see processPlainResponse
 */
export const addEcdhPreKey = async (keypair) => {
  store.dispatch(startTask());
  let response = await fetch(ROOT_URI + "/secure_chat/ecdh", {
    method: 'POST', 
    headers: {'Content-Type': 'application/json'}, 
    body: JSON.stringify(keypair)
  });
  store.dispatch(stopTask());
  processPlainResponse(response, STATUS_CREATED);
};

/**
 * checks the password
 * @param {string} password password
 * @returns if the password is valid
 * @function checkPassword
 * @see processPlainResponse
 */
export const checkPassword = async (password) => {
  store.dispatch(startTask());
  let response = await fetch(ROOT_URI + "/authentication/check", {
    method: 'POST', 
    headers: {'Content-Type': 'application/json'}, 
    body: JSON.stringify({password: password})
  });
  store.dispatch(stopTask());

  if(response.status === 401) {
    return false;
  }

  return processPlainResponse(response, STATUS_NO_CONTENT);
}
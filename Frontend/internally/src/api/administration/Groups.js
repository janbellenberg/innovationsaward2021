import {startTask, stopTask} from '../../redux/Dispatcher';
import store from '../../redux/Store';

import {processJsonResponse, processPlainResponse} from '..';
import {STATUS_OK, STATUS_CREATED, STATUS_NO_CONTENT, ADMINISTRATION_URI} from '../constants';

const BASE_URI = ADMINISTRATION_URI + "/groups";

/**
 * loads all groups
 * @returns list of the existing groups
 * @function loadGroups
 * @see processJsonResponse
 */
export const loadGroups = async () => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI);
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || [];
};

/**
 * creates a new group
 * @param {object} group data of the new group
 * @function addGroup
 * @see processPlainResponse
 */
export const addGroup = async (group) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI, {
    method: "POST", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(group)
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_CREATED);
};

/**
 * updates an existing group
 * @param {int} id id of the group
 * @param {object} group data of the group
 * @function updateGroup
 * @see processPlainResponse
 */
export const updateGroup = async (id, group) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id, {
    method: "PUT", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(group)
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * deletes an existing group
 * @param {int} id id of the group
 * @function deleteGroup
 * @see processPlainResponse
 */
export const deleteGroup = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id, {
    method: "DELETE"
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};
import {startTask, stopTask} from '../../redux/Dispatcher';
import store from '../../redux/Store';

import {processJsonResponse, processPlainResponse} from '..';
import {STATUS_OK, STATUS_CREATED, STATUS_NO_CONTENT, ADMINISTRATION_URI} from '../constants';

const BASE_URI = ADMINISTRATION_URI + "/departments";

/**
 * loads the departments
 * @returns list of departments
 * @function loadDepartments
 * @see processJsonResponse
 */
export const loadDepartments = async () => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI);
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || [];
};

/**
 * creates a new department
 * @param {object} department data of the new department
 * @function addDepartment
 * @see processPlainResponse
 */
export const addDepartment = async (department) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI, {
    method: "POST", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(department)
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_CREATED);
};

/**
 * updates an existing department
 * @param {int} id id of the department
 * @param {object} department data of the department
 * @function updateDepartment
 * @see processPlainResponse
 */
export const updateDepartment = async (id, department) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id, {
    method: "PUT", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(department)
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * deletes an existing department
 * @param {int} id id of the department
 * @function deleteDepartment
 * @see processPlainResponse
 */
export const deleteDepartment = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id, {
    method: "DELETE"
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};
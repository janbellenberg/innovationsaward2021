import {startTask, stopTask} from '../../redux/Dispatcher';
import store from '../../redux/Store';

import {processJsonResponse, processPlainResponse} from '..';
import {STATUS_OK, STATUS_CREATED, STATUS_NO_CONTENT, ADMINISTRATION_URI} from '../constants';

const BASE_URI = ADMINISTRATION_URI + "/locations";

/**
 * loads the existing locations
 * @returns list of the existing locations
 * @function loadLocations
 * @see processJsonResponse
 */
export const loadLocations = async () => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI);
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || [];
};

/**
 * creates a new location
 * @param {object} location data of the new location
 * @function addLocation
 * @see processPlainResponse
 */
export const addLocation = async (location) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI, {
    method: "POST", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(location)
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_CREATED);
};

/**
 * updates an existing location
 * @param {int} id id of the location
 * @param {object} location data of the location
 * @function updateLocation
 * @see processPlainResponse
 */
export const updateLocation = async (id, location) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id, {
    method: "PUT", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(location)
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * deletes an existing location
 * @param {int} id id of the location
 * @function deleteLocation
 * @see processPlainResponse
 */
export const deleteLocation = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id, {
    method: "DELETE"
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};
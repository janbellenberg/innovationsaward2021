import {startTask, stopTask} from '../../redux/Dispatcher';
import store from '../../redux/Store';

import {processJsonResponse, processPlainResponse} from '..';
import {STATUS_OK, STATUS_CREATED, STATUS_NO_CONTENT, ADMINISTRATION_URI} from '../constants';

const BASE_URI = ADMINISTRATION_URI + "/employees";

/**
 * loads the employees
 * @param {object} searchData filter parameters
 * @returns list of the employees matching the search data
 * @function searchEmployees
 * @see processJsonResponse
 */
export const searchEmployees = async (searchData) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "?" + new URLSearchParams(searchData));
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || [];
};

/**
 * adds a new employee
 * @param {object} employee data of the new employee
 * @function addEmployee
 * @see processPlainResponse
 */
export const addEmployee = async (employee) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI, {
    method: "POST", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(employee)
  });
  
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_CREATED);
}

/**
 * updates an existing employee
 * @param {int} id id of the employee
 * @param {object} employee data of the employee
 * @function updateEmployee
 * @see processPlainResponse
 */
export const updateEmployee = async (id, employee) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id, {
    method: "PUT", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(employee)
  });
  
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
}

/**
 * deletes an existing employee
 * @param {int} id id of the employee
 * @function deleteEmployee
 * @see processPlainResponse
 */
export const deleteEmployee = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id, {
    method: "DELETE"
  });
  
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
}

/**
 * sets the password of an employee to the default value
 * @param {int} id id of the employee
 * @function resetPassword
 * @see processPlainResponse
 */
export const resetPassword = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id + "/password/default", {
    method: "PATCH"
  });
  
  store.dispatch(stopTask());
  await processPlainResponse(response, STATUS_NO_CONTENT);
}
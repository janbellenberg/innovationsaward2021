import {startTask, stopTask} from '../../redux/Dispatcher';
import store from '../../redux/Store';

import {processJsonResponse, processPlainResponse} from '..';
import {STATUS_OK, STATUS_NO_CONTENT, ADMINISTRATION_URI} from '../constants';

const SETTINGS_URI = ADMINISTRATION_URI + "/settings";
const INFO_URI = ADMINISTRATION_URI + "/information";

/**
 * loads the current settings
 * @returns current settings
 * @function loadSettings
 * @see processJsonResponse
 */
export const loadSettings = async () => {
  store.dispatch(startTask());
  let response = await fetch(SETTINGS_URI);
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || {};
};

/**
 * updates the settings
 * @param {object} settings new settings
 * @function updateSettings
 * @see processPlainResponse
 */
export const updateSettings = async (settings) => {
  store.dispatch(startTask());
  let response = await fetch(SETTINGS_URI, {
    method: "PUT", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(settings)
  });
  store.dispatch(stopTask());

  return await processPlainResponse(response, STATUS_NO_CONTENT);
}

/**
 * loads the informations about the os of the server
 * @returns os information
 * @function getOsInformation
 * @see processJsonResponse
 */
export const getOsInformations = async () => {
  store.dispatch(startTask());
  let response = await fetch(INFO_URI + "/os");
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || {};
};

/**
 * loads the network configuration of the server
 * @returns network configuration
 * @function getNetworkInformations
 * @see processJsonResponse
 */
export const getNetworkInformations = async () => {
  store.dispatch(startTask());
  let response = await fetch(INFO_URI + "/network");
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || {};
};

/**
 * loads the informations about the server java runtime
 * @returns java information
 * @function getJavaInformations
 * @see processJsonResponse
 */
export const getJavaInformations = async () => {
  store.dispatch(startTask());
  let response = await fetch(INFO_URI + "/java");
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || {};
};
import {startTask, stopTask} from '../../redux/Dispatcher';
import store from '../../redux/Store';

import {processJsonResponse, processPlainResponse} from '..';
import {STATUS_OK, STATUS_NO_CONTENT, ROOT_URI} from '../constants';

const BASE_URI = ROOT_URI + "/profile";

/**
 * loads the own profile
 * @returns instance of the profile
 * @function loadMyProfile
 * @see processJsonResponse
 */
export const loadMyProfile = async () => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI);
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data;
};

/**
 * loads the profile of another employee
 * @param {int} id id of the employee
 * @returns instance of the profile
 * @function loadForeignProfile
 * @see processJsonResponse
 */
export const loadForeignProfile = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id);
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data;
};

/**
 * updates the profile image
 * @param {object} image new image (form data)
 * @function updateImage
 * @see processPlainResponse
 */
export const updateImage = async (image) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/image", {
    method: "PATCH",
    body: image
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * sets the own profile image to the default one
 * @function deleteImage
 * @see processPlainResponse
 */
export const deleteImage = async () => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/image" , {
    method: "DELETE"
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * updates the own status message
 * @param {string} content new content of the status message
 * @function updateMessage
 * @see processPlainResponse
 */
export const updateMessage = async (content) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/message", {
    method: "PATCH", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({message: content})
  });
  store.dispatch(stopTask());

  return await processPlainResponse(response, STATUS_NO_CONTENT);
};
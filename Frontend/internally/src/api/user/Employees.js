import {startTask, stopTask} from '../../redux/Dispatcher';
import store from '../../redux/Store';

import {processJsonResponse, processPlainResponse} from '..';
import {STATUS_OK, STATUS_NO_CONTENT, ROOT_URI} from '../constants';

const BASE_URI = ROOT_URI + "/employees";

/**
 * search employees by query
 * @param {string} query search query
 * @returns list of employees
 * @function searchEmployees
 * @see processJsonResponse
 */
export const searchEmployees = async (query) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "?query=" + query);
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || [];
};

/**
 * updates the ui design
 * @param {bool} design true -> dark, false -> light
 * @function updateDesign
 * @see processPlainResponse
 */
export const updateDesign = async (design) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/design", {
    method: "PATCH", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({design: design})
  });
  
  store.dispatch(stopTask());

  return await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * updates the own statuscode
 * @param {int} code 1 -> Online, 2 -> Abwesend, 3 -> Beschäftigt
 * @function updateStatus
 * @see processPlainResponse
 */
export const updateStatus = async (code) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/status", {
    method: "PATCH", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({status: code})
  });
  
  store.dispatch(stopTask());

  return await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * updates the own password
 * @param {string} password new password
 * @returns object with new salt value
 * @see processPlainResponse
 * @function updatePassword
 */
export const updatePassword = async (password) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/password", {
    method: "PATCH", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({password: password})
  });
  
  store.dispatch(stopTask());

  return await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * sets, if 2fa should be enabled
 * @param {string} enable enabled?
 * @returns success
 * @function updateTwoFA
 * @see processPlainResponse
 */
export const updateTwoFA = async (enable) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/twoFA", {
    method: "PATCH", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({twoFA: enable})
  });
  
  store.dispatch(stopTask());

  return await processPlainResponse(response, STATUS_NO_CONTENT);
}
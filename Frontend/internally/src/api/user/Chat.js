import {startTask, stopTask} from '../../redux/Dispatcher';
import store from '../../redux/Store';

import {processJsonResponse, processPlainResponse} from '..';
import {STATUS_OK, STATUS_CREATED, STATUS_NO_CONTENT, ROOT_URI} from '../constants';

const BASE_URI = ROOT_URI + "/chat";

/**
 * loads the chats where the current employee is a member
 * @returns list of own chats
 * @function loadChats
 * @see processJsonResponse
 */
export const loadChats = async () => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI);
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || [];
};

/**
 * creates a new chat with default title
 * @returns id of the new chat
 * @function addChat
 * @see processJsonResponse
 */
export const addChat = async () => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI, {
    method: "POST"
  });
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_CREATED);
  return data === undefined ? undefined : data.id;
};

/**
 * loads the messages in a chat (max. 20x)
 * @param {int} id id of the chat
 * @param {int} last id of the last message
 * @returns list of messages
 * @function loadMessagesOfChat
 * @see processJsonResponse
 */
export const loadMessagesOfChat = async (id, last) => {
  let query = last === undefined ? "" : "?last=" + last;

  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id + "/messages" + query);
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data === undefined ? [] : data.reverse();
};

/**
 * changes the title of the chat
 * @param {int} id id of the chat
 * @param {string} title new title
 * @function renameChat
 * @see processPlainResponse
 */
export const renameChat = async (id, title) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id, {
    method: "PATCH", 
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({title: title})
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * deletes the chat
 * @param {int} id id of the new chat
 * @function deleteChat
 * @see processPlainResponse
 */
export const deleteChat = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id, {
    method: "DELETE"
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * loads the details of the message
 * @param {int} id id of the message
 * @returns details of the message
 * @function getMessageDetails
 * @see processJsonResponse
 */
export const getMessageDetails = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/message/" + id);
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data;
};

/**
 * loads the highlighted messages
 * @returns list of messages
 * @function getHighlighted
 * @see processJsonResponse
 */
 export const getHighlighted = async () => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/highlights");
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || [];
};

/**
 * marks the message as highlighted
 * @param {int} id id of the message
 * @function highlightMessage
 * @see processPlainResponse
 */
export const highlightMessage = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/message/" + id + "/highlight", {
    method: "POST", 
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * removes the highlight marker
 * @param {int} id id of the message
 * @function unHighlightMessage
 * @see processPlainResponse
 */
export const unHighlightMessage = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/message/" + id + "/highlight", {
    method: "DELETE", 
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * get the pinned message of a chat
 * @param {int} id id of the chat
 * @function getPinned
 * @see processJsonResponse
 */
export const getPinned = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id + "/pin");
  store.dispatch(stopTask());

  if(response.status === 204) {
    return undefined;
  }

  let data = await processJsonResponse(response, STATUS_OK);
  return data;
};

/**
 * pins the message in the chat
 * @param {int} id id of the message
 * @function pinMessage
 * @see processPlainResponse
 */
export const pinMessage = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/message/" + id + "/pin", {
    method: "POST", 
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * unpins the pinned message of the chat
 * @param {int} id id of the chat
 * @function unPinMessage
 * @see processPlainResponse
 */
export const unPinMessage = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id + "/pin", {
    method: "DELETE", 
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};

/**
 * loads the member of the chat
 * @param {int} id id of the chat
 * @returns list of members
 * @function getMembers
 * @see processJsonResponse
 */
export const getMembers = async (id) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + id + "/members");
  store.dispatch(stopTask());

  let data = await processJsonResponse(response, STATUS_OK);
  return data || [];
}

/**
 * adds the employee to the chat
 * @param {int} chat id of the chat
 * @param {string} employee username of the employee
 * @function addMember
 * @see processPlainResponse
 */
export const addMember = async (chat, employee) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + chat + "/employee/" + employee, {
    method: "POST", 
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_CREATED);
};

/**
 * removes the employee of the chat
 * @param {int} chat id of the chat
 * @param {int} employee id of the employee
 * @function deleteMember
 * @see processPlainResponse
 */
export const deleteMember = async (chat, employee) => {
  store.dispatch(startTask());
  let response = await fetch(BASE_URI + "/" + chat + "/employee/" + employee, {
    method: "DELETE", 
  });
  store.dispatch(stopTask());

  await processPlainResponse(response, STATUS_NO_CONTENT);
};
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { updateUserInfo } from '../../redux/Dispatcher';

import { updateStatus } from '../../api/user/Employees';

import './StatusSwitch.css';

/**
 * component for the selection of a status
 * @function StatusSwitch
 * @see updateStatus
 * @see updateUserInfo
 */
const StatusSwitch = () => {
  const dispatch = useDispatch();
  let user = useSelector(s => s.user);

  const apply = (value) => {
    updateStatus(value).then(success => {
      if(success) {
        let u = Object.assign({}, user);  //copy object
        u.status = value;
        dispatch(updateUserInfo(u));
      }
    });
  }

  return (
    <section>
      <div id="status-controller" active={user !== null ? user.status : 0}>
        <div className="status-item" onClick={() => apply(1)}>Online</div>
        <div className="status-item" onClick={() => apply(2)}>Abwesend</div>
        <div className="status-item" onClick={() => apply(3)}>Beschäftigt</div>
      </div>
    </section>
  );
};

export default StatusSwitch;
import React from 'react';
import PropTypes from 'prop-types';
import "./InboxElement.css";

/**
 * component for the list-item of new, encrypted messages (user dashboard)
 * @param {object} props instance, onClick event
 * @function InboxElement
 */
const InboxElement = ({item, onClick}) => {
  return (
    <div className="secure-chat-item" count={item.count} onClick={onClick}>
      {item.sender.firstname} {item.sender.lastname}
    </div>
  );
}

InboxElement.propTypes = {
  item: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired
};

export default InboxElement;
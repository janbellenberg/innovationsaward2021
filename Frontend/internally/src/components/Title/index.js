import React from 'react';
import PropTypes from 'prop-types';
import ParticleBackground from '../ParticleBackground';

import './Title.css';

/**
 * component for the title, part of the grid layout
 * @param {object} props title (can be node)
 * @function Title
 */
const Title = ({title}) => {
  return (
    <section>
      <ParticleBackground/>
      <div id="title">
        <h1>internally</h1>
        <h2>- {title} -</h2>
      </div>
    </section>
  );
};

Title.propTypes = {
  title: PropTypes.node.isRequired
};

export default Title;
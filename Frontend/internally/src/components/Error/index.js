import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { hideError } from '../../redux/Dispatcher';

import "./Error.css";

/**
 * component for error message
 * @function Error
 */
const Error = () => {
  const error = useSelector(state => state.error);
  const dispatch = useDispatch();

  return error === null ? "" : (
    <div id="error-msg" mode={error.warn ? "warn" : "error"}>
      <h2>{error.warn ? "Warnung" : "Fehler"}</h2>
      <span id="details">{error.details}</span>
      
      <div id="symbol-wrapper" onClick={() => {dispatch(hideError())}}>
        <div></div>
        <div></div>
      </div>
    </div>
  );
}

export default Error;
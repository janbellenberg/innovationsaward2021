import React from 'react';
import PropTypes from 'prop-types';
import './Property.css';

/**
 * component for the property with hover animation in the profile
 * @param {object} props title and children
 * @function Property
 */
const Property = ({title, children = ""}) => {
  return (
    <div className="properties" title={title}>
      {children}
      </div>
  );
};

Property.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node
};

export default Property;
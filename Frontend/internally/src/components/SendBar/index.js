import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './SendBar.css';

/**
 * component for the bar in the chat ui for sending messages
 * @param {object} props send and timed send events
 * @function SendBar
 */
const SendBar = ({onSend, onTimedSend}) => {
  const [content, setContent] = useState("");

  return (
    <footer>
      <textarea
        value={content}
        type="text"
        onChange={(e) => setContent(e.target.value)}
        placeholder="Nachricht"
        autoFocus=""
        style={{height: content
          .replace("\r\n", "\n")    // convert CRLF to LF
          .replace("\r", "\n")      // convert CR to LF
          .split("\n")              // split
          .length * 1.15 + 2.3 + 'em'   /* calculate height for the textarea */}} />

      <input
        type="button"
        id="timed-message"
        style={{cursor: content.trim().length > 0 ? "pointer" : "not-allowed"}} 
        onClick={content.trim().length > 0 ? () => {
          onTimedSend(content);
          setContent("");
          } : null} />

      <input
        type="button" 
        value={window.innerWidth < 800 ? "➤" : "Senden"}
        style={{cursor: content.trim().length > 0 ? "pointer" : "not-allowed"}} 
        onClick={content.trim().length > 0 ? () => {
          onSend(content);
          setContent("");
        } : null} />
    </footer>
  );
};

SendBar.propTypes = {
  onSend: PropTypes.func.isRequired,
  onTimedSend: PropTypes.func.isRequired
};

export default SendBar;
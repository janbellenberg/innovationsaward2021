import React, { useEffect } from 'react';
import { Line, defaults } from 'react-chartjs-2';

let labels = [];

/**
 * component for the line chat with cpu and ram usage
 * @param {object} props onStart event, border color 1, border color 2, data 1, data 2
 * @function LineChart
 * @see Line
 * @see https://www.npmjs.com/package/react-chartjs-2
 */
const LineChart = ({onStart, border1, border2, init1, init2}) => {
 
  // set defaults
  defaults.global.tooltips.enabled = false;
  defaults.global.legend.display = true;
  defaults.global.legend.position = 'bottom';

  // add labels
  useEffect(() => {
    labels = [];
    for(var i = 0; i < 60 * 5; i++){
        labels.push("");
    }
  }, []);

  return (
    <div>
      <Line ref={r => onStart(r)} height={250} width={300}
        data={{
          labels: labels,
          datasets: [
            { // initialize datasets
              label: 'CPU',
              data: init1,
              backgroundColor: "rgba(0, 0, 0, 0)",
              borderColor: border1,
              borderWidth: 2
            },
            {
              label: 'RAM',
              data: init2,
              backgroundColor: "rgba(0, 0, 0, 0)",
              borderColor: border2,
              borderWidth: 2
            }
          ]
        }}

        options={options}
      />
    </div>
  );
}

export default LineChart;

let options = {
  elements: {point: {radius: 0}},   // disable dots
  maintainAspectRatio: false,
  scales: {
    xAxes: [                        // disable x-grid
      {
        display: false
      }
    ],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          min: 0,
          max: 100
        },
      },
    ],
  },
  legend: {
    labels: {
      fontSize: 20,
    },
  }
};
import React from 'react';
import PropTypes from 'prop-types';
import "./Related.css";

/**
 * component for links to related pages in the administration
 * @param {object} props children
 * @function Related
 */
const Related = ({children}) => {
  return (
    <aside id="related">
      <h2>Verwandte Links</h2>
      <ul>
          {children}
      </ul>
    </aside>
  );
};

Related.propTypes = {
  children: PropTypes.node.isRequired
};

export default Related;
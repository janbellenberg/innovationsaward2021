import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import NavigationElement from '../NavigationElement';
import StatusSwitch from '../../StatusSwitch';

import "../Navigation.css";

import { ReactComponent as Chat } from "../../../svg/Chat.svg";
import { ReactComponent as Dashboard } from "../../../svg/Dashboard.svg";
import { ReactComponent as Profile } from "../../../svg/Profile.svg";
import { ReactComponent as Logout } from "../../../svg/Logout.svg";
import { ReactComponent as Employees } from "../../../svg/Employees.svg";

/**
 * navigation in the normal, user ui
 * @param {object} props which page is currently active
 * @function Navigation
 * @see NavigationElement
 * @see StatusSwitch
 */
const Navigation = ({active}) => {
  let user = useSelector(state => state.user);

  return (
    <div id="nav-wrapper">
      <nav role="navigation">
        <span id="mobile-title">internally</span>
        <div id="nav-top">
          
          {/* navigation top area */}
          <NavigationElement title="Dashboard" dest="/" active={active === 0}>
            <Dashboard />
          </NavigationElement>
          
          <NavigationElement title="Chat" dest="/chat" active={active === 1}>
            <Chat />
          </NavigationElement>
          
          <NavigationElement title="Profil" dest="/profile" active={active === 2}>
            <Profile />
          </NavigationElement>
          
          <NavigationElement title="Mitarbeiter" dest="/employees" active={active === 4}>
            <Employees />
          </NavigationElement>
          
          <NavigationElement title="Abmelden" dest="/logout">
            <Logout />
          </NavigationElement>
        </div>
        
        { // user info
          user === null ? "" : 
          <>
            <div className="break"></div>
            <strong id="nav-name">{user.firstname} {user.lastname}</strong>
            <div className="break"></div>
            <span id="nav-email">{user.email}</span>
            <div className="break"></div>
          </>
        }

        {/* navigation bottom area */}

        <StatusSwitch />

        { (user !== null && user.administration) ? 
            <Link to="/administration"><input type="button" value="Administration öffnen"/></Link>
           : null
        }
      </nav>
    </div>
  );
};

Navigation.propTypes = {
  active: PropTypes.number.isRequired
};

export default Navigation;
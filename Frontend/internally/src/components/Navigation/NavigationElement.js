import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

/**
 * component for a single navigation item
 * @param {object} props the title, if the icon should be orange, if the site is currently active, link destination, children
 * @function NavigationElement
 */
const NavigationElement = ({title, isOrange = false, active = false, dest, children}) => {
  return (
    <Link to={dest}>
      <div className={active ? "nav-item active" : "nav-item"} onClick={() => {
        if(("#" + dest) === window.location.hash) {
          window.location.reload();
        }
      }}>
      
        <div className={isOrange ? "orange" : "blue"}>
          {children}
        </div>
        <span>{title}</span>
      </div>
    </Link>
  );
}

NavigationElement.propTypes = {
  title: PropTypes.string.isRequired,
  isOrange: PropTypes.bool,
  active: PropTypes.bool,
  dest: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
};

export default NavigationElement;
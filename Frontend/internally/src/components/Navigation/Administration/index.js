import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import NavigationElement from '../NavigationElement';

import "./Navigation.css";

import { ReactComponent as Chat } from "../../../svg/Chat.svg";
import { ReactComponent as Dashboard } from "../../../svg/Dashboard.svg";
import { ReactComponent as Departments } from "../../../svg/Departments.svg";
import { ReactComponent as Groups } from "../../../svg/Groups.svg";
import { ReactComponent as Informations } from "../../../svg/Informations.svg";
import { ReactComponent as Locations } from "../../../svg/Locations.svg";
import { ReactComponent as Logout } from "../../../svg/Logout.svg";
import { ReactComponent as More } from "../../../svg/More.svg";
import { ReactComponent as Employees } from "../../../svg/Employees.svg";

/**
 * navigation in the administration ui
 * @param {object} props which page is currently active
 * @function Navigation
 * @see NavigationElement
 */
const Navigation = ({active}) => {
  const [big, setBig] = useState(false);
  let user = useSelector(state => state.user);

  return (
    <div>
      <nav role="navigation" className={big ? "big" : ""}>
        <span id="mobile-title">internally</span>
        <div id="nav-top">
          {/* navigation top area */}

          <NavigationElement title="Dashboard" dest="/administration" active={active === 0}>
            <Dashboard />
          </NavigationElement>

          <NavigationElement title="Mitarbeiter" dest="/administration/employees" active={active === 1}>
            <Employees />
          </NavigationElement>

          <NavigationElement title="Abteilungen" dest="/administration/departments" active={active === 2}>
            <Departments />
          </NavigationElement>

          <NavigationElement title="Standorte" dest="/administration/locations" active={active === 4}>
            <Locations />
          </NavigationElement>
          
          <div className="nav-item" id="nav-more" onClick={ () => setBig(!big) }>
            <More />
            <span>Mehr</span>
          </div>

          <NavigationElement title="Abmelden" dest="/logout">
            <Logout />
          </NavigationElement>
        </div>

        { // user data
          user === null ? "" : 
          <>
            <div className="break"></div>
            <strong id="nav-name">{user.firstname} {user.lastname}</strong>
            <div className="break"></div>
            <span id="nav-email">{user.email}</span>
            <div className="break"></div>
          </>
        }

        <div id="nav-bottom">
          {/* navigation bottom area */}
          <NavigationElement isOrange={true} title="Chat" dest="/chat">
            <Chat />
          </NavigationElement>

          <NavigationElement isOrange={true} title="Gruppen" dest="/administration/groups" active={active === 8}>
            <Groups />
          </NavigationElement>

          <NavigationElement isOrange={true} title="Informationen" dest="/info">
            <Informations />
          </NavigationElement>
        </div>
      </nav>
    </div>
  );
};

Navigation.propTypes = {
  active: PropTypes.number.isRequired
};

export default Navigation;
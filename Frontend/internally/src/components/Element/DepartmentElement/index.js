import React from 'react';
import PropTypes from 'prop-types';
import Element from '..';

/**
 * item for a department (administration)
 * @param {object} props instance of the department, onEdit event
 * @function DepartmentElement
 * @see Element
 */
const DepartmentElement = ({department, onEdit}) => {
  return (
    <Element title={department.name}>
      <input type="button" value="Abteilung bearbeiten" onClick={() => onEdit(department)}></input>
    </Element>
  );
}

DepartmentElement.propTypes = {
  department: PropTypes.object.isRequired,
  onEdit: PropTypes.func.isRequired
};

export default DepartmentElement;
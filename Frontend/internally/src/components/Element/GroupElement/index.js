import React from 'react';
import PropTypes from 'prop-types';
import Element from '..';

/**
 * item for a group (administration)
 * @param {object} props instance of the group, onEdit event
 * @function GroupElement
 * @see Element
 */
const GroupElement = ({group, onEdit}) => {
  return (
    <Element title={group.name}>
    <div class="details-wrapper">
      <div>
        <span>Administrator:</span>
        <span>{group.admin ? "Ja" : "Nein"}</span>
      </div>
      <div>
        <span>Login in 2 Schritten:</span>
        <span>{group.twoFA ? "Ja" : "Nein"} </span>
      </div>
    </div>

      <input type="button" value="Gruppe bearbeiten" onClick={() => onEdit(group)}></input> 
    </Element>
  );
}

GroupElement.propTypes = {
  group: PropTypes.object.isRequired,
  onEdit: PropTypes.func.isRequired
};

export default GroupElement;
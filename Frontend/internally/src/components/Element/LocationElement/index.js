import React from 'react';
import PropTypes from 'prop-types';
import Element from '..';

/**
 * item for a location (administration)
 * @param {object} props instance of the location, onEdit event
 * @function LocationElement
 * @see Element
 */
const LocationElement = ({location, onEdit}) => {
  return (
    <Element title={location.description}>
      {location.address}<br/>
      {location.postcode} {location.city}<br/>
      {location.country}<br/>

      <input type="button" value="Standort bearbeiten" onClick={() => onEdit(location)}></input>
    </Element>
  );
}

LocationElement.propTypes = {
  location: PropTypes.object.isRequired,
  onEdit: PropTypes.func.isRequired
};

export default LocationElement;
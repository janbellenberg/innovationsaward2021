import React from 'react';
import PropTypes from 'prop-types';
import "./Element.css";

/**
 * wrapper for a element component (administration)
 * @param {object} props title of the element, children
 * @function Element
 */
const Element = ({title, children}) => {
  return (
    <section className="element">
      <h2>{title}</h2>
      {children}
    </section>
  );
}

Element.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
};

export default Element;
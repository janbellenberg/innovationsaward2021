import React from 'react';
import PropTypes from 'prop-types';
import Element from '..';

/**
 * item for a employee (administration)
 * @param {object} props instance of the employee, list of groups, departments and locations, onEdit event
 * @function EmployeeELement
 * @see Element
 */
const EmployeeElement = ({employee, groups, departments, locations, onEdit}) => {
    // get group of employee
    const group = groups.filter(item => {
      return item.id === employee.group;
    });
    
    // get department of employee
    const department = departments.filter(item => {
      return item.id === employee.department;
    });

    // get location of employee
    const location = locations.filter(item => {
      return item.id === employee.location;
    });

    return (
      <Element title={"@ " + employee.username}>
        {employee.lastname}, {employee.firstname}<br/>
        <div class="details-wrapper">
          <div>
            <span>E-Mail:</span>
            <span><a href={"mailto:" + employee.email}>{employee.email}</a></span>
          </div>
          <div>
            <span>Abteilung:</span>
            <span>{department[0].name}</span>
          </div>
          <div>
            <span>Standort:</span>
            <span>{location[0].description}</span>
          </div>
          <div>
            <span>Gruppe:</span>
            <span>{group[0].name}</span>
          </div>
        </div>
        <input type="button" value="Benutzer bearbeiten" onClick={() => onEdit(employee)}></input>
      </Element>
    );
}

EmployeeElement.propTypes = {
  employee: PropTypes.object.isRequired,
  groups: PropTypes.array.isRequired,
  departments: PropTypes.array.isRequired,
  locations: PropTypes.array.isRequired,
  onEdit: PropTypes.func.isRequired
};

export default EmployeeElement;
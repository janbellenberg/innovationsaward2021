import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Dialog from '..';
import {updateMessage} from '../../../api/user/Profile';

const StatusTextDialog = ({current, onUpdate, onHide}) => {
  const [message, setMessage] = useState(current);

  return (
    <Dialog title="Status bearbeiten" onHide={onHide}>
      <input
        className="big"
        type="text"
        id="status-message"
        placeholder="Hey there, I'm using internally"
        value={message}
        onChange={(e) => setMessage(e.target.value)} /><br/>

      <input
        className="big"
        type="button"
        value="Status speichern"
        onClick={() => {
          updateMessage(message.trim()).then(success => {
            if(success) {
              onUpdate(message.trim());
              onHide();
            }
          })
        }} />
    </Dialog>
  );
}

StatusTextDialog.propTypes = {
  current: PropTypes.string.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onHide: PropTypes.func.isRequired
};

export default StatusTextDialog;
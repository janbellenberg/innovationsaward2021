import React, { useState } from "react";
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Dialog from "..";

import AES from '../../../encryption/AES';
import { loadMasterKey, loadSalt } from '../../../api/SecureChat';
import { updateMasterKey } from '../../../api/Initialization';
import { updatePassword } from '../../../api/user/Employees';

/**
 * dialog for changing password
 * @param {object} props onHide event
 * @function PasswordDialog
 * @see Dialog
 * @see AES
 * @see loadMasterKey
 * @see loadSalt
 * @see updateMasterKey
 * @see updatePassword
 */
const PasswordDialog = ({onHide}) => {
  const user = useSelector(s => s.user);
  const [currentPassword, setCurrentPassword] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  const update = async () => {
    try {
      // download current master key
      let master = await loadMasterKey();
      if(master !== undefined) {
        let salt = await loadSalt(user.id);
        let aes = new AES(salt);
        
        // derive keys 
        let keyOld = aes.deriveKey(currentPassword.trim());   // old key
        let keyNew = aes.deriveKey(password1.trim());         // new key
        master = aes.encrypt(aes.decrypt(master, keyOld), keyNew);    // decrypt master-key with old key and re-encrypt it with new key
        await updateMasterKey({master_key: master});
      }
      
      // update password
      let success = await updatePassword(password1.trim());

      if(success) {
        onHide();
      }

    } catch(e) {}
  }

  return (
    <Dialog title="Password bearbeiten" onHide={onHide}>
      <label htmlFor="current_password">Aktuelles Passwort:</label><br/>
      <input
        className="big"
        type="password"
        id="current_password"
        placeholder="•••••••"
        autoFocus
        value={currentPassword}
        onChange={(e) => setCurrentPassword(e.target.value)} /><p/>

      <label htmlFor="password1">Passwort:</label><br/>
      <input
        className="big"
        type="password"
        id="password1"
        placeholder="•••••••"
        value={password1}
        onChange={(e) => setPassword1(e.target.value)} /><p/>

      <label htmlFor="password2" style={{width: "100%"}}>Passwort wiederholen:</label><br/>
      <input
        className="big"
        type="password"
        id="password2"
        placeholder="•••••••"
        value={password2}
        onChange={(e) => setPassword2(e.target.value)}/><p/>

      { password1 === password2 && password1.trim().length > 0 && currentPassword.trim().length > 0
        ? <input type="button" value="Passwort speichern" onClick={update} />
        : <span style={{color: "red"}}>Die Passwörter sind nicht identisch oder die Eingabe ist leer</span>
      }
    </Dialog>
  );
};

PasswordDialog.propTypes = {
  onHide: PropTypes.func.isRequired
};

export default PasswordDialog;
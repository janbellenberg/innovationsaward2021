import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import Dialog from '../../../components/Dialog';
import { algorithms, sendSecureMessage } from '../../../api/SecureChat';

/**
 * dialog for seding and encrypting secure messages
 * @param {object} props id of the employee, security number, hide event
 * @function SecureMessageSendDialog
 * @see Dialog
 * @see sendSecureMessage
 */
const SecureMessageSendDialog = ({employee, security_number, onHide}) => {
  const [message, setMessage] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const user = useSelector(state => state.user);

  // encrypt and send the message
  const send = async () => {
    sendSecureMessage(message, employee, user.id, password).then(success => {
      setError(!success);
      setMessage("");
    });
  };

  return (
    <Dialog title="Sichere Nachricht senden" format="big" onHide={onHide}>
      <input
        type="text"
        id="secure-message-content"
        placeholder="Nachricht"
        value={message}
        onChange={(e) => setMessage(e.target.value)} />
        
      <input
        type="password"
        placeholder="Passwort"
        value={password}
        onChange={(e) => setPassword(e.target.value)} />
      <p/>

      <div id="security-number">
        Sicherheitsnummer (bitte mit Empfänger abgleichen): 
        <code>
          {security_number.slice(0, security_number.length / 2)}
          <br/>
          {security_number.slice(security_number.length / 2, security_number.length)}
        </code>
      </div>

      <details id="encryption-information">
        <summary>Sicherheitsinformationen</summary>
        <div>
          Verschlüsselung: 
          <strong>{algorithms.encryption.name}</strong>
          {algorithms.encryption.details}<br/>

          Verifizierung:
          <strong>{algorithms.verification.name}</strong>
          {algorithms.verification.details}<br/>

          Schlüsselaustausch:
          <strong>{algorithms.keyExchange.name}</strong>
          {algorithms.keyExchange.details}<br/>

          Schlüsselableitung:
          <strong>{algorithms.keyDerivation.name}</strong>
          {algorithms.keyDerivation.details}<br/>

        </div>
      </details>
      
      <div id="encryption-warning">
        Nach dem Übertragen einer Nachricht kann diese nur EINMAL entschlüsselt und gelesen
        werden!
      </div><p/>

      <div id="encryption-warning" style={{color: "red", display: error ? "block" : "none"}}>
        Bei der Übertragung ist ein Fehler aufgetreten!
      </div><p/>

      <input type="button" id="submit-encrypted-btn" value="Nachricht senden" onClick={send}/>
    </Dialog>
  );
}

export default SecureMessageSendDialog;
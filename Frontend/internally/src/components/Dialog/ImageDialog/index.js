import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '..';

import { updateImage, deleteImage } from '../../../api/user/Profile';

/**
 * dialog for editing profile image
 * @param {object} props onUpdate event, onHide even
 * @function ImageDialog
 * @see Dialog
 * @see Profile
 * @see updateImage
 * @see deleteImage
 */
const ImageDialog = ({onUpdate, onHide}) => {

  return (
    <Dialog title="Bild bearbeiten" onHide={onHide}>
      <input
        type="file"
        id="img-input"
        accept="image/png, image/jpeg"
        onChange={(e) => updateImage(e.target.files[0]).then(onUpdate).then(onHide)} />

      <p/>

      <input
        type="button"
        value="Bild löschen"
        onClick={() => {
          deleteImage().then(onUpdate).then(onHide);
        }} />
    </Dialog>
  );
};

ImageDialog.propTypes = {
  onUpdate: PropTypes.func.isRequired,
  onHide: PropTypes.func.isRequired
};

export default ImageDialog;
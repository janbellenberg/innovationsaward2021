import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Dialog from '..';
import Message from '../../Message';
import { useSelector } from 'react-redux';
import { getPinned, unPinMessage } from '../../../api/user/Chat';

/**
 * dialog for the pinned message in a chat
 * @param {object} param0 id of the chat, hide event
 * @function PinnedMessageDialog
 * @see Dialog
 * @see Message
 */
const PinnedMessageDialog = ({chatID, onHide}) => {
  let user = useSelector(state => state.user);
  const [message, setMessage] = useState(undefined);

  const unpin = () => {
    unPinMessage(chatID);
    setMessage(undefined);
  };

  useEffect(() => {
    getPinned(chatID).then(result => setMessage(result));
  }, [chatID]);

  return (
    <Dialog title="Angepinnte Nachricht" onHide={onHide}>
      {message === undefined ? "Keine Nachricht angepinnt" : 
        <>
          <Message 
            content={message.content} 
            sender={message.sender.firstname + " " + message.sender.lastname}
            star={false}
            me={message.sender.id === user.id}
            onClick={() => {}}
            />
          <input type="button" value="Nicht mehr anpinnen" onClick={unpin} />
        </>
      }
    </Dialog>
  )
}

PinnedMessageDialog.propTypes = {
  chatID: PropTypes.number.isRequired,
  onHide: PropTypes.func.isRequired
};


export default PinnedMessageDialog;
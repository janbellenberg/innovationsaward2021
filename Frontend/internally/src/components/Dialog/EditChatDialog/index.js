import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import Dialog from '..';
import MemberList from '../../MemberList';
import { getMembers, addMember, deleteChat, renameChat } from '../../../api/user/Chat';

/**
 * dialog for editing a chat
 * @param {object} props instance of selected chat, rename, delete and hide event
 * @function EditChatDialog
 * @see Dialog
 * @see MemberList
 */
const EditChatDialog = ({chat, onRename, onDelete, onHide}) => {
  const [title, setTitle] = useState(chat.name);
  const [username, setUsername] = useState("");
  const [members, setMembers] = useState([]);

  const loadMembers = useCallback(async () => {
    getMembers(chat.id).then(result => setMembers(result));
  }, [chat]);

  const renameChatEvent = () => {
    renameChat(chat.id, title.trim()).then(() => onRename(title.trim()));
  };

  const deleteChatEvent = () => {
    deleteChat(chat.id).then(onDelete);
  };

  const addMemberEvent = () => {
    addMember(chat.id, username).then(loadMembers).then(() => setUsername(""));
  };

  useEffect(() => {
    loadMembers();
  }, [chat, loadMembers]);

  return (
    <Dialog title="Chat bearbeiten" format="big" onHide={onHide}>
      <label htmlFor="chat-name">Name des Chats</label>
      <input
        id="chat-name"
        type="text"
        placeholder="Titel"
        style={{width: "50%"}}
        autoFocus
        value={title}
        onChange={(e) => setTitle(e.target.value)} /><p/>

      <input
        type="button"
        value="Chat speichern"
        onClick={renameChatEvent} />
      
      <input
          type="button"
          value="Chat löschen"
          className="red-button"
          onClick={deleteChatEvent} /><p/>

      <fieldset>
        <legend>Mitglieder</legend><br/>

        <label htmlFor="add-user-input">Mitarbeiter hinzufügen</label>
        <input
          id="add-user-input"
          type="text"
          placeholder="Benutzername"
          style={{width: "50%", marginRight: "2em"}}
          value={username}
          onChange={(e) => setUsername(e.target.value)} />

        <input
          type="button"
          value="Hinzufügen"
          style={{display: "inline"}}
          onClick={addMemberEvent} />
        <p/>

        <MemberList chat={chat.id} members={members} onRefresh={loadMembers} />
      </fieldset><p/>
    </Dialog>
  )
}

EditChatDialog.propTypes = {
  chat: PropTypes.object.isRequired,
  onRename: PropTypes.func.isRequired, 
  onDelete: PropTypes.func.isRequired,
  onHide: PropTypes.func.isRequired
};

export default EditChatDialog;
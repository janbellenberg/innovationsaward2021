import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Dialog from '..';

/**
 * dialog for add / edit locations
 * @param {object} props selected location, if the location should be added, êvents
 * @function LocationDialog
 * @see Dialog
 */
const LocationDialog = ({location, put, onHide, onAdd, onUpdate, onDelete}) => {
  const [description, setDescription] = useState(put ? "" : location.description);
  const [address, setAddress] = useState(put ? "" : location.address);
  const [postcode, setPostcode] = useState(put ? "" : location.postcode);
  const [city, setCity] = useState(put ? "" : location.city);
  const [country, setCountry] = useState(put ? "" : location.country);
  
  const buildObject = () =>  {
    return {
      description: description,
      address: address,
      postcode: postcode,
      city: city,
      country: country
    }
  }

  return (
    <Dialog title="Standort verwalten" format="big" onHide={onHide}>

      <label htmlFor="description">Beschreibung: </label>
      <input value={description} onChange={(e) => setDescription(e.target.value)} id="description" type="text" />

      <label htmlFor="street">Straße: </label>
      <input value={address} onChange={(e) => setAddress(e.target.value)} id="street" type="text" placeholder="Straße + Hausnummer" />
      <p/>

      <label htmlFor="postcode">PLZ: </label>
      <input value={postcode} onChange={(e) => setPostcode(e.target.value)} id="postcode" type="text" />

      <label htmlFor="city">Ort: </label>
      <input value={city} onChange={(e) => setCity(e.target.value)} id="city" type="text" />
      <p/>

      <label htmlFor="country">Land: </label>
      <input value={country} onChange={(e) => setCountry(e.target.value)} id="country" type="text" placeholder="2-stelliger Ländercode" />
      <p/>

      {put ? 
        <input type="button" value="Standort hinzufügen" onClick={() => onAdd(buildObject())} /> :
        <>
          <input type="button" value="Standort speichern" onClick={() => onUpdate(location.id, buildObject())} />
          <input type="button" value="Standort löschen" onClick={() => onDelete(location.id)} className="red-button" />
        </>
      }
      
    </Dialog>
  );
}

LocationDialog.propTypes = {
  location: PropTypes.object,
  put: PropTypes.bool,
  onHide: PropTypes.func.isRequired,
  onAdd: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default LocationDialog;
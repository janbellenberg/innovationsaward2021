import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Dialog from '..';
import ToggleButton from '../../ToggleButton';

/**
 * dialog for add / edit group
 * @param {object} props selected group, if the group should be added, events
 * @function GroupDialog
 * @see Dialog
 */
const GroupDialog = ({group, put, onHide, onAdd, onUpdate, onDelete}) => {
  const [name, setName] = useState(put ? "" : group.name);
  const [admin, setAdmin] = useState(put ? false : group.admin);
  const [twoFA, setTwoFA] = useState(put ? false : group.twoFA);
  
  const buildObject = () => {
    return {
      name: name,
      admin: admin,
      twoFA: twoFA
    }
  }

  return (
    <Dialog title="Gruppe verwalten" onHide={onHide}>

      <label htmlFor="name">Bezeichnung: </label>
      <input value={name} onChange={(e) => setName(e.target.value)} id="name" type="text" style={{width: "70%"}} />
      <p/>

      <div style={{display: "flex", justifyContent: "space-around"}}>
        <ToggleButton
          text="Administration"
          value={admin}
          onChange={(value) => setAdmin(value)}/>

        <ToggleButton
          text="Login in 2 Schritten"
          value={twoFA}
          onChange={(value) => setTwoFA(value)}/>
      </div><p/>

      {put ? 
        <input type="button" value="Gruppe hinzufügen" onClick={() => onAdd(buildObject())} /> : 
        <>
          <input
            type="button"
            value="Gruppe speichern"
            onClick={() => onUpdate(group.id, buildObject())} />

          <input
            className="red-button"
            type="button"
            value="Gruppe löschen"
            onClick={() => onDelete(group.id)} />
        </>
      }

    </Dialog>
  );
}

GroupDialog.propTypes = {
  group: PropTypes.object,
  put: PropTypes.bool,
  onHide: PropTypes.func.isRequired,
  onAdd: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default GroupDialog;
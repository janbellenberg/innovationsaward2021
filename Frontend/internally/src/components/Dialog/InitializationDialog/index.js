import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { showError, startTask, stopTask } from '../../../redux/Dispatcher';
import { updateRsaKeys, addEcdhPreKey, updateMasterKey, checkPassword } from '../../../api/Initialization';
import { loadSalt, loadRsaPrivate, loadMasterKey } from '../../../api/SecureChat';

import AES from '../../../encryption/AES';
import RSA from '../../../encryption/RSA';
import ECDH from '../../../encryption/ECDH';

import Dialog from '..';

/**
 * dialog for the initialization of the secure chat
 * @param {object} props new rsa-key required, new master-key required, count of ecdh pre-keys to create
 * @function InitializationDialog
 * @see App
 * @see Dialog
 * @see AES
 * @see RSA
 * @see ECDH
 * @see updateRsaKeys
 * @see addEcdhPreKey
 * @see updateMasterKey
 * @see checkPassword
 * @see loadSalt
 * @see loadRsaPrivate
 * @see loadMasterKey
 */
const InitializationDialog = ({newRsa, newMasterKey, missingEcdh}) => {
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();
  const user = useSelector(state => state.user);

  const doInitialization = async () => {
    let done = 0;
    const increment = () => done++;
    dispatch(startTask());

    // check if password is valid
    if(!await checkPassword(password)) {
      dispatch(stopTask());
      dispatch(showError("Ihr Passwort ist falsch"));
      return;
    }

    // load salt
    let salt = await loadSalt(user.id);
    if(salt === undefined) {
      dispatch(stopTask());
      return;
    }

    const aes = new AES(salt);

    // generate master key
    let rawKey = password;
    if(newMasterKey) {
      rawKey = require('crypto').randomBytes(64).toString('hex');
      let result = await updateMasterKey({master_key: aes.encrypt(rawKey, aes.deriveKey(password))});
      if(!result) {
        return;
      }
    } else {
      rawKey = await loadMasterKey();
      if(rawKey === undefined) {
        dispatch(stopTask());
        return;
      }
    }

    // derive key, based on master key
    const key = aes.deriveKey(rawKey);

    // generate rsa-keys
    if(newRsa) {
      let rsa = new RSA();
      let keys = rsa.createKeyPair();
      keys.private = aes.encrypt(keys.private, key);
      await updateRsaKeys(keys);
    }

    // generate missing ecdh pre-keys
    if(missingEcdh > 0) {
      
      // download rsa private key for signature
      let privKey = await loadRsaPrivate();
      if(privKey === undefined) {
        dispatch(stopTask());
        return;
      }

      privKey = aes.decrypt(privKey, key);

      // generate and add missing ecdh pre-keys
      for(let i = 0; i < missingEcdh; i++) {
        let ecdh = new ECDH();
        let rsa = new RSA();
        let keypair = ecdh.generatePair();
        keypair.private = aes.encrypt(keypair.private, key);
        keypair.signature = rsa.sign(keypair.private, privKey);

        addEcdhPreKey(keypair).then(increment);
      }
    }


    let interval = setInterval(() => {
      if(done >= missingEcdh) {
        clearInterval(interval);
        window.location.reload();
      }
    }, 100);
  };

  return (
    <Dialog title="Einrichtung: sicherer Chat">
      <b>Damit wir Ihre Nachrichten auch in Zukunft sicher übertragen können, müssen wir von Zeit zu Zeit ein paar Dinge einrichten.</b><p/>
      
      <input 
        id="password"
        type="password"
        placeholder="Bitte geben Sie Ihr Passwort ein"
        value={password}
        onChange={e => setPassword(e.target.value)} />

      <input
        type="button"
        value="Einrichtung abschließen" 
        onClick={doInitialization} />
    </Dialog>
  );
};

InitializationDialog.propTypes = {
  newRsa: PropTypes.bool.isRequired,
  missingEcdh: PropTypes.number.isRequired
};

export default InitializationDialog;
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Dialog from '..';

/**
 * dialog searching employees in the administration
 * @param {object} props list of existing departments, locations & groups + events
 * @function SearchEmployeeDialog
 * @see Dialog
 */
const SearchEmployeeDialog = ({departments, locations, groups, onSearch, onHide}) => {
  const [username, setUsername] = useState("%");
  const [firstname, setFirstname] = useState("%");
  const [lastname, setLastname] = useState("%");
  const [email, setEmail] = useState("%");
  const [department, setDepartment] = useState(0);
  const [location, setLocation] = useState(0);
  const [group, setGroup] = useState(0);

  const searchEmployee = () => {
    onSearch({
      username: username,
      firstname: firstname,
      lastname: lastname,
      email: email,
      department: department,
      location: location,
      group: group
    });
  };

  return (
    <Dialog title="Mitarbeiter suchen" format="big" onHide={onHide}>

      <label htmlFor="name">Benutzername: </label>
      <input value={username} onChange={(e) => setUsername(e.target.value)} id="name" type="text"/>
      <p/>

      <label htmlFor="firstname">Vorname: </label>
      <input value={firstname} onChange={(e) => setFirstname(e.target.value)} id="firstname" type="text" />

      <label htmlFor="lastname">Nachname: </label>
      <input value={lastname} onChange={(e) => setLastname(e.target.value)} id="lastname" type="text" />
      <p/>

      <label htmlFor="email">E-Mail: </label>
      <input value={email} onChange={(e) => setEmail(e.target.value)} id="email" type="email" />
      <p/>

      <label htmlFor="department">Abteilung: </label>
      <select 
        id="department"
        value={department}
        onChange={(e) => setDepartment(parseInt(e.target.value))}>

        <option value="0">Alle</option>
        {departments.map((item) => 
          <option
            value={item.id}
            key={item.id}>
              {item.name}
          </option>)}
          
      </select>

      <label htmlFor="location">Standort: </label>
      <select id="location" value={location} onChange={(e) => setLocation(parseInt(e.target.value))}>
        
        <option value="0">Alle</option>
        {locations.map((item) => 
          <option
            value={item.id}
            key={item.id}>
              {item.description}
          </option>)}
          
      </select>
      <p/>

      <label htmlFor="group">Gruppe: </label>
      <select id="group" value={group} onChange={(e) => setGroup(parseInt(e.target.value))}>
        
        <option value="0">Alle</option>
        {groups.map((item) => 
          <option
            value={item.id}
            key={item.id}>
              {item.name}
          </option>)}
          
      </select>
      <p/>

      <div>
        <input type="button" value="Mitarbeiter suchen" onClick={searchEmployee} />
      </div>
    </Dialog>
  );
}

SearchEmployeeDialog.propTypes = {
  departments: PropTypes.array.isRequired,
  locations: PropTypes.array.isRequired,
  groups: PropTypes.array.isRequired,
  onSearch: PropTypes.func.isRequired,
  onHide: PropTypes.func.isRequired
};

export default SearchEmployeeDialog;
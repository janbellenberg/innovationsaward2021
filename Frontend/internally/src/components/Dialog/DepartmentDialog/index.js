import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Dialog from '..';

/**
 * dialog for add / edit department
 * @param {object} props selected department, if the department should be added, events
 * @function DepartmentDialog
 * @see Dialog
 */
const DepartmentDialog = ({department, put = true, onHide, onAdd, onUpdate, onDelete}) => {
  const [id] = useState(put ? 0 : department.id);
  const [name, setName] = useState(put ? "" : department.name);

  return (
    <Dialog title="Abteilung verwalten" onHide={onHide}>

      <label htmlFor="name">Name: </label>
      <input value={name} onChange={(e) => setName(e.target.value)} type="text" />
      <p/>

      {put ? 
        <input type="button" value="Abteilung hinzufügen" onClick={() => onAdd(name)} /> :
        <>
          <input type="button" value="Abteilung speichern" onClick={() => onUpdate(id, name)} />
          <input type="button" value="Abteilung löschen" onClick={() => onDelete(id)} className="red-button" />
        </>
      }

    </Dialog>
  );
}

DepartmentDialog.propTypes = {
  department: PropTypes.object,
  put: PropTypes.bool,
  onHide: PropTypes.func.isRequired,
  onAdd: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default DepartmentDialog;
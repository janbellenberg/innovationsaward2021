import React from 'react';
import PropTypes from 'prop-types';
import "./Dialog.css";

/**
 * wrapper-class for all dialogs
 * @param {object} props title, format, onHide event, children
 * @function Dialog
 */
const Dialog = ({title, children, onHide, format}) => {
  return (
    <div className="dialog-wrapper">
      <dialog open current="" className={format}>
        { onHide === undefined ? null :
          <span className="close-dialog" onClick={onHide}>X</span>
        }
        <h2>{title}</h2>
        {children}
      </dialog>
    </div>
  );
}

Dialog.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  onHide: PropTypes.func,
  format: PropTypes.string
}

export default Dialog;
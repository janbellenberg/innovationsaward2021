import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import Dialog from '..';
import ReceiverList from '../../ReceiverList';
import "./MessageDetailsDialog.css";
import { getMessageDetails, highlightMessage, pinMessage } from '../../../api/user/Chat';

/**
 * dialog for the details of a message
 * @param {object} props id of the message, hide, highlight, update and delete event
 * @function MessageDetailsDialog
 * @see Dialog
 * @see ReceiverList
 */
const MessageDetailsDialog = ({messageID, onHide, onHighlight, onUpdateMessage, onDeleteMessage}) => {

  const [details, setDetails] = useState({});
  const [content, setContent] = useState("");

  const pin = () => {
    pinMessage(messageID);
  };

  const save = () => {
    highlightMessage(messageID);
  };

  useEffect(() => {
    getMessageDetails(messageID).then(result => {
      setDetails(result);
      setContent(result.content);
    });
  }, [messageID]);

  return (
    <Dialog title="Mehr zu dieser Nachricht" format="big" onHide={onHide}>
      {!details.my ? null :
        <fieldset>
          <legend>Nachricht bearbeiten</legend>

          <label htmlFor="content">Inhalt: </label>
          <textarea
            style={{width: "20em", display: "inline", verticalAlign: "middle", height: "7em"}}
            id="content"
            placeholder="Nachricht"
            value={content}
            onChange={(e) => setContent(e.target.value)} />

          <input
            type="button"
            value="Nachricht aktualisieren"
            onClick={() => onUpdateMessage(content)}
            style={{display: "inline"}} />

        </fieldset>
      }<p/>

      <div id="dialog-content-wrapper">
        <div>
          Diese Nachricht wurde {details.edited ? "" : "nicht"} bearbeitet<p/>
          Gesendet {details.timestamp}<p/>
        </div>
        <p/>

        <div id="button-container">
          <input
            type="button"
            value="Nachricht anpinnen"
            onClick={pin} />
          <p/>

          <input
            type="button"
            value="Nachricht speichern"
            onClick={() => {save(); onHighlight();}} />
          <p/>

          { !details.my ? null :
            <input
              type="button"
              id="delete"
              className="red-button"
              value="Nachricht zurückrufen"
              onClick={() => {
                onDeleteMessage();
                onHide();
              }} />
          }
        </div>
      </div>

      {details.my ? <ReceiverList receivers={details.received} /> : null}
    </Dialog>
  );
}

MessageDetailsDialog.propTypes = {
  messageID: PropTypes.number.isRequired,
  onHide: PropTypes.func.isRequired,
  onHighlight: PropTypes.func.isRequired,
  onUpdateMessage: PropTypes.func.isRequired,
  onDeleteMessage: PropTypes.func.isRequired
};

export default MessageDetailsDialog;
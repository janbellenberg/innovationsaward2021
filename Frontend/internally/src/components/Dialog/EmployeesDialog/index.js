import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Dialog from '..';

/**
 * dialog for add / edit employees
 * @param {object} props selected employee, list of departments, locations, groups, if the employee should be added, events
 * @function EmployeeDialog
 * @see Dialog
 */
const EmployeeDialog = ({employee, departments, locations, groups, put, onHide, onAdd, onUpdate, onDelete, onResetPassword}) => {
  const [username, setUsername] = useState(put ? "" : employee.username);
  const [firstname, setFirstname] = useState(put ? "" : employee.firstname);
  const [lastname, setLastname] = useState(put ? "" : employee.lastname);
  const [email, setEmail] = useState(put ? "" : employee.email);
  const [department, setDepartment] = useState(put ? 1 : employee.department);
  const [location, setLocation] = useState(put ? 1 : employee.location);
  const [group, setGroup] = useState(put ? 1 : employee.group);

  const buildObject = () => {
    return {
      username: username,
      firstname: firstname,
      lastname: lastname,
      email: email,
      department: department,
      location: location,
      group: group
    }
  }

  return (
    <Dialog title="Mitarbeiter verwalten" format="big" onHide={onHide}>

      <label htmlFor="name">Benutzer: </label>
      <input value={username} onChange={(e) => setUsername(e.target.value)} id="name" type="text" />
      <p/>

      <label htmlFor="firstname">Vorname: </label>
      <input value={firstname} onChange={(e) => setFirstname(e.target.value)} id="firstname" type="text" />

      <label htmlFor="lastname">Nachname: </label>
      <input value={lastname} onChange={(e) => setLastname(e.target.value)} id="lastname" type="text" />
      <p/>

      <label htmlFor="email">E-Mail: </label>
      <input value={email} onChange={(e) => setEmail(e.target.value)} id="email" type="email" />
      <p/>

      <label htmlFor="department">Abteilung: </label>
      <select id="department" value={department} onChange={(e) => setDepartment(parseInt(e.target.value))}>
        {departments.map((item) => 
          <option 
            key={item.id} 
            value={item.id}>
              {item.name}
          </option>
        )}
      </select>

      <label htmlFor="location">Standort: </label>
      <select id="location" value={location} onChange={(e) => setLocation(parseInt(e.target.value))}>
        {locations.map((item) => 
          <option 
            key={item.id} 
            value={item.id}>
              {item.description}
          </option>
        )}
      </select>
      <p/>

      <label htmlFor="group">Gruppe: </label>
      <select id="group" value={group} onChange={(e) => setGroup(parseInt(e.target.value))}>
        {groups.map((item) => 
          <option 
            key={item.id} 
            value={item.id}>
              {item.name}
          </option>
        )}
      </select>
      <p/>

      {put ? 
        <input type="button" value="Mitarbeiter hinzufügen" onClick={() => onAdd(buildObject())} /> : 
        <>
          <input type="button" value="Mitarbeiter speichern" onClick={() => onUpdate(employee.id, buildObject())} /> 
          <input type="button" value="Passwort zurücksetzen" onClick={() => onResetPassword(employee.id)} /> 
          <input type="button" value="Mitarbeiter löschen" onClick={() => onDelete(employee.id)} className="red-button" />
        </>}

    </Dialog>
  );
}

EmployeeDialog.propTypes = {
  employee: PropTypes.object,
  departments: PropTypes.array.isRequired,
  locations: PropTypes.array.isRequired,
  groups: PropTypes.array.isRequired,
  put: PropTypes.bool,
  onHide: PropTypes.func.isRequired,
  onAdd: PropTypes.func.isRequired,
  onUpdate: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onResetPassword: PropTypes.func.isRequired
};

export default EmployeeDialog;
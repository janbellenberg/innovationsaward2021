import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import Dialog from "..";
import Message from "../../Message";
import { algorithms, receiveSecureMessage, loadSecurityNumber } from '../../../api/SecureChat';

/**
 * dialog for receiving and decrypting secure messages
 * @param {object} props message, hide event
 * @function SecureMessageReceiveDialog
 * @see Dialog
 * @see receiveSecureMessage
 */
const SecureMessageReceiveDialog = ({message, onHide}) => {
  const [content, setContent] = useState(undefined);
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const [security_number, setSecurityNumber] = useState("");

  const user = useSelector(s => s.user);

  // load the message and decrypt it
  const loadMessage = async () => {
    receiveSecureMessage(message.next, user.id, message.sender.id, password).then(result => {
      setError(result === undefined);
      setContent(result);
    });
  };

  // load security number
  useEffect(() => {
    loadSecurityNumber(user.id, message.sender.id)
      .then(result => setSecurityNumber(result));
  }, [message, user]);

  return (
    <Dialog title="Sichere Nachricht empfangen" format="big" onHide={onHide}>
      
      {content === undefined ? 
        <>
          <input
            type="password"
            placeholder="Passwort"
            onChange={(e) => setPassword(e.target.value)}
            value={password} />

          <input
            type="button"
            value="Nachricht entschlüsseln"
            onClick={loadMessage} />
        </> : "" }
      
      {content === undefined ? "" : 
        <>
          <Message 
            content={content} 
            sender={message.sender.firstname + " " + message.sender.lastname}
            star={false}
            me={false}
            onClick={() => {}} />
          <br/> 
        </>
      }

      <div id="security-number">
        Sicherheitsnummer (bitte mit Empfänger abgleichen): 
        <code>
          {security_number.slice(0, security_number.length / 2)}
          <br/>
          {security_number.slice(security_number.length / 2, security_number.length)}
        </code>
      </div>
    
      <details id="encryption-information">
        <summary>Sicherheitsinformationen</summary>
        <div>
          Verschlüsselung: 
          <strong>{algorithms.encryption.name}</strong>
          {algorithms.encryption.details}<br/>

          Verifizierung:
          <strong>{algorithms.verification.name}</strong>
          {algorithms.verification.details}<br/>

          Schlüsselaustausch:
          <strong>{algorithms.keyExchange.name}</strong>
          {algorithms.keyExchange.details}<br/>

          Schlüsselableitung:
          <strong>{algorithms.keyDerivation.name}</strong>
          {algorithms.keyDerivation.details}<br/>

        </div>
      </details>
      
      <div id="encryption-warning">
        Nach dem Übertragen einer Nachricht kann diese nur EINMAL entschlüsselt und gelesen
        werden!
      </div><p/>

      <div id="encryption-warning" style={{color: "red", display: error ? "block" : "none"}}>
        Bei der Übertragung ist ein Fehler aufgetreten!
      </div><p/>
  </Dialog>
  );
}

export default SecureMessageReceiveDialog;
import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import Dialog from "..";
import Message from "../../Message";
import { getHighlighted, unHighlightMessage } from '../../../api/user/Chat';

/**
 * dialog for saved messages
 * @param {object} props onHide event
 * @function SavedMessagesDialog
 * @see Dialog
 * @see Message
 */
const SavedMessagesDialog = ({onHide}) => {
  const user = useSelector(state => state.user);
  const [messages, setMessages] = useState([]);

  const loadMessages = () => {
    getHighlighted().then(result => setMessages(result));
  };

  const unsaveMessage = (id) => {
    unHighlightMessage(id).then(loadMessages);
  };

  useEffect(() => {
    loadMessages();
  }, []);

  return (
    <Dialog title="Gespeicherte Nachrichten" format="big" onHide={onHide}>
      { messages.map((message) => 
        <Message 
          content={message.content} 
          sender={message.sender.firstname + " " + message.sender.lastname}
          star={false}
          me={user.id === message.sender.id}
          onClick={() => {}}>

          <input
            type="button"
            value="Nicht mehr speichern"
            style={{display: "inline-block"}}
            onClick={() => unsaveMessage(message.id)} />
        </Message>
      )}
  </Dialog>
  );
}

SavedMessagesDialog.propTypes = {
  onHide: PropTypes.func.isRequired
};

export default SavedMessagesDialog;
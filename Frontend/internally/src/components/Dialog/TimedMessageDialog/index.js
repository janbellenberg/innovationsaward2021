import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Dialog from '..';
import Message from '../../Message';

/**
 * dialog for sending message with a specific release-time
 * @param {object} props message content, send + hide events
 * @function TimedMessageDialog
 * @see Dialog
 */
const TimedMessageDialog = ({message, onSend, onHide}) => {

  const getFormatted = (d) => {
    let dateTime = new Date(d);   // copy object without reference
    const minutes = dateTime.getMinutes();
    const offset = dateTime.getTimezoneOffset();

    dateTime.setMinutes(minutes - offset);
    return dateTime.toISOString();
  }

  const initialDate = new Date();
  const [minDate, setMinDate] = useState(getFormatted(initialDate).slice(0, 10));

  initialDate.setHours(initialDate.getHours() + 1);
  const [date, setDate] = useState(getFormatted(initialDate).slice(0, 10));
  const [time, setTime] = useState(getFormatted(initialDate).slice(11, 16));
  const [isPast, setPast] = useState(false);

  // start timer for updating min date
  useEffect(() => {
    let ticker = setInterval(() => {
      setMinDate(getFormatted(new Date()).slice(0, 10));
      checkPast(date, time);
    }, 500);
    return () => clearInterval(ticker);
  }, [date, time]);
  
  // check if selected is in the past
  const checkPast = (dateValue, timeValue) => {
    let tmp = new Date(dateValue + " " + timeValue);
    setPast(tmp <= new Date());
  }

  const handleTimeChange = (data) => {
    setTime(data);
    checkPast(date, data);
  }

  const handleDateChange = (data) => {
    setDate(data);
    checkPast(data, time);
  }

  return (
    <Dialog title="Nachricht zeitgesteuert senden" onHide={onHide}>
      <Message 
        content={message} 
        sender=""
        star={false}
        me={true}
        onClick={() => {}}
        /><br/>

      <input
       type="date"
       value={date}
       min={minDate}
       onChange={(e) => handleDateChange(e.target.value)} />

      <input
       type="time"
       value={time}
       onChange={(e) => handleTimeChange(e.target.value)} /><p/>

      { isPast ? 
       <span style={{color: "red"}}>Sie können keinen Zeitpunkt in der Vergangenheit angeben!</span> :
       <input type="button" value="Nachricht senden" onClick={() => onSend(message, date, time)} />
      }

    </Dialog>
  )
}

TimedMessageDialog.propTypes = {
  message: PropTypes.string.isRequired,
  onSend: PropTypes.func.isRequired,
  onHide: PropTypes.func.isRequired
};

export default TimedMessageDialog;
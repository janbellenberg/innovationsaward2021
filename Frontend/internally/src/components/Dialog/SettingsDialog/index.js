import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Dialog from '..';
import Switch from '../../Switch';

import { loadSettings, updateSettings } from '../../../api/administration/Dashboard';

/**
 * dialog for changing settings
 * @param {object} props onHide event
 * @function SettingsDialog
 * @see Dialog
 * @see loadSettings
 * @see updateSettings
 */
const SettingsDialog = ({onHide}) => {
  const [smtpUsername, setSmtpUsername] = useState("");
  const [smtpPassword, setSmtpPassword] = useState("");
  const [smtpHost, setSmtpHost] = useState("");
  const [smtpPort, setSmtpPort] = useState("");
  const [smtpFrom, setSmtpFrom] = useState("");
  const [defaultPassword, setPassword] = useState("");
  const [adminPassword, setAdminPassword] = useState("");
  const [loginMail, setLoginMail] = useState(true);
  const [useTwoFA, setTwoFA] = useState(true);
  const [changeAdminPassword, setChangeAdminPassword] = useState(false);

  // load settings
  useEffect(() => {
    loadSettings().then(result => {
      setPassword(result["default-password"]);
      setLoginMail(result["login-notification"]);
      setTwoFA(result["2fa"]);

      setSmtpUsername(result.smtp.username);
      setSmtpPassword(result.smtp.password);
      setSmtpHost(result.smtp.host);
      setSmtpPort(result.smtp.port);
      setSmtpFrom(result.smtp.from);
    });
  }, []);
  
  return (
    <Dialog title="Einstellungen" format="fullScreen" onHide={onHide} open={true}>
      <fieldset>
        <legend>SMTP</legend>

        <label htmlFor="username">Benutzer: </label>
        <input value={smtpUsername} onChange={(e) => setSmtpUsername(e.target.value)} id="username" type="text" />

        <label htmlFor="password">Passwort: </label>
        <input value={smtpPassword} onChange={(e) => setSmtpPassword(e.target.value)} id="password" type="password" /><p/>

        <label htmlFor="host">Host: </label>
        <input value={smtpHost} onChange={(e) => setSmtpHost(e.target.value)} id="host" type="text" />

        <label htmlFor="port">Port: </label>
        <input value={smtpPort} onChange={(e) => setSmtpPort(e.target.value)} id="port" type="number" min="0" /><p/>

        <label htmlFor="from">Absender: </label>
        <input value={smtpFrom} onChange={(e) => setSmtpFrom(e.target.value)} id="from" type="email" />
        <p/>
      </fieldset><p/>

      <fieldset>
        <legend>Sicherheit</legend>

        <label className="big" htmlFor="default-password">Standard Passwort: </label>
        <input 
          className="big" 
          type="text"
          value={defaultPassword}
          onChange={(e) => setPassword(e.target.value)}
          id="default-password" /><p/>

        <div id="action-wrapper">
          <Switch 
            title="Login Benachrichtigung" 
            option1="Ja" 
            option2="Nein" 
            current={loginMail} 
            onSwitch={(e) => setLoginMail(e)} />

          <Switch 
            title="Login in 2 Schritten" 
            option1="Ja" 
            option2="Nein" 
            current={useTwoFA}
            onSwitch={(e) => setTwoFA(e)} />

          <Switch 
            title="Admin Passwort ändern" 
            option1="Ja" 
            option2="Nein" 
            current={changeAdminPassword}
            onSwitch={(e) => setChangeAdminPassword(e)} />

        </div><p/>

        {changeAdminPassword ? 
            <>
              <label className="big" htmlFor="admin-password">Admin Passwort: </label>
              <input 
                className="big" 
                type="text"
                value={adminPassword}
                onChange={(e) => setAdminPassword(e.target.value)}
                id="admin-password" /><p/>
              <p/>
            </>

          : null}
      </fieldset><p/>

      <input
        style={{display: "inline"}}
        type="button"
        value="Einstellungen speichern"
        onClick={() => updateSettings({
          smtp: {
            username: smtpUsername,
            password: smtpPassword,
            host: smtpHost,
            port: smtpPort,
            from: smtpFrom
          },
          "admin_password": changeAdminPassword ? adminPassword : undefined,
          "default-password": defaultPassword,
          "login-notification": loginMail,
          "2fa": useTwoFA
        }).then(success => {
          if(success){
            onHide();
          }})} />

      <span style={{color: "#9c9c9c", fontSize: "0.9em", marginLeft: "2em"}}>
        Wenn Sie in den nächsten Minuten keine E-Mail zur Bestätigung erhalten, sind die Konfigurationen fehlerhaft.
      </span>
      <p/>
    </Dialog>
  );
};

SettingsDialog.propTypes = {
  onHide: PropTypes.func.isRequired
};

export default SettingsDialog;
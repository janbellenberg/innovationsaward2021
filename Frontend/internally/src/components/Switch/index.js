import React from 'react';
import PropTypes from 'prop-types';
import "./Switch.css";

/**
 * component for switching between to options
 * @param {object} props title, current value, both options and the switch event
 * @function Switch
 */
const Switch = ({title, current, option1, option2, onSwitch}) => {
  return (
    <fieldset>
      <legend>{title}</legend>
      <div className="switch" yes={current === true ? "" : null}>
        <div onClick={() => onSwitch(true)}>{option1}</div>
        <div onClick={() => onSwitch(false)}>{option2}</div>
      </div>
    </fieldset>
  );
};

Switch.propTypes = {
  title: PropTypes.string.isRequired,
  current: PropTypes.bool.isRequired,
  option1: PropTypes.string.isRequired,
  option2: PropTypes.string.isRequired,
  onSwitch: PropTypes.func.isRequired
};

export default Switch; 
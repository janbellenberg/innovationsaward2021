import React from 'react';
import PropTypes from 'prop-types';
import "./GridLayout.css";

import Title from "../Title";

/**
 * component for the grid layout in login, logout and info
 * @param {object} props title (can be node), header, children
 * @function GridLayout
 * @see Title
 */
const GridLayout = ({title, header, children}) => {
  return (
    <main id="grid-layout">
      <Title title={title} />
      <section>
        <div id="grid-content-wrapper">
          <h1 id="grid-title">{header === undefined ? title : header}</h1>
          {children}
        </div>
      </section>
    </main>
  );
}

GridLayout.propTypes = {
  title: PropTypes.node.isRequired,
  header: PropTypes.string,
  children: PropTypes.node
};

export default GridLayout;
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ParticleBackground from '../ParticleBackground';

import './SideOverlay.css';

/**
 * component for the side overlay in chat and employees
 * @param {object} props children, the title, if open, close & search even and if a searchbar should be shown
 * @function SideOverlay
 * @see ParticleBackground
 */
const SideOverlay = ({children, title, open, onClose, searchBar = false, onSearch = undefined}) => {
  const [query, setQuery] = useState("");

  return (
    <div>
      <div id="blur-bg" style={{display: open ? "block" : "none"}}></div>

      <aside className="overlay" open={open ? " " : undefined}>
        <span id="close" onClick={onClose}>✖</span>
        <ParticleBackground/>

        <div>
          <h1>{title}</h1>

          {searchBar ? <fieldset id="search-bar">
            <legend>Suchen</legend>
            <input
              value={query}
              type="text"
              id="search"
              placeholder={title}
              onChange={(e) => setQuery(e.target.value)} />

            <input
              type="button"
              id="search-btn"
              value="Suchen"
              onClick={() => onSearch(query)} />

          </fieldset> : null}
        </div>

        {children}
      </aside>
    </div>
  );
};

SideOverlay.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  searchBar: PropTypes.bool,
  onSearch: PropTypes.func,
};

export default SideOverlay;
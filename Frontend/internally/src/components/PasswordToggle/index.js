import React from 'react';
import PropTypes from 'prop-types';
import { ReactComponent as Eye } from '../../svg/Eye.svg';
import './PasswordToggle.css';
/**
 * component for the small eye-icon next to the password field
 * @param {object} props show / hide events
 * @function PasswordToggle
 */
const PasswordToggle = ({onShow, onHide}) => {
  return (
    <div 
      id="show-password" 
      onMouseDown={onShow}
      onMouseUp={onHide}>

      <Eye />

    </div>
  )
};

PasswordToggle.propTypes = {
  onShow: PropTypes.func.isRequired,
  onHide: PropTypes.func.isRequired
};

export default PasswordToggle;
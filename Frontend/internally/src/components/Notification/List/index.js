import React, { useState, useEffect } from 'react';

import AddNotification from '../AddNotification';
import Notification from '..';
import '../Notifications.css';

import { loadNotifications, addNotification, deleteNotification } from '../../../api/Notifications';

/**
 * wrapper component for the notifications
 * @function NotificationList
 * @see AddNotification
 * @see Notification
 */
const NotificationList = () => {
  const [isAdd, setAdd] = useState(false);
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    load();
  }, []);

  // load notifications
  const load = () => {
    loadNotifications()
      .then(result => setNotifications(result));
  }

  return (
    <div>
      {!isAdd ? <div id="add-notification-btn" onClick={() => setAdd(true)}>+</div> : null}

      <div id="notifications">
        {isAdd ? 
          <AddNotification
            add={data => {
              addNotification({
                content: data.content,
                same_department: data.department,
                same_location: data.location
              }).then(() => setAdd(false)).then(load);
            }}
            close={() => setAdd(false)} />
         : null}

        {notifications.map((n) => <Notification
          key={n.id}
          item={n}
          onDelete={id => {
            deleteNotification(id).then(load);
          }} />)}
      </div>
    </div>
  );
};

export default NotificationList;
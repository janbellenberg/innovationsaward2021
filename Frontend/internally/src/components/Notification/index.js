import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

/**
 * component for a single notification
 * @param {object} props instance of the notification, delete event
 * @function Notification
 */
const Notification = ({item, onDelete}) => {
  const user = useSelector(s => s.user);

  return (
    <div className="notification">
      {user.id === item.author.id ? <div className="close-notification delete" onClick={() => onDelete(item.id)}>X</div> : null}
      <div className="sender">{item.author.firstname} {item.author.lastname}</div>
      <div className="message">{item.content}</div>
    </div>
  );
};

Notification.propTypes = {
  item: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired
};

export default Notification;
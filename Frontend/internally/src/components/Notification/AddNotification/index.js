import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import ToggleButton from '../../ToggleButton';

/**
 * component for adding a notification
 * @param {object} props add + close events
 * @function AddNotification
 * @see ToggleButton
 */
const AddNotification = ({add, close}) => {
  let [content, setContent] = useState("");
  let [department, setDepartment] = useState(false);
  let [location, setLocation] = useState(false);
  let user = useSelector(s => s.user);

  let updateContent = (val) => setContent(val);

  return (
    <div className="notification" id="add">
      <div className="close-notification" onClick={() => close()}>X</div>

      <div className="sender">
        {user.firstname} {user.lastname}
      </div>

      <div className="message">
        <textarea 
          onChange={(e) => updateContent(e.target.value)} 
          value={content}>
        </textarea>
      </div><p/>

      <ToggleButton
        text="Eigene Abteilung"
        value={department}
        onChange={(value) => setDepartment(value)}/>

      <ToggleButton
        text="Eigener Standort"
        value={location}
        onChange={(value) => setLocation(value)}/><br/>
      
      <input 
        type="button"
        id="commit-notification"
        onClick={() => add({
          content: content, 
          department: department, 
          location: location
        })}
        value="Benachrichtigung senden" />
    </div>
  );
};

AddNotification.propTypes = {
  add: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default AddNotification;
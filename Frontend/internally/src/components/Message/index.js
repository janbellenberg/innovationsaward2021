import React from 'react';
import PropTypes from 'prop-types';
import './Message.css';

/**
 * component for a message
 * @param {object} props children, content of the message, name of the sender, if its an own message, if its an saved message, onClick event
 * @function Message
 */
const Message = ({children, content, sender, me, star, onClick}) => {
  return (
    <div className="container" me={me ? "" : undefined} onClick={() => onClick()}>
      {star ? <div className="star">★</div> : null}
      <div className="bubble">
        {!me ? <h2>{sender}</h2> : null}
        <pre>{content}</pre>
      </div>
      {children}
    </div>
  );
};

Message.propTypes = {
  children: PropTypes.node,
  content: PropTypes.string.isRequired,
  sender: PropTypes.string.isRequired,
  me: PropTypes.bool.isRequired,
  star: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired
};

export default Message;
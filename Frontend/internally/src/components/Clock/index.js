import React, { useState, useEffect } from 'react';
import "./Clock.css";

const days = ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"];

/**
 * clock component for dashboard
 * @function Clock
 */
const Clock = () => {

  let [now, setNow] = useState(new Date());

  // start ticker and remove it when unmounting
  useEffect(() => {
    let ticker = setInterval(() => setNow(new Date()), 500);
    return () => clearInterval(ticker);
  }, []);

  // get date / time values
  let minute = now.getMinutes().toString().padStart(2, "0");
  let hour = now.getHours().toString().padStart(2, "0");

  let day = now.getDate().toString().padStart(2, "0");
  let month = (now.getMonth() + 1).toString().padStart(2, "0");
  let year = now.getFullYear();

  return (
    <div id="clock">
      <div id="current-time">
        {hour}:{minute}
      </div>
      <div id="current-date">
        {days[now.getDay()]}, {day}.{month}.{year}
      </div>
    </div>
  );
}

export default Clock;
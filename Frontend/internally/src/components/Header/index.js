import React, { useState, Children } from 'react';
import PropTypes from 'prop-types';
import "./Header.css";
import MenuButton from '../MenuButton';

/**
 * component for the header bar with title and action buttons
 * @param {object} props should always show the menu button, the title, children
 * @function Header
 * @see MenuButton
 */
const Header = ({hasMenuButton = false, title, children}) => {
  const [showActions, setShowActions] = useState(false);

  return (
    <div>
      <MenuButton showAlways={hasMenuButton} />
      <header menu={hasMenuButton ? "" : null}>
        <h1>{title}</h1>
        {children}
        
        { Children.count(children) > 0 ? 
          <svg height="24" viewBox="0 0 24 24" width="24" onClick={() => setShowActions(true)}>
            <path d="M7.41,8.59L12,13.17l4.59-4.58L18,10l-6,6l-6-6L7.41,8.59z" fill="#fff"></path>
          </svg>
        : null}
      </header>

      {showActions ? 
        <div id="blur-bg">
          <div className="small-action-bar">
          <span className="close-dialog" onClick={() => setShowActions(false)}>X</span>
            {children}
          </div>
        </div>
      : null}
    </div>
  );
}

Header.propTypes = {
  hasMenuButton: PropTypes.bool,
  title: PropTypes.string.isRequired,
  children: PropTypes.node
};

export default Header;
import React from 'react';
import MenuItem from '../../../components/DashboardMenu/MenuItem';

import { ReactComponent as Chat } from "../../../svg/Chat.svg";
import { ReactComponent as Profile } from "../../../svg/Profile.svg";
import { ReactComponent as Info } from "../../../svg/Informations.svg";
import { ReactComponent as Logout } from "../../../svg/Logout.svg";
import { ReactComponent as Employees } from "../../../svg/Employees.svg";

/**
 * Menu component for dashboard navigation
 * @function Menu
 * @see MenuItem
 */
const Menu = () => {
  return (
    <section id="menu">
      <MenuItem 
        title="Chat" 
        dest="/chat"
        iconOrange={false}>
          <Chat />
      </MenuItem>

      <MenuItem 
        title="Profil" 
        dest="/profile"
        iconOrange={false} >
          <Profile />
      </MenuItem>

      <MenuItem 
        title="Mitarbeiter" 
        dest="/employees"
        iconOrange={false} >
          <Employees />
      </MenuItem>

      <div style={{flexBasis: "100%"}}></div>
      
      <MenuItem 
        title="Info" 
        dest="/info"
        iconOrange={true} >
          <Info />
      </MenuItem>

      <MenuItem 
        title="Abmelden" 
        dest="/logout"
        iconOrange={true} >
          <Logout />
      </MenuItem>

    </section>
  );
}

export default Menu;
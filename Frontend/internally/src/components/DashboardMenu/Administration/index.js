import React from 'react';
import MenuItem from '../MenuItem';

import '../Menu.css';

import { ReactComponent as Chat } from "../../../svg/Chat.svg";
import { ReactComponent as Departments } from "../../../svg/Departments.svg";
import { ReactComponent as Groups } from "../../../svg/Groups.svg";
import { ReactComponent as Info } from "../../../svg/Informations.svg";
import { ReactComponent as Locations } from "../../../svg/Locations.svg";
import { ReactComponent as Logout } from "../../../svg/Logout.svg";
import { ReactComponent as Employees } from "../../../svg/Employees.svg";

/**
 * Menu component for administration dashboard navigation
 * @function Menu
 * @see MenuItem
 */
const Menu = () => {
  return (
    <section id="menu">
      <MenuItem 
        title="Mitarbeiter" 
        dest="/administration/employees" 
        iconOrange={false}>
          <Employees />
      </MenuItem>
        
      <MenuItem 
        title="Abteilungen" 
        dest="/administration/departments"
        iconOrange={false}>
          <Departments />
      </MenuItem>
        
      <MenuItem 
        title="Standorte" 
        dest="/administration/locations"
        iconOrange={false}>
          <Locations />
      </MenuItem>
        
      <MenuItem 
        title="Gruppen" 
        dest="/administration/groups"
        iconOrange={false}>
          <Groups />
      </MenuItem>

      {/* line break */}
      <div style={{flexBasis: "100%"}} />

      <MenuItem 
        title="Info" 
        dest="/info"
        iconOrange={true}>
          <Info />
      </MenuItem>
        
      <MenuItem 
        title="Chat" 
        dest="/chat"
        iconOrange={true}>
          <Chat />
      </MenuItem>
        
      <MenuItem 
        title="Abmelden" 
        dest="/logout"
        iconOrange={true}>
          <Logout />
      </MenuItem>
        
    </section>
  );
}

export default Menu;
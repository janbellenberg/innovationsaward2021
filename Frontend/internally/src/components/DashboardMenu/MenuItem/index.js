import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

/**
 * menu item for dashboard navigation
 * @param {object} props title of the item, icon should be orange, link destination, icon
 * @function MenuItem
 */
const MenuItem = ({title, iconOrange = false, dest, children}) => {
  return (
    <Link to={dest} className="link">
      <div className={iconOrange ? "orange" : "blue"}>
        {children}
      </div>
      <div className="description">
        {title}
      </div>
    </Link>
  );
}

MenuItem.propTypes = {
  title: PropTypes.string.isRequired,
  iconOrange: PropTypes.bool,
  dest: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
}

export default MenuItem;
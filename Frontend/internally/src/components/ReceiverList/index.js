import React from 'react';
import PropTypes from 'prop-types';
import Receiver from './Receiver';
import "../MemberList/MemberList.css";

/**
 * wrapper component for the receiver of a message
 * @param {object} props list of the receiver
 * @function ReceiverList
 * @see Receiver
 */
const ReceiverList = ({receivers}) => {
  return (
    <div className="member-list">
        {receivers.map((receiver) => 
          <Receiver key={receiver.uid} receiver={receiver} />
        )}
    </div>
  );
};

ReceiverList.propTypes = {
  receivers: PropTypes.array.isRequired
};

export default ReceiverList;
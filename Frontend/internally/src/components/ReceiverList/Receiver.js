import React from 'react';
import PropTypes from 'prop-types';

/**
 * component for a single receiver item
 * @param {object} props instance of the receiver
 * @function Receiver
 */
const Receiver = ({receiver}) => {

  return (
    <div className="member-list-item no-remove">
      <img
        alt={receiver.firstname + " " + receiver.lastname}
        src={"/api/v1/profile/" + receiver.uid + "/image"} />

      <div className="member-name">
        {receiver.firstname} {receiver.lastname}
      </div>

      <div className="message-status">
        Empfangen: <br/> {receiver.timestamp.received}
        <p/>
        Gelesen: <br/> {receiver.timestamp.read}
      </div>
    </div>
  );
};

Receiver.propTypes = {
  receiver: PropTypes.object.isRequired
};

export default Receiver;
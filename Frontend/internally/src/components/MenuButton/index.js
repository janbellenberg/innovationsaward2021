import React, { useState } from 'react';
import './MenuButton.css';

/**
 * menu button for displaying the navigation (mobile size, user ui)
 * @param {object} props 
 * @function MenuButton
 */
const MenuButton = ({showAlways = false}) => {

  const [open, setOpen] = useState(false);

  // toggle the navigation with an animation
  const toggleNavigation = () => {
    var nav = document.getElementById("nav-wrapper");
    if(nav === null) {
      nav = document.querySelector("nav");
    }

    if(nav === null){
      return;
    }

    if(!open){
      // show navigation
      nav.style.display = "block";

      setTimeout(() => {
        nav.style.opacity = 1;
      }, 1);
    }
    else{
      // hide navigation
      nav.style.opacity = 0;
      
      setTimeout(() => {
        nav.style.display = "none";
      }, 500);
    }

    setOpen(!open);
  }

  return (
    <div id="openmenu" open={open} always={showAlways ? "" : undefined} onClick={() => toggleNavigation() }>
      <div className="bar1"></div>
      <div className="bar2"></div>
      <div className="bar3"></div>
    </div>
  );
}

export default MenuButton;
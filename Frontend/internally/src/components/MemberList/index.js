import React from 'react';
import PropTypes from 'prop-types';
import Member from './Member';
import "./MemberList.css";

/**
 * wrapper component for member list in chat dialog
 * @param {object} props list of members, event to reload when member was removed
 * @function MemberList
 * @see Member
 */
const MemberList = ({chat, members, onRefresh}) => {
  return (
    <div className="member-list">
        {members.map((member) => 
          <Member key={member.id} chat={chat} member={member} onRefresh={onRefresh} />
        )}
    </div>
  );
};

MemberList.propTypes = {
  chat: PropTypes.number.isRequired,
  members: PropTypes.array.isRequired,
  onRefresh: PropTypes.func.isRequired
};

export default MemberList;
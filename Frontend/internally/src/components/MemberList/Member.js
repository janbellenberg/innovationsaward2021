import React from 'react';
import PropTypes from 'prop-types';
import { deleteMember } from '../../api/user/Chat';

/**
 * component for a member of a chat
 * @param {object} props instance of the member, event to reload when member was removed
 * @function Member
 */
const Member = ({chat, member, onRefresh}) => {
  const deleteMemberEvent = () => {
    deleteMember(chat, member.id).then(onRefresh);
  };

  return (
    <div className="member-list-item" onClick={deleteMemberEvent}>
      <img
        alt={member.firstname + " " + member.lastname}
        src={"/api/v1/profile/" + member.id + "/image"} />

      <div className="member-name">
        {member.firstname} {member.lastname}
      </div>
    </div>
  );
};

Member.propTypes = {
  chat: PropTypes.number.isRequired,
  member: PropTypes.object.isRequired,
  onRefresh: PropTypes.func.isRequired
};

export default Member;
import React from 'react';
import "./Wait.css";
import { useSelector } from 'react-redux';

/**
 * wait screen
 * @function Wait
 */
const Wait = () => {
  // check redux, if tasks are running
  const waiting = useSelector(state => state.waiting);

  return (
    waiting > 0 ? 
      <div id="wait">
        <div>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
        <h1>Bitte warten...</h1>
      </div>
    : ""
  );
}

export default Wait;
import React from 'react';
import PropTypes from 'prop-types';
import "./StatusIndicator.css";

// Options for status information
const options = ["Offline", "Online", "Abwesend", "Beschäftigt"];

/**
 * component for indication of the status in the profile of another employee
 * @param {object} props current status
 * @function StatusIndicator
 */
const StatusIndicator = ({status}) => {
  return (
    <div id="status" status={status}>
      {options[status]}
    </div>
  );
};

StatusIndicator.propTypes = {
  status: PropTypes.number.isRequired
};

export default StatusIndicator; 
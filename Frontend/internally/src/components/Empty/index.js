import React from 'react';
import PropTypes from 'prop-types';
import { ReactComponent as Lens } from "../../svg/Empty.svg";
import "./Empty.css";

/**
 * component for empty data in administration
 * @param {object} props title of the empty entity
 * @function Empty
 */
const Empty = ({description}) => {
  return (
    <div className="nothing-found">
      <div>
        <Lens />
      </div>
      <h1>Es scheint, als wurden keine {description} gefunden</h1>
    </div>
  );
}

Empty.propTypes = {
  description: PropTypes.string.isRequired
};

export default Empty;
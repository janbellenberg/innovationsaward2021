import React from 'react';
import PropTypes from 'prop-types';
import InfoEntry from './InfoEntry';

/**
 * wrapper-component for os information
 * @param {object} props information data
 * @function OSInfo
 * @see InfoEntry
 */
const OSInfo = ({data}) => {
  return (
    <article>
      <h2 id="title-os">Betriebssystem</h2>
      <table aria-describedby="title-os">
        <tbody>
          <InfoEntry title="Name" value={data.name} />
          <InfoEntry title="Version" value={data.version} />
          <InfoEntry title="Architektur" value={data.arch} />
          <InfoEntry title="Benutzer" value={data.user} />
        </tbody>
      </table>
    </article>
  );
};

OSInfo.propTypes = {
  data: PropTypes.object.isRequired
};

export default OSInfo;
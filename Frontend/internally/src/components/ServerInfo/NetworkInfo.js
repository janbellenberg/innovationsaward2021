import React from 'react';
import PropTypes from 'prop-types';
import InfoEntry from './InfoEntry';

/**
 * wrapper-component for network information
 * @param {object} props information data
 * @function NetworkInfo
 * @see InfoEntry
 */
const NetworkInfo = ({data}) => {
  return (
    <article>
      <h2 id="title-network">Netzwerk</h2>
      <table aria-describedby="title-network">
        <tbody>
          <InfoEntry title="IPv4" value={data.ipv4.join(' ')} small={true}/>
          <InfoEntry title="IPv6" value={data.ipv6.join(' ')} />
          <InfoEntry title="Hostname" value={data.hostname} />
          <InfoEntry title="Offene Sockets" value={data.sockets} />
        </tbody>
      </table>
    </article>
  );
};

NetworkInfo.propTypes = {
  data: PropTypes.object.isRequired
};

export default NetworkInfo;
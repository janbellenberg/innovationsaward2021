import React from 'react';
import PropTypes from 'prop-types';
import InfoEntry from './InfoEntry';

/**
 * wrapper-component for java information
 * @param {object} props information data
 * @function JavaInfo
 * @see InfoEntry
 */
const JavaInfo = ({data}) => {
  return (
    <article>
      <h2 id="title-java">Java</h2>
      <table aria-describedby="title-java">
        <tbody>
          <InfoEntry title="VM Name" value={data["vm.name"]} />
          <InfoEntry title="Version" value={data.version} />
          <InfoEntry title="Hersteller" value={data.vendor} />
          <InfoEntry title="internally Version" value={data.internally_version} />
        </tbody>
      </table>
    </article>
  );
};

JavaInfo.propTypes = {
  data: PropTypes.object.isRequired
};

export default JavaInfo;
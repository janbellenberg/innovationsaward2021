import React from 'react';
import PropTypes from 'prop-types';
import './ServerInfo.css';

/**
 * component for a single information on admin dashboard
 * @param {object} props title, value and if the space should be smaller
 * @function InfoEntry
 */
const InfoEntry = ({title, value, small = false}) => {
  return (
    <tr>
      <td>{title}</td>
      <td column-title={title} style={{paddingRight: small ? "6em" : "10px"}}>
        {value}
      </td>
    </tr>
  );
};

InfoEntry.propTypes = {
  title: PropTypes.string.isRequired,
  value: PropTypes.node.isRequired,
  small: PropTypes.bool
};

export default InfoEntry;
import React from 'react';
import PropTypes from 'prop-types';
import "./ToggleButton.css";

/**
 * component for the toggle button (groups dialog, add notification)
 * @param {object} props text, value, onChange event
 * @function ToggleButton
 */
const ToggleButton = ({text, value, onChange}) => {
  return (
    <span
      className="checkable"
      style={{ // select color according to current value
        borderColor: value ? "var(--green)" : "var(--orange)",
        color: value ? "var(--green)" : "var(--orange)"
      }}
      onClick={() => onChange(!value)}>
        <b>{text}</b> <br/>
        {value ? "Ja" : "Nein"}
    </span>
  );
};

ToggleButton.propTypes = {
  text: PropTypes.string.isRequired,
  value: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired
};

export default ToggleButton;
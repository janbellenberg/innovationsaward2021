import React from 'react';
import Particles from 'react-particles-js';

import './ParticleBackground.css';

// configuration of the particle animation
const configuration = {
  particles: {
    number: { // reduce amount on small devices
      value: window.innerWidth < 800 ? 20 : 30
    },
    color: {
      value: '#ffffff'
    },
    shape: {
      type: 'circle'
    },
    opacity: {
      random: true,
      anim: {
        enable: false
      }
    },
    size: {
      value: 5,
      random: true,
      anim: {
        enable: false
      }
    },
    line_linked: {    // configure the line between the dots
      enable: true,
      distance: 150,
      color: '#ffffff',
      opacity: 0.4,
      width: 1
    },
    move: {         // configure the move animation
      enable: true,
      speed: 2,
      direction: 'none',
      random: true,
      straight: true,
      out_mode: 'bounce'
    }
  }
}

/**
 * component for the particle animation
 * @function ParticleBackground
 * @see Particles
 * @see https://www.npmjs.com/package/react-particles-js
 */
const ParticleBackground  = () => {
  return (
    <Particles params={configuration}/>
  );
}

export default ParticleBackground;
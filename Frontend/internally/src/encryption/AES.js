const crypto = require('crypto');

const algorithm = 'AES-256-CTR';
const charset = 'utf-8';
const encoding = 'hex';
const hashAlgorithm = 'sha512';

/**
 * @classdesc Class for AES-Encryption
 * @class
 */
class AES {
  /**
   * CONSTRUCTOR initializes the IV
   * @constructor
   * @param {string} salt own salt value
   */
  constructor(salt) { // salt from sender
    this.iv = crypto.pbkdf2Sync(salt, "", 1000, 16, hashAlgorithm);
  }

  /**
   * Derives a new key for the encryption
   * @param {string} password last key
   * @returns derived key
   * @function deriveKey
   */
  deriveKey(password) {
    let key = crypto.pbkdf2Sync(password, this.iv, 1000, 256 / 8, hashAlgorithm);
    return key.toString(encoding);
  }

  /**
   * Encrypts data using AES
   * @param {string} data data, that should be encrypted
   * @param {string} key key, that should be used
   * @returns encrypted data
   * @function encrypt
   */
  encrypt(data, key) {
    const cipher = crypto.createCipheriv(algorithm, Buffer.from(key, encoding), this.iv);
    
    let encrypted = cipher.update(data, charset, encoding);
    encrypted += cipher.final(encoding);
    return encrypted;
  }

  /**
   * Decrypts data using AES
   * @param {string} encrypted encrypted data
   * @param {string} key key, that was used for the encryption
   * @returns decrypted, raw data
   * @function decrypt
   */
  decrypt(encrypted, key) {
    const decipher = crypto.createDecipheriv(algorithm, Buffer.from(key, encoding), this.iv);

    let decrypted = decipher.update(encrypted, encoding, charset);
    decrypted += decipher.final(charset);
    return decrypted;
  }
}

export default AES;
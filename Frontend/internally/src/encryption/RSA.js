const crypto = require('crypto');
const NodeRSA = require('node-rsa');

const hashAlgorithm = 'SHA512';
const algorithm = 'RSA-' + hashAlgorithm;
const encoding = 'base64';

const serverPublicKey = "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA20F8VTzoe8MdEb7/+LEr\nbipoDyKMAuBkdRmzJRrxSmkGX44GnacX38vhJvcJJp7sFYNZALofRMxIcX7U+fXZ\noDc1xJKaIgGBqZRse/KV4kllVDKdXjUd2K8O2OVhEZpi4aHuQfqRYmylWxwmETqQ\nLwzrg08ae1XElXTgARL/2AW9PtHnLQYayZM8B63iUr/prdXxd57nvEnkdCHwx2zq\ntPGq28b7xFvD+NSouyouJWWmCVjPjCmPNSWg/HivNlo/m7yJs7SEnDfEMUBCa4V0\nvMPTTvPxJaryE8uM9k5dlMY9meD6es14ng7+h5b8viyqsQHUMhePzeozHkq8ZNq1\ngwIDAQAB-----END PUBLIC KEY-----";

/**
 * @classdesc Class for RSA sign / verify
 * @class
 * @hideconstructor
 */
class RSA {

  /**
   * Generates a new RSA-Key
   * @returns object with the public and private component of a new rsa-key
   * @function createKeyPair
   */
  createKeyPair() {
    let key = new NodeRSA({b: 2048});
    let keypair = {
      public: key.exportKey('pkcs8-public-pem'),
      private: key.exportKey('pkcs1-pem')
    }

    return keypair;
  }

  /**
   * Signs data with a private key
   * @param {string} message data to sign
   * @param {*} privateKey private key, that should be used to sign
   * @returns signature
   * @function sign
   */
  sign(message, privateKey) {
    const sign = crypto.createSign(algorithm);
    sign.write(message);
    sign.end();

    let signature = sign.sign(privateKey, encoding);
    return signature;
  }

  /**
   * Verifies the signature of a message
   * @param {string} message original message
   * @param {string} signature signed message (signature)
   * @param {string} publicKey public key (matching with the used private key)
   * @returns true, if the signature is valid
   * @function verify
   */
  verify(message, signature, publicKey) {
    const verify = crypto.createVerify(algorithm);
    verify.write(message);
    verify.end();

    let verified = verify.verify(publicKey, signature, encoding);
    return verified;
  }

  /**
   * Verifies the signature of a message (public key from the server)
   * @param {string} data original message
   * @param {string} signature signed message (signature)
   * @returns true, if the signature is valid
   * @function verifyServerAuthority
   */
  verifyServerAuthority(data, signature) {
    return this.verify(data, signature, serverPublicKey);
  }

  /**
   * Calculates the security number for two employees
   * @param {string} publicKey1 public key of the user with the smaller id
   * @param {string} publicKey2 public key of the user with the bigger id
   * @returns the SHA512 Hash of both certificates
   * @function calculateSecurityNumber
   */
  calculateSecurityNumber(publicKey1, publicKey2) {   // values are sorted by UID
    const hash = crypto.createHash(hashAlgorithm);
    hash.update(publicKey1 + publicKey2);
    
    let securityNumber = hash.digest(encoding);
    return securityNumber;
  }
}

export default RSA;
const crypto = require('crypto');

const curve = 'secp521r1';
const encoding = 'hex';

/**
 * @classdesc Class for ECDH key exchange
 * @class
 * @hideconstructor
 */
class ECDH {

  /**
   * Generate a new ECDH-Keypair
   * @returns object with the public and private component of a new ecdh keypair
   * @function generatePair
   */
  generatePair(){
    const me = crypto.createECDH(curve);
    me.generateKeys();

    let pair = {
      public: me.getPublicKey().toString(encoding),
      private: me.getPrivateKey().toString(encoding)
    };

    return pair;
  }

  /**
   * Calculates the shared secret
   * @param {string} myPrivate own private ecdh component
   * @param {string} foreignPublic public ecdh component of the peer
   * @returns the calculated, shared secret
   * @function getSecret
   */
  getSecret(myPrivate, foreignPublic) {
    const me = crypto.createECDH(curve);
    me.setPrivateKey(Buffer.from(myPrivate, encoding));

    let secret = me.computeSecret(Buffer.from(foreignPublic, encoding));
    return secret.toString(encoding);
  }
}

export default ECDH;
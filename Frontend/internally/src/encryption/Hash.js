const crypto = require('crypto')

const encoding = 'hex';

/**
 * @classdesc Class for SHA-Hashing
 * @class
 * @hideconstructor
 */
class Hash{
  /**
   * Calculates the SHA-512 Hash value
   * @param {string} input data, that should be hashed
   * @returns SHA-512 Hash
   * @function getSHA512Hash
   */
  getSHA512Hash(input) {
    const hash = crypto.createHash('sha512');
    hash.update(input);
    return hash.digest(encoding);
  }
}

export default Hash;
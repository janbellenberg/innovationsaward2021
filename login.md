# Login

Wie gewünscht enthält das SQL-Skript bereits auch Beispieldaten.
Für den Login stehen Ihnen folgende Mitarbeiter zur Verfügung:
- admin
- janbellenberg
- simonmaddahi

Alle Mitarbeiter haben das Passwort "internally".

# PORTS
Verfügbar ist die Oberfläche über die Ports 8080 (HTTP) und 8443 (HTTPS). 
Die Konfigurationsoberfläche des Wildfly-Servers befindet sich unter dem Port 9990. Das Einrichten wird in der Dokumentation detailliert erklärt.
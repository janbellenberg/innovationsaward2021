# internally
> 1st place GFOS Innovationsaward 2021
> Report available [here](https://www.innovations-report.de/sonderthemen/foerderungen-preise/die-gewinner-des-gfos-innovationsaward-2021-stehen-fest/)

The software "internally" is a CHAT-APPLICATION for the internal communication of a company. The software was developed by Jan Bellenberg and Simon Maddahi during the [GFOS-Innovationsaward 2021](https://www.gfos.com/fileadmin/user_upload/pdf/mintlab/innovationsaward/GFOS_Innovationsaward_2021_Aufgabe_Informatik.pdf).

> For further documentation please visit the complete [documentation file](Dokumentation/Dokumentation.docx)

## Installation (docker)
1. install docker
2. download project folder
3. create folder "internally" in the home-directory
4. copy file "Backend > database > internally.sql" into "$HOME/internally/db-init"
5. copy folder "Backend > wildfly-config > data" into "$HOME/internally/wildfly"
6. goto the root of the project folder
7. run `docker build -t internally:latest .` to build the application image (this can take more than 10 minutes)
8. run `docker-compose up` (**the ports 8080, 8443, 9990, 3306 must be free!**) 

## Used Technologies
- Java 15 + Maven  |  Java 11 + Maven when running in docker
- Wildfly 22
- React 17
- Node.js 15.2.1
- Docker (WSL) 3.2.2
- MySQL (mariadb) 10.5.9

### Docker images
- [node:lts-alpine3.13](https://hub.docker.com/_/node)
- [maven:3.6.3-openjdk-11-slim](https://hub.docker.com/_/maven)
- [alpine:3](https://hub.docker.com/_/alpine)
- [mariadb:10.5.9](https://hub.docker.com/_/mariadb)

## Color codes
- blue: #4e7d96
- green: #9e9d24
- yellow: #ffa000
- orange: #f35322
- dark: #191919
- dark 2: #37474f
- blue dark: #275973

## Features
- real-time chat using WebSockets
- group chat
- encrypted messages
- secure password & login management
- inspection of the server status
- organize employees in departments & locations
- support for docker

## Copyright
Copyright © 2021 Jan Bellenberg, Simon Maddahi

supervised by Mr. Ganser

[Heinz-Nixdorf Berufskolleg](https://www.hnbk.de)
